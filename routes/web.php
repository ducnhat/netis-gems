<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['lang']], function () {

Route::get('/ChangeLanguage','ProductController@ChangeLang')->name('Product.Changelanguage');    
Route::get('/', function () {
    return view('index');
});

// news
// Route::get('/news', function () {
//     return view('news.news');
// });

Route::get('/news','Frontend\NewsController@index')->name('frontend.news.index');
Route::get('/news/{slug}','Frontend\NewsController@detailNews')->name('frontend.news.detail');
Route::get('/desgin_competition', function () {
    return view('news.detail.desgin_competition');
});
Route::get('/small_is_beautiful', function () {
    return view('news.detail.small_is_beautiful');
});
Route::get('/dancings_stones', function () {
    return view('news.detail.dancings_stones');
});
Route::get('/yes_to_green_light', function () {
    return view('news.detail.yes_to_green_light');
});


//video
// Route::get('/video', function () {
//     return view('video.video');
// });
Route::get('/video','Frontend\NewsController@index2')->name('frontend.video.index');
//contact
Route::get('/contact-us', function () {
    return view('contact_us.contact_us');
});
Route::get('/ourProducts', function () {
    return view('our_products.ourproducts');
});
Route::get('/genuine-gemstones', function () {
    return view('our_products.genuine_gemstones');
});
Route::get('/sapphire', function () {
    return view('our_products.sapphire');
});
Route::get('/ruby', function () {
    return view('our_products.ruby');
});
Route::get('/topaz', function () {
    return view('our_products.topaz');
});
Route::get('/black_spinel', function () {
    return view('our_products.black_spinel');
});
Route::get('/smoky_quartz', function () {
    return view('our_products.smoky_quartz');
});
Route::get('/marcasite', function () {
    return view('our_products.marcasite');
});
//created stones
Route::get('/created-stones', function () {
    return view('our_products.created_stones');
});
Route::get('/zirconia', function () {
    return view('our_products.detail_created_stones.zirconia');
});
Route::get('/nano', function () {
    return view('our_products.detail_created_stones.nano');
});
//our company
Route::get('/our-company', function () {
    return view('our_company.our_company');
});
Route::get('/csr', function () {
    return view('our_company.csr');
});
Route::get('/history', function () {
    return view('our_company.history');
});
Route::get('/swarovki_business', function () {
    return view('our_company.swarovki_business');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::get('/index', 'AdminController@index')->name('admin.index');

    Route::group(['prefix' => 'news'], function () {
        Route::get('/', 'NewsController@index')->name('admin.news.list');
        Route::get('/created', 'NewsController@index2')->name('admin.news.created');
        Route::post('/', 'NewsController@store')->name('admin.news.store');
        Route::get('/{id}', 'NewsController@destroy')->name('admin.news.destroy');
        Route::get('edit/{id}', 'NewsController@edit')->name('admin.news.edit');
        Route::post('/update/{id}', 'NewsController@update')->name('admin.news.update');
        Route::post('/uploadimage/{type?}', 'NewsController@uploadImage')->name('admin.news.uploadimage');
        // Route::post('update/{id}', 'CityController@createOrupdate')->name('admin.city.update');
    });

      Route::group(['prefix' => 'category'], function () {
          Route::get('/', 'CategoryController@index')->name('admin.category.list');
          Route::get('/created', 'CategoryController@index2')->name('admin.category.created');
          Route::post('/', 'CategoryController@store')->name('admin.category.store');
          Route::get('/{id}', 'CategoryController@destroy')->name('admin.category.destroy');
          Route::get('edit/{id}', 'CategoryController@edit')->name('admin.category.edit');
        Route::post('/update/{id}', 'CategoryController@update')->name('admin.category.update');

      });

});


});