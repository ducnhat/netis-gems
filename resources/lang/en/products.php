<?php 
return [
	'products_home'=>'Our Products',
	'our_company'=>'Our Company',
	'genuine_gemstones'=>'Genuine Gemstones',
	'created_stones'=>'Created Stones',
	'about_swarovski'=>'About SWAROVSKI ',
	'history'=>'History',
	'news'=>'News',
	'contact_us'=>'Contact Us',
	'search'=>'Search',
	'name'=>'Name',
	'phone'=>'Phone',
	'address'=>'Address',
	'business_development_manager'=>'Business Development Manager',
	'account_manager'=>'Account Manager',
	'discover_more'=>'discover more',
	'read_more'=>'read more',
	'find_out_more'=>'find out more',
	'back'=>'Back',
	'language'=>'Language',
	'please_find_news'=>'Please find below the lasted news related to Swarovski Genuine Gemstones and Created Stones',
	'please_find_video'=>'Please find below the latest Videos from Swarovski Gemstones TM',
	'select_color'=>'Select color',
	'select_size'=>'Select size'


];

 ?>