@extends('layouts.master')
@section('styles')
      <title>MARCASITE</title>
      <link rel="stylesheet" href="../css/marcasite.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <!-- Page Content -->
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
               @include('our_products.menu_left')
            </div>
            <!--/UdmComment-->
         <div class="article col-xs-12 col-sm-12 col-md-10">
   <!--/UdmComment-->
   <div class="row">
      <div class="component-product-details col-lg-12">
         <div class="product">
            <h2 class="title">                SWAROVSKI GENUINE MARCASITE                              </h2>
         </div>
      </div>
        @if($langNetis == 'vi')
           <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                        Đá Marcasite Swarovski có nguồn gốc từ pyrit sắt có chất lượng tốt nhất, Một kỳ quan tạo hóa mang yếu tố của cả đá quý và kim loại. 
                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                    <p>Kỹ thuật cắt và đánh bóng chính xác của Swarovski mang lại vẻ đẹp thanh lịch nhưng không kém phần lung linh.

                    </p>
                                          <!-- <p><a target="_self" href="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Where to buy</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
        @else
      <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                   
                      Marcasite, a glittering and versatile jewelry stone cut from the naturally occurring mineral iron pyrite, has been called a traditional material with avant-garde reflection 
                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                    <p>Swarovski applies more than a century of state-of-the-art precision cutting to this fascinating gemstone and is the leading producer of high quality marcasite.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif
      <div class="component-product-details featured col-xs-12 col-sm-12 col-md-6" style="z-index:100; margin-bottom: 30px; clear: both;">
      </div>
      <div style="clear:both;float:none"></div>
      <div class="noGcn noFormatting noLinks noLists product-cutcontainer">
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Round_Star-Marcasite-19024-CMYK-4000px-0039_Kopie.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Round_Star_11.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Round_Star_11.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Round Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_Round_Cut_0308.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Round_Star-Marcasite-19024-CMYK-4000px-0039_Kopie.png"
                                       data-video="../img/gemstones/detail_marcasite/Round_Star_11.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Round_Star-Marcasite-19024-CMYK-4000px-0039_Kopie.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="0.90" data-foo="0">0.90 mm</option>
                        <option value="1.00" data-foo="1">1.00 mm</option>
                        <option value="1.10" data-foo="2">1.10 mm</option>
                        <option value="1.20" data-foo="3">1.20 mm</option>
                        <option value="1.30" data-foo="4">1.30 mm</option>
                        <option value="1.35" data-foo="5">1.35 mm</option>
                        <option value="1.40" data-foo="6">1.40 mm</option>
                        <option value="1.50" data-foo="7">1.50 mm</option>
                        <option value="1.60" data-foo="8">1.60 mm</option>
                        <option value="1.70" data-foo="9">1.70 mm</option>
                        <option value="1.80" data-foo="10">1.80 mm</option>
                        <option value="1.90" data-foo="11">1.90 mm</option>
                        <option value="2.00" data-foo="12">2.00 mm</option>
                      </select>
                      <br>
                    </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 2 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Square_Princess-Marcasite-19039-RGB-500px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Square_Princess_11.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Square_Princess_11.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Square Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_SquareMarcasite.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Square_Princess-Marcasite-19039-RGB-500px-0039.png"
                                       data-video="../img/gemstones/detail_marcasite/Square_Princess_11.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Square_Princess-Marcasite-19039-RGB-500px-0039.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="1.20" data-foo="0">1.20 mm</option>
                        <option value="1.30" data-foo="1">1.30 mm</option>
                        <option value="1.40" data-foo="2">1.40 mm</option>
                        <option value="1.50" data-foo="3">1.50 mm</option>
                        <option value="1.60" data-foo="4">1.60 mm</option>
                        <option value="1.70" data-foo="5">1.70 mm</option>
                        <option value="1.80" data-foo="6">1.80 mm</option>
                        <option value="1.90" data-foo="7">1.90 mm</option>
                        <option value="2.00" data-foo="8">2.00 mm</option>
                        <option value="2.10" data-foo="9">2.10 mm</option>
                        <option value="2.50" data-foo="10">2.50 mm</option>
                        <option value="3.00" data-foo="11">3.00 mm</option>
                      </select>
                      <br>
                    </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 3 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Baguette_Cut-Marcasite-19049-CMYK-4000px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Baguette_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Baguette_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Baguette Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_K2515_BaguettesMarcasite.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Baguette_Cut-Marcasite-19049-CMYK-4000px-0039.png"
                                       data-video="../img/gemstones/detail_marcasite/Baguette_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Baguette_Cut-Marcasite-19049-CMYK-4000px-0039.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="2.2x1.2" data-foo="0">2.2x1.2 mm</option>
                        <option value="2.3x1.3" data-foo="1">2.3x1.3 mm</option>
                        <option value="2.4x1.4" data-foo="2">2.4x1.4 mm</option>
                        <option value="2.5x1.5" data-foo="3">2.5x1.5 mm</option>
                        <option value="4.0x2.0" data-foo="4">4.0x2.0 mm</option>
                      </select>
                      <br>
                    </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 4 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Marcasite_Rose_Cut-Marcasite-RGB-300px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Marcasite_Rose_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Marcasite_Rose_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Rose Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_RoseCut_00114.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Marcasite_Rose_Cut-Marcasite-RGB-300px-0039.png"
                                       data-video="../img/gemstones/detail_marcasite/Marcasite_Rose_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Marcasite_Rose_Cut-Marcasite-RGB-300px-0039.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="2.50" data-foo="0">2.50 mm</option>
                        <option value="3.00" data-foo="1">3.00 mm</option>
                        <option value="4.00" data-foo="2">4.00 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 5 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Table-29932-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Round_Table_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Round_Table_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Round Table Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_Marcasite_RoundTableCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Table-29932-FULLRES.png"
                                       data-video="../img/gemstones/detail_marcasite/Round_Table_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Table-29932-FULLRES.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="1.30" data-foo="0">1.30 mm</option>
                        <option value="1.35" data-foo="1">1.35 mm</option>
                        <option value="1.40" data-foo="2">1.40 mm</option>
                        <option value="1.50" data-foo="3">1.50 mm</option>
                        <option value="1.60" data-foo="4">1.60 mm</option>
                        <option value="1.70" data-foo="5">1.70 mm</option>
                        <option value="1.80" data-foo="6">1.80 mm</option>
                        <option value="1.90" data-foo="7">1.90 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 6 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Pure_Brilliance-34416-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Round_Brilliant_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Round_Brilliant_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Round Brilliant Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_Marcasite_RoundBrilliantCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Pure_Brilliance-34416-FULLRES.png"
                                       data-video="../img/gemstones/detail_marcasite/Round_Brilliant_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Pure_Brilliance-34416-FULLRES.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="1.25" data-foo="0">1.25 mm</option>
                        <option value="1.50" data-foo="1">1.50 mm</option>
                        <option value="1.75" data-foo="2">1.75 mm</option>
                        <option value="2.00" data-foo="3">2.00 mm</option>
                        <option value="2.25" data-foo="4">2.25 mm</option>
                        <option value="2.50" data-foo="5">2.50 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 7 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Fan-34393-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Fan_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Fan_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>FAN CUT</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_Fan_Cut_0308.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Fan-34393-FULLRES.png"
                                       data-video="../img/gemstones/detail_marcasite/Fan_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Fan-34393-FULLRES.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="4x3" data-foo="0">4x3 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 8 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Shield-Marcasite-68012-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Shield_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Shield_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Shield Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/K2515_SHIELD_CUT.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Shield-Marcasite-68012-FULLRES.png"
                                       data-video="../img/gemstones/detail_marcasite/Shield_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Shield-Marcasite-68012-FULLRES.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="3.50" data-foo="0">3.50 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 9 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Checkerboard-Marcasite-00109-RGB-4000px-0043.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Marcasite-Checkerboard-00109-FULLRES.mp4_large_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Marcasite-Checkerboard-00109-FULLRES.mp4_large_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Checkerboard Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/checkerboard-cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Checkerboard-Marcasite-00109-RGB-4000px-0043.png"
                                       data-video="../img/gemstones/detail_marcasite/Marcasite-Checkerboard-00109-FULLRES.mp4_large_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Checkerboard-Marcasite-00109-RGB-4000px-0043.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="2.50" data-foo="0">2.50 mm</option>
                        <option value="3.00" data-foo="1">3.00 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 10 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Honeycomb-Marcasite-55001-RGB-4000px-0043.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Marcasite-Honeycomb-55001-FULLRES.mp4_large_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Marcasite-Honeycomb-55001-FULLRES.mp4_large_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Honeycomb Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/honeycomb_cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Honeycomb-Marcasite-55001-RGB-4000px-0043.png"
                                       data-video="../img/gemstones/detail_marcasite/Marcasite-Honeycomb-55001-FULLRES.mp4_large_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Honeycomb-Marcasite-55001-RGB-4000px-0043.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="3.00" data-foo="1">3.00 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 11 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Dome-Marcasite-55002-RGB-4000px-0043.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Marcasite-Dome-55002-FULLRES.mp4_large_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Marcasite-Dome-55002-FULLRES.mp4_large_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Dome Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/dome_cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Dome-Marcasite-55002-RGB-4000px-0043.png"
                                       data-video="../img/gemstones/detail_marcasite/Marcasite-Dome-55002-FULLRES.mp4_large_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Dome-Marcasite-55002-RGB-4000px-0043.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="3.50" data-foo="1">3.50 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 12 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_marcasite/big_img/Marcasite-Peak-Marcasite-04063-RGB-4000px-0043.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_marcasite/Marcasite-Peak-04063-FULLRES.mp4_large_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_marcasite/Marcasite-Peak-04063-FULLRES.mp4_large_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Marcasite                                                
                           <h2>Peak Cut</h2>
                     </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_marcasite/peak_cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_marcasite/big_img/Marcasite-Peak-Marcasite-04063-RGB-4000px-0043.png"
                                       data-video="../img/gemstones/detail_marcasite/Marcasite-Peak-04063-FULLRES.mp4_large_1.mp4"
                                    src="../img/gemstones/detail_marcasite/Marcasite-Peak-Marcasite-04063-RGB-4000px-0043.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="2.50" data-foo="0">2.50 mm</option>
                        <option value="3.00" data-foo="1">3.00 mm</option>
                      </select>
                      <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Marcasite</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <!-- <div class="borderblack screen"></div> -->
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

      </div>
   </div>
   <!--<div class="row contentrow"></div>-->
   <!--UdmComment-->
</div>
            <!--UdmComment-->
         </div>
         <!-- Return To Top ================================================== -->
         <div class="component-return-to-top clearfix" id="component-return-to-top">
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
 $(document).on("click", ".colorselect .left", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left; - 1 * position >= 48 ? ($(this).parent().find(".coloritems").css("left", position + 48), $(this).parent().find(".colorselect .right").removeClass("arrowhide")) : $(this).parent().find(".colorselect .left").addClass("arrowhide")
        }), 
        $(document).on("click", ".colorselect .right", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left,
                maxleft = $(this).parent().find(".coloritems").attr("rel");
            maxleft > -1 * position && maxleft >= 0 ? ($(this).parent().find(".coloritems").css("left", position - 48), $(this).parent().find(".colorselect .left").removeClass("arrowhide")) : $(this).parent().find(".colorselect .right").addClass("arrowhide")
        });

      $(document).on("click", ".coloritems a", function(event) {
            var imgUrl  = $(this).find('img.lazyloaded').data('src');
            var videoUrl  = $(this).find('img.lazyloaded').data('video');
            var parentElement = $(this).closest('.coloritems');
            parentElement.find('li').removeClass('active');
            // $('.coloritems li').removeClass('active'); 
            $(this).parent('li').addClass('active');
            var colorUrl = $(this).data('color');
            $(this).closest('.choice_gems').find('.colorshare .cutcolor a').html(colorUrl.replace("-"," "));
            var Component_Product =  $(this).closest('.component-product');
            Component_Product.find('.cutpreview').find('img').attr('src','');
            Component_Product.find('.cutpreview').find('video').attr('src', '');
            Component_Product.find('.cutpreview').find('img').attr('src', imgUrl);
            Component_Product.find('.cutpreview').find('video').attr('src', videoUrl);
            Component_Product.find('.cutpreview').find('img').css('display', 'block');
            Component_Product.find('.cutpreview').find('video').css('display', 'none');
           Component_Product.find('.cutpreview').css('background', 'transparent url(../img/gemstones/detail/grey.jpg) 15px 0 repeat-y;ne');
        })

          // $(document).on('mousemove',".cutpreview",function(){
          //      $(this).find('img').css('display', 'none');
          //      $(this).find('video').css('display', 'block');
          //      $(this).css('background', 'none');
          // })

      </script>
@endsection