@extends('layouts.master')
@section('styles')
      <title>SMOKY QUARTZ</title>
      <link rel="stylesheet" href="../css/smoky_quartz.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <!-- Page Content -->
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
              @include('our_products.menu_left')
            </div>
            <!--/UdmComment-->
         <div class="article col-xs-12 col-sm-12 col-md-10">
   <!--/UdmComment-->
   <div class="row">
      <div class="component-product-details col-lg-12">
         <div class="product">
            <h2 class="title">                SWAROVSKI GENUINE SMOKY QUARTZ                              </h2>
         </div>
      </div>
       @if($langNetis == 'vi')
           <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                    Mỗi viên đá Thạch Anh được cắt chính xác từ  nguyên liệu tốt nhất.
                    Màu nâu và trong suốt của khoáng chất thạch anh . Đây là một loại đá quý rất bền và có khả năng chống mài mòn .

                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>Đá dòng thạch anh giúp tập trung vào sức mạnh bên trong của người đeo, có tác dụng đối với việc chữa bệnh và tự cải thiện cơ thể.&nbsp;</p>
                    <!--  <p><a target="_self" href="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Where to buy</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
       @else
      <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                    Every Swarovski Genuine Smoky Quartz is precision-cut to optimized proportions and brilliance from only the very best raw materials. The brown, transparent variety of the quartz mineral is a very durable and resistant genuine gemstone that is perfect for daily wear. 
                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>Genuine gemstones in the quartz family are thought to focus on the wearer’s inner power toward healing and self-improvement.&nbsp;</p>
                     <!-- <p><a target="_self" href="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Where to buy</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif
      <div class="component-product-details featured col-xs-12 col-sm-12 col-md-6" style="z-index:100; margin-bottom: 30px; clear: both;">
        <!--  <a href="/contact-us/contact-us.en.html" style="padding: 0; margin: 0;">
         <button class="find-button btn btn-primary">
         Contact your local sales office
         </button>
         </a> -->
      </div>
      <div style="clear:both;float:none"></div>
      <div class="noGcn noFormatting noLinks noLists product-cutcontainer">
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_smoky_quartz/big_img/Genuine_Smoky_Quartz-Round_Star-Sand-19776-RGB-500px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_smoky_quartz/Round_Star_9.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_smoky_quartz/Round_Star_9.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Genuine Smoky Quartz                                                 
                           <h2>Round Star Cut</h2>
                        </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_smoky_quartz/K2515_Round_Star_Cut_0308_6.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6  choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="sand">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_smoky_quartz/big_img/Genuine_Smoky_Quartz-Round_Star-Sand-19776-RGB-500px-0039.png"
                                       data-video="../img/gemstones/detail_smoky_quartz/Round_Star_9.mp4"
                                    src="../img/gemstones/detail_smoky_quartz/Genuine_Smoky_Quartz-Round_Star-Sand-19776-RGB-500px-0039.png">                                                
                                  </a>
                                 </li>
                                <li >                                                
                                  <a href="javascript:void(0)" data-color="oak">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_smoky_quartz/big_img/Genuine_Smoky_Quartz-Round_Star-Oak-19419-RGB-500px-0039.png"
                                       data-video="../img/gemstones/detail_smoky_quartz/Round_Star_10.mp4"
                                    src="../img/gemstones/detail_smoky_quartz/Genuine_Smoky_Quartz-Round_Star-Oak-19419-RGB-500px-0039.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            
                           </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="1.00" data-foo="0">1.00 mm</option>
                        <option value="1.10" data-foo="1">1.10 mm</option>
                        <option value="1.25" data-foo="2">1.25 mm</option>
                        <option value="1.50" data-foo="3">1.50 mm</option>
                        <option value="1.75" data-foo="4">1.75 mm</option>
                        <option value="2.00" data-foo="5">2.00 mm</option>
                        <option value="2.25" data-foo="6">2.25 mm</option>
                        <option value="2.50" data-foo="7">2.50 mm</option>
                        <option value="3.00" data-foo="8">3.00 mm</option>
                      </select>
                      <br>
                    </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Sand</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 2 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_smoky_quartz/big_img/Genuine_Smoky_Quartz-Square_Princess-Sand-19447-RGB-500px-00.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_smoky_quartz/Square_Princess_9.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_smoky_quartz/Square_Princess_9.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Genuine Smoky Quartz                                                 
                           <h2>Square Princess Cut</h2>
                        </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_smoky_quartz/K2515_Square_Princess_Cut_0308_6.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6  choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="sand">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_smoky_quartz/big_img/Genuine_Smoky_Quartz-Square_Princess-Sand-19447-RGB-500px-00.png"
                                       data-video="../img/gemstones/detail_smoky_quartz/Square_Princess_9.mp4"
                                    src="../img/gemstones/detail_smoky_quartz/Genuine_Smoky_Quartz-Square_Princess-Sand-19447-RGB-500px-00.png">                                                
                                  </a>
                                 </li>
                                <li >                                                
                                  <a href="javascript:void(0)" data-color="oak">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_smoky_quartz/big_img/Genuine_Smoky_Quartz-Square_Princess-Oak-19449-RGB-500px-003.png"
                                       data-video="../img/gemstones/detail_smoky_quartz/Square_Princess_10.mp4"
                                    src="../img/gemstones/detail_smoky_quartz/Genuine_Smoky_Quartz-Square_Princess-Oak-19449-RGB-500px-003.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="2.00" data-foo="0">2.00 mm</option>
                        <option value="2.50" data-foo="1">2.50 mm</option>
                        <option value="3.00" data-foo="2">3.00 mm</option>
                      </select>
                      <br>
                     </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Sand</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <!-- <div class="borderblack screen"></div> -->
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
      </div>
   </div>
   <!--<div class="row contentrow"></div>-->
   <!--UdmComment-->
</div>
            <!--UdmComment-->
         </div>
         <!-- Return To Top ================================================== -->
         <div class="component-return-to-top clearfix" id="component-return-to-top">
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
 $(document).on("click", ".colorselect .left", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left; - 1 * position >= 48 ? ($(this).parent().find(".coloritems").css("left", position + 48), $(this).parent().find(".colorselect .right").removeClass("arrowhide")) : $(this).parent().find(".colorselect .left").addClass("arrowhide")
        }), 
        $(document).on("click", ".colorselect .right", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left,
                maxleft = $(this).parent().find(".coloritems").attr("rel");
            maxleft > -1 * position && maxleft >= 0 ? ($(this).parent().find(".coloritems").css("left", position - 48), $(this).parent().find(".colorselect .left").removeClass("arrowhide")) : $(this).parent().find(".colorselect .right").addClass("arrowhide")
        });


          $(document).on("click", ".coloritems a", function(event) {
            var imgUrl  = $(this).find('img.lazyloaded').data('src');
            var videoUrl  = $(this).find('img.lazyloaded').data('video');
            var parentElement = $(this).closest('.coloritems');
            parentElement.find('li').removeClass('active');
            // $('.coloritems li').removeClass('active'); 
            $(this).parent('li').addClass('active');
            var colorUrl = $(this).data('color');
            $(this).closest('.choice_gems').find('.colorshare .cutcolor a').html(colorUrl.replace("-"," "));
            var Component_Product =  $(this).closest('.component-product');
            Component_Product.find('.cutpreview').find('img').attr('src','');
            Component_Product.find('.cutpreview').find('video').attr('src', '');
            Component_Product.find('.cutpreview').find('img').attr('src', imgUrl);
            Component_Product.find('.cutpreview').find('video').attr('src', videoUrl);
            Component_Product.find('.cutpreview').find('img').css('display', 'block');
            Component_Product.find('.cutpreview').find('video').css('display', 'none');
           Component_Product.find('.cutpreview').css('background', 'transparent url(../img/gemstones/detail/grey.jpg) 15px 0 repeat-y;ne');
        })

          // $(document).on('mousemove',".cutpreview",function(){
          //      $(this).find('img').css('display', 'none');
          //      $(this).find('video').css('display', 'block');
          //      $(this).css('background', 'none');
          // })

      </script>
@endsection