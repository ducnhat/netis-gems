@extends('layouts.master')
@section('styles')
      <title>{{trans('products.created_stones')}}</title>
      <link rel="stylesheet" href="css/genuine_gemstones.css">
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <!-- Page Content -->
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
               @include('our_products.menu_left')
            </div>
            <!--/UdmComment-->
            <div class="col-xs-12 col-md-10 no-padding-left no-padding-right">
               <div class="component-promo featured hero col-xs-12 col-md-12 no-margin">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                           <img alt="Swarovski Zirconia" class="img-responsive lazyloaded" src="img/createdstones/bg_img.jpg">           
                        </div>
                     </div>
                     @if($langNetis == 'vi')
                       <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto;">
                        <div class="component-promo-text">
                           <p class="slide-meta hidden"></p>
                           <p><!-- Đá nhân tạo của Swarovski có chất lượng và độ chiếu sáng đặt biệt hiếm thấy. Hãng chỉ sử dụng nguyên liệu thô tốt nhất và kỹ thuật cắt chính xác nhất trong quá trình sản xuất đá . Chúng tôi cam kết liên tục hoàn thiện kỹ thuật. Mỗi năm, Swarovski đều giối thiệu bộ sưu tập các nét cắt và màu sắc mới của Đá giúp khám phá hơn nữa các sáng tạo của ngành trang sức và thiết kế đồng hồ. --><br><a target="_blank" href="https://www.swarovski-gemstones.com/download/SWAROVSKI_ZIRCONIA_2018_ONLINE-CATALOG.pdf" class="" >Xem Zirconia Product Catalog (incl. Nano) 2018/2019&nbsp;</a><br></p>
                        </div>
                     </div>
                     @else
                     <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto;">
                        <div class="component-promo-text">
                           <p class="slide-meta hidden"></p>
                           <p><!-- Our Created Stones are unique in their character, quality, and brilliance. We use only the finest raw materials and the most precise cutting techniques in the production of our stones, reflecting our continuous commitment to technical perfection. Each year, Swarovski launches a new collection of Created Stone cuts and colors to help explore the creative boundaries of jewelry and watch design --><br><a target="_blank" href="https://www.swarovski-gemstones.com/download/SWAROVSKI_ZIRCONIA_2018_ONLINE-CATALOG.pdf" class="" >View Zirconia Product Catalog (incl. Nano) 2018/2019&nbsp;</a><br></p>
                        </div>
                     </div>
                     @endif
                  </div>
               </div>
               <hr>
               <div class="component-promo featured col-xs-12 text-center">
                  <h2 class="title alt" style="text-transform: uppercase;">SWAROVSKI {{trans('products.created_stones')}}<br></h2>
                  <div class="col-md-8 col-md-offset-2 text-center"></div>
               </div>
               <div class="row">
               <div class="component-promo featured col-xs-12 col-sm-4">
                  <div class="row black">
                     <div class="col-xs-12">
                        <a href="/zirconia" target="">
                           <div class="image-container">
                              <img alt="Swarovski Zirconia" class="img-responsive lazyloaded" src="img/createdstones/zirconia.png">                            
                           </div>
                        </a>
                     </div>
                     <div class="col-xs-12 text-center">
                        <div class="component-promo-text">                      
                           <p class="slide-meta ">                                                                                </p>                       
                           <h4 class="title">SWAROVSKI<br>ZIRCONIA<br></h4>
                           <div>
                              <p><br></p>
                           </div>
                           <p class="related-link">                            <a href="/zirconia" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="component-promo featured col-xs-12 col-sm-4">
                  <div class="row black">
                     <div class="col-xs-12">
                        <a href="/nano" target="">
                           <div class="image-container">
                              <img alt="Alpinite Castable" class=" img-responsive lazyloaded" src="img/createdstones/castable.jpg">                            
                           </div>
                        </a>
                     </div>
                     <div class="col-xs-12 text-center">
                        <div class="component-promo-text">                      
                           <p class="slide-meta ">                                                                                </p>                       
                           <h4 class="title">Nano&nbsp;<br><br></h4>
                           <div>
                              <p><br></p>
                           </div>
                           <p class="related-link">                            <a href="/nano" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                        </div>
                     </div>
                  </div>
               </div>
              


            </div>
               <!--UdmComment-->

               <!--/UdmComment-->
            </div>
            <!--UdmComment-->
         </div>
      </div>
   </div>
</div>
@endsection