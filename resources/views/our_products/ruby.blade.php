@extends('layouts.master')
@section('styles')
      <title>RUBY</title>
      <link rel="stylesheet" href="../css/sapphire.css">
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <!-- Page Content -->
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
              @include('our_products.menu_left')
            </div>
            <!--/UdmComment-->
         <div class="article col-xs-12 col-sm-12 col-md-10">
   <!--/UdmComment-->
   <div class="row">
      <div class="component-product-details col-lg-12">
         <div class="product">
            <h2 class="title">                SWAROVSKI GENUINE RUBY                              </h2>
         </div>
      </div>
      @if($langNetis == 'vi')

        <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                      Ruby là một trong những loại đá đẹp và quý giá nhất của trái đất.
                      Các kỹ thuật viên có tay nghề cao của Swarovski đặc biệt chú ý đến sự đối xứng của các viên đá ruby được chọn.
                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>Họ cắt và sắp xếp các cạnh một cách chính xác để tạo ra viên đá đẳng cấp, rực rỡ nhất.&nbsp;</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @else       
      <div class="component-product-details col-xs-12 col-sm-12 col-md-12" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                      <p>
                     Rubies are one of the earth‘s most beautiful and treasured genuine gemstones. Swarovski’s skilled technicians pay special attention to the exact symmetry of selected rubies, cutting and aligning the facets precisely to create superlative, intensely brilliant stones, which are naturally radiant and richly colored.
                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif



      <div class="component-product-details featured col-xs-12 col-sm-12 col-md-6" style="z-index:100; margin-bottom: 30px; clear: both;">
         <!-- <a href="/contact-us/contact-us.en.html" style="padding: 0; margin: 0;">
         <button class="find-button btn btn-primary">
         Contact your local sales office
         </button>
         </a> -->
      </div>
      <div style="clear:both;float:none"></div>
      <div class="noGcn noFormatting noLinks noLists product-cutcontainer" >
         <div class="component-product featured col-sm-12 col-md-12 col-lg-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                    <img src="../img/gemstones/detail_ruby/big_img/CCG_Ruby-Round_Pure_Brilliance-Bright_Ruby.png" alt="">
                    <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_ruby/big_img/Genuine_Ruby-Round_Brilliant-Bright_Red-12442-FULLRES.mp4_la.mp4">
                       <source type="video/mp4" src="../img/gemstones/detail_ruby/big_img/Genuine_Ruby-Round_Brilliant-Bright_Red-12442-FULLRES.mp4_la.mp4">
                    </video>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6">
                        <div style="position:relative;">
                           Genuine Ruby                        
                           <h2>Round Brilliant Cut</h2>
                        </div>
                     <img class="img-fluid"  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail/3d_cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                         <!--   <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="bright-red">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_ruby/big_img/CCG_Ruby-Round_Pure_Brilliance-Bright_Ruby.png"
                                       data-video="../img/gemstones/detail_ruby/big_img/Genuine_Ruby-Round_Brilliant-Bright_Red-12442-FULLRES.mp4_la.mp4"
                                    src="../img/gemstones/detail_ruby/CCG_Ruby-Round_Pure_Brilliance-Bright_Ruby.png">                                                
                                  </a>
                                 </li>
                                 <li>                                                
                                  <a href="javascript:void(0)" data-color="top-ruby">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_ruby/big_img/CCG_Ruby-Round_Pure_Brilliance-Top_Ruby.png"
                                 data-video="../img/gemstones/detail_ruby/big_img/Genuine_Ruby-Round_Brilliant-Top_Ruby-12492-FULLRES.mp4_larg.mp4"
                                  src="../img/gemstones/detail_ruby/CCG_Ruby-Round_Pure_Brilliance-Top_Ruby.png">                                                </a>
                                 </li>
                                 <li>                                                
                                  <a href="javascript:void(0)" data-color="top-red">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_ruby/big_img/CCG_Ruby-Round_Pure_Brilliance-Top_Red.png"
                                  data-video="../img/gemstones/detail_ruby/big_img/Genuine_Ruby-Round_Brilliant-Top_Red-12542-FULLRES.mp4_large.mp4"
                                  src="../img/gemstones/detail_ruby/CCG_Ruby-Round_Pure_Brilliance-Top_Red.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                         <!--   <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                      <select>
                        <option value="0.70" data-foo="0">0.70 mm</option>
                        <option value="0.80" data-foo="1">0.80 mm</option>
                        <option value="0.90" data-foo="2">0.90 mm</option>
                        <option value="1.00" data-foo="3">1.00 mm</option>
                        <option value="1.10" data-foo="4">1.10 mm</option>
                        <option value="1.20" data-foo="5">1.20 mm</option>
                        <option value="1.25" data-foo="6">1.25 mm</option>
                        <option value="1.30" data-foo="7">1.30 mm</option>
                        <option value="1.40" data-foo="8">1.40 mm</option>
                        <option value="1.50" data-foo="9">1.50 mm</option>
                        <option value="1.60" data-foo="10">1.60 mm</option>
                        <option value="1.70" data-foo="11">1.70 mm</option>
                        <option value="1.75" data-foo="12">1.75 mm</option>
                        <option value="1.80" data-foo="13">1.80 mm</option>
                        <option value="1.90" data-foo="14">1.90 mm</option>
                        <option value="2.00" data-foo="15">2.00 mm</option>
                      </select>
                      <br>
                    </span>
                    
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="#">bright red</a></span>
                     </div>
                   
                     <span class="addinfos"></span>                
                  </div>
                  </div>
                </div>  
               </div>
              
            </div>
            <!-- <div class="borderblack screen"></div> -->
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>
      </div>
   </div>
   <!--<div class="row contentrow"></div>-->
   <!--UdmComment-->
</div>
            <!--UdmComment-->
         </div>
         <!-- Return To Top ================================================== -->
         <div class="component-return-to-top clearfix" id="component-return-to-top">
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
      $('ul.coloritems').filter(function(){
         if($(this).children("li").length <= 5){
               $(this).css('marginLeft', '0px');
               // $(this).closest('.colorselect').find('.select-color').css('marginLeft', '0px');
         } 
      })  
   });

 $(document).on("click", ".colorselect .left", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left; - 1 * position >= 48 ? ($(this).parent().find(".coloritems").css("left", position + 48), $(this).parent().find(".colorselect .right").removeClass("arrowhide")) : $(this).parent().find(".colorselect .left").addClass("arrowhide")
        }), 
        $(document).on("click", ".colorselect .right", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left,
                maxleft = $(this).parent().find(".coloritems").attr("rel");
            maxleft > -1 * position && maxleft >= 0 ? ($(this).parent().find(".coloritems").css("left", position - 48), $(this).parent().find(".colorselect .left").removeClass("arrowhide")) : $(this).parent().find(".colorselect .right").addClass("arrowhide")
        });


         $(document).on("click", ".coloritems a", function(event) {
            var imgUrl  = $(this).find('img.lazyloaded').data('src');
            var videoUrl  = $(this).find('img.lazyloaded').data('video');
            $('.coloritems li').removeClass('active'); 
            $(this).parent('li').addClass('active');
            var colorUrl = $(this).data('color');
            $('.colorshare .cutcolor a').html(colorUrl.replace("-"," "));
          
            $('.cutpreview').find('img').attr('src','');
            $('.cutpreview').find('video').attr('src', '');
            $('.cutpreview').find('img').attr('src', imgUrl);
            $('.cutpreview').find('video').attr('src', videoUrl);
            $('.cutpreview').find('img').css('display', 'block');
            $('.cutpreview').find('video').css('display', 'none');
            $('.cutpreview').css('background', 'transparent url(../img/gemstones/detail/grey.jpg) 15px 0 repeat-y;ne');
        })

          // $(document).on('mousemove',".cutpreview img",function(){
          //      $('.cutpreview').find('img').css('display', 'none');
          //      $('.cutpreview').find('video').css('display', 'block');
          //      $('.cutpreview').css('background', 'none');
          // })

      </script>
@endsection