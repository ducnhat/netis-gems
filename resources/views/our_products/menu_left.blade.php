<style>
  @media (max-width: 767px) {
      #component-sidebar-menu{
         display: none;
      }
   }
  @media (max-width: 480px) {
      #component-sidebar-menu{
         display: none;
      }
   }
</style>
<div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
   <div class="btn-group dropdown">
      <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">
         <li class="subnav" role="menuitem">
            <h5 class="title ">
               <a href="#" class="">
                   {{trans('products.products_home')}}
               </a>
            </h5>
            <ul class="dropdown-menu">
               <li role="menuitem" class="folder">
                  <a href="/genuine-gemstones" class="">{{trans('products.genuine_gemstones')}}</a>
                  <ul class="dropdown-menu">
                     <li role="menuitem" class="folder" style="text-transform: none;">
                        <a href="/sapphire" class="">
                          Swarovski Sapphire
                        </a>
                     </li>
                     <li role="menuitem" class="folder" style="text-transform: none;">
                        <a href="/ruby" class="">
                          Swarovski Ruby
                        </a> 
                     </li>
                     <li role="menuitem" class="folder" style="text-transform: none;">             
                        <a href="/topaz" class="">
                          Swarovski Topaz                  
                        </a>              
                     </li>
                     <li role="menuitem" class="folder" style="text-transform: none;">             
                        <a href="/black_spinel" class="">
                          Swarovski Black Spinel                 
                        </a>              
                     </li>
                     <li role="menuitem" class="folder" style="text-transform: none;">             
                        <a href="/smoky_quartz" class="">                    
                          Swarovski Smoky Quartz                 
                        </a>              
                     </li>
                     <li role="menuitem" class="folder" style="text-transform: none;">             
                        <a href="/marcasite" class="">
                          Swarovski Marcasite                 
                        </a>              
                     </li>
                  </ul>
               </li>
               <li role="menuitem" class="folder">
                  <a href="/created-stones" class=""> 
                    {{trans('products.created_stones')}}                  
                  </a>                          
                  <ul class="dropdown-menu">
                     <li role="menuitem" class="folder" style="text-transform: none;">           
                        <a href="/zirconia" class="">
                          Swarovski Zirconia                  
                        </a>               
                     </li>
                     <li role="menuitem" class="folder" style="text-transform: none;">             
                        <a href="/nano" class="">
                           Swarovski Nano                
                        </a>
                     </li>
                  </ul>
               </li>
            </ul>
         </li>
      </ul>
   </div>
</div>