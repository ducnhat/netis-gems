@extends('layouts.master')
@section('styles')
<title>SAPPHIRE</title>
<link rel="stylesheet" href="../css/sapphire.css">
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
               @include('our_products.menu_left')
            </div>
          <div class="article col-xs-12 col-sm-12 col-md-10">
            <div class="row">
              <div class="component-product-details col-lg-12">
                 <div class="product">
                    <h2 class="title">SWAROVSKI GENUINE SAPPHIRE                                
                    </h2>
                 </div>
              </div>
              <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
                 <div class=" product">
                    <div class="col-xs-12">
                       <div class="component-product-text-gem">
                          <p class="slide-meta hidden"></p>
                          <div class="noGcn">
                             @if($langNetis == 'vi')
                              <p>Đá Sappires luôn quyến rũ và là niềm ao ước của giới hoàng tộc. Hơn nữa, Sapphires là biểu tượng cho nét thanh lịch cùng với sự chân thành, tình yêu, niềm vui bất tận. &nbsp;
                             </p>
                             @else
                             <p>Alluring sapphires, always desired by royalty and aristocracy, are an elegant and sublime symbol of sincerity, love, joy, faithfulness, and truth.As the hardest mineral after diamonds, sapphires are durable, rare, and eternally beautiful. &nbsp;
                             </p>
                             @endif
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
              <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
                 <div class="">
                    <div class="col-xs-12">
                       <div class="component-product-text-gem">
                          <p class="slide-meta hidden"></p>
                          <div class="noGcn">
                              @if($langNetis == 'vi')
                              <p>Đá Sapphires của Swarovski được cắt và đánh bóng tại các cơ sở hiện đại của công ty giúp tôn lên màu sắc quý phái và vẻ đẹp của từng viên đá.&nbsp;</p>
                              @else
                             <p> Swarovski Genuine Sapphires are precision-cut and polished in the company’s state-of-the-art facilities to reveal the precious color and individual beauty of each stone.&nbsp;</p>
                             @endif
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
              <div class="noGcn noFormatting noLinks noLists product-cutcontainer" >
                 <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" >
                    <div class="row">
                       <div class="borderblack mobile"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                          <div class="cutpreview_children">
                            <img src="../img/gemstones/detail/big_img/Genuine_big_size_Very_Light_Blue-CMYK-2.png" alt="">
                            <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail/Very_Light_Blue---Round_Brilliant_1.mp4">
                            
                            </video>
                          </div>
                        </div>
                       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                        <div class="cutoverview_child ">
                          <div class="row">
                          <div class="col-sm-12 col-md-6 col-lg-6 ">
                             
                              <div style="position:relative;">
                                 Genuine Sapphire                        
                                 <h2>Round Brilliant Cut</h2>
                              </div>
                         
                             <img class="lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail/3d_cut.png">                        
                          </div>
                          <div class="col-sm-12 col-md-6 col-lg-6 ">
                             <div class="row color-row justify-content-center">
                             <div class="colorselect">
                                <div class="select-color"> {{trans('products.select_color')}}:</div>
                                <ul>
                                  <li class="left">
                                    <a href="#">
                                      <img src="../img/gemstones/detail/arrow-left.png" title="Scroll left">
                                    </a>                            
                                  </li>
                                  <li>
                                    <ul class="coloritems" rel="435" value="1">
                                      <li class="active">
                                        <a href="javascript:void(0)" data-color="very-light-blue">
                                          <span class="glow"></span>
                                          <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"   data-src="../img/gemstones/detail/big_img/Genuine_big_size_Very_Light_Blue-CMYK-2.png" data-video="../img/gemstones/detail/Very_Light_Blue---Round_Brilliant_1.mp4"
                                          src="../img/gemstones/detail/Genuine_Very_Light_Blue-CMYK-2.png">
                                        </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0)" data-color="pastel-light"><span class="glow"></span>
                                          <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                          data-src="../img/gemstones/detail/big_img/Genuine_Blue_Sapphire-Round_Brilliant-Pastel_Light-CMYK-2000.png"
                                         data-video="../img/gemstones/detail/big_img/Pastel_Light---Round_Brilliant_1.mp4"
                                          src="../img/gemstones/detail/Genuine_Brilliant-Pastel_Light-CMYK-2000.png">
                                          </a>
                                      </li>
                                      <li>
                                        <a href="javascript:void(0)" data-color="pastel-blue">
                                          <span class="glow"></span>
                                          <img class="  lazyloaded" 
                                         data-src="../img/gemstones/detail/big_img/Genuine_Blue_Sapphire-Round_Brilliant-Pastel_Blue-CMYK-2000p.png"
                                          data-video="../img/gemstones/detail/big_img/Pastel_Blue---Round_Brilliant_1.mp4"
                                          src="../img/gemstones/detail/Genuine_Pastel_Blue-CMYK-2000p.png">
                                        </a>
                                      </li>
                                      <li>
                                        <a href="javascript:void(0)" data-color="bright-blue">
                                          <span class="glow"></span>                          
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Blue_Sapphire-Round_Brilliant-Bright_Blue-CMYK-2000p.png"
                                          data-video="../img/gemstones/detail/big_img/Bright_Blue---Round_Brilliant_1.mp4"
                                          src="../img/gemstones/detail/Genuine_Bright_Blue-CMYK-2000p.png">           
                                        </a>
                                      </li>
                                      <li>
                                        <a href="javascript:void(0)" data-color="top-blue">  
                                          <span class="glow"></span>                          
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Blue_Sapphire-Round_Brilliant-Top_Blue-CMYK-2000px-0.png"
                                          data-video="../img/gemstones/detail/big_img/Top_Blue---Round_Brilliant_1.mp4" src="../img/gemstones/detail/GenuineBrilliant-Top_Blue-CMYK-2000px-0.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="very-light-pink"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Pink_Sapphire-Round_Brilliant-Very_Light_Pink-CMYK-2.png"
                                          data-video="../img/gemstones/detail/big_img/Very_Light_Pink---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Very_Light_Pink-CMYK-2.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="pastel-light"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Pink_Sapphire-Round_Brilliant-Pastel_Light-CMYK-1000.png"
                                          data-video="../img/gemstones/detail/big_img/Pastel_Light---Round_Brilliant_2.mp4" src="../img/gemstones/detail/Genuine_Pastel_Light-CMYK-1000.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="pastel-red"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Pink_Sapphire-Round_Brilliant-Pastel_Red-CMYK-2000px.png"
                                          data-video="../img/gemstones/detail/big_img/Pastel_Light---Round_Brilliant_2.mp4" src="../img/gemstones/detail/Genuine_Pink_Sapphire-Round_Brilliant-Pastel_Red-CMYK-2000px.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="pink-light"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Pink_Sapphire-Round_Brilliant-Pink_Light-CMYK-2000px.png"
                                          data-video="../img/gemstones/detail/big_img/Pink_Light---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Pink_Sapphire-Round_Brilliant-Pink_Light-CMYK-2000px.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="pink-medium"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Pink_Sapphire-Round_Brilliant-Pink_Medium-CMYK-2000p.png"
                                          data-video="../img/gemstones/detail/big_img/Pink_Medium---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Pink_Sapphire-Round_Brilliant-Pink_Medium-CMYK-2000p.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="light-yellow"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Yellow_Sapphire-Round_Brilliant-Light_Yellow-28164-R.png"
                                          data-video="../img/gemstones/detail/big_img/Light_Yellow---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Yellow_Sapphire-Round_Brilliant-Light_Yellow-28164-R.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="canary-yellow"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Yellow_Sapphire-Round_Brilliant-Canary_Yellow-26872-.png"
                                          data-video="../img/gemstones/detail/big_img/Canary_Yellow---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Yellow_Sapphire-Round_Brilliant-Canary_Yellow-26872-.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="bright-yellow"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Yellow_Sapphire-Round_Brilliant-Bright_Yellow-25583-.png"
                                          data-video="../img/gemstones/detail/big_img/Bright_Yellow---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Yellow_Sapphire-Round_Brilliant-Bright_Yellow-25583-.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="classic-yellow"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Yellow_Sapphire-Round_Brilliant-Classic-Yellow-RGB-5.png"
                                          data-video="../img/gemstones/detail/big_img/Classic_Yellow---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Yellow_Sapphire-Round_Brilliant-Classic-Yellow-RGB-5.png">
                                        </a>
                                      </li>
                                      <li>                                               
                                        <a href="javascript:void(0)" data-color="bright-orange"><span class="glow"></span>
                                          <img class="  lazyloaded" data-src="../img/gemstones/detail/big_img/Genuine_Yellow_Sapphire-Round_Brilliant-Bright_Orange-25635-.png"
                                          data-video="../img/gemstones/detail/big_img/Canary_Yellow---Round_Brilliant_1.mp4" src="../img/gemstones/detail/Genuine_Yellow_Sapphire-Round_Brilliant-Bright_Orange-25635-.png">
                                        </a>
                                      </li>
                                    </ul>
                                  </li>
                                   <li class="right">                               
                                    <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            
                                   </li>
                                </ul>
                              </div>
                            </div>
                            <span class="cutsize"> {{trans('products.select_size')}}<br></span>
                            <span class="cutsizes">
                              <select>
                                <option value="0.70" data-foo="0">0.70 mm</option>
                                <option value="0.80" data-foo="1">0.80 mm</option>
                                <option value="0.90" data-foo="2">0.90 mm</option>
                                <option value="1.00" data-foo="3">1.00 mm</option>
                                <option value="1.10" data-foo="4">1.10 mm</option>
                                <option value="1.20" data-foo="5">1.20 mm</option>
                                <option value="1.25" data-foo="6">1.25 mm</option>
                                <option value="1.30" data-foo="7">1.30 mm</option>
                                <option value="1.40" data-foo="8">1.40 mm</option>
                                <option value="1.50" data-foo="9">1.50 mm</option>
                                <option value="1.60" data-foo="10">1.60 mm</option>
                                <option value="1.70" data-foo="11">1.70 mm</option>
                                <option value="1.75" data-foo="12">1.75 mm</option>
                                <option value="1.80" data-foo="13">1.80 mm</option>
                                <option value="1.90" data-foo="14">1.90 mm</option>
                                <option value="2.00" data-foo="15">2.00 mm</option>
                              </select>
                              <br>
                            </span>
                            <div class="colorshare">
                              <span class="cutcolor">
                                <a href="javascript:void(0)">Very Light Blue</a>
                              </span>
                            </div>
                            <span class="addinfos"></span>                
                          </div>
                        </div>
                        </div>  
                       </div>    
                    </div>
                    <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        
                    <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>       <span class="hidden" itemprop="sku"></span>        
                    <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
                 </div>
              </div>
            </div>
          </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
 $(document).on("click", ".colorselect .left", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left; - 1 * position >= 48 ? ($(this).parent().find(".coloritems").css("left", position + 48), $(this).parent().find(".colorselect .right").removeClass("arrowhide")) : $(this).parent().find(".colorselect .left").addClass("arrowhide")
        }), 
        $(document).on("click", ".colorselect .right", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left,
                maxleft = $(this).parent().find(".coloritems").attr("rel");
            maxleft > -1 * position && maxleft >= 0 ? ($(this).parent().find(".coloritems").css("left", position - 48), $(this).parent().find(".colorselect .left").removeClass("arrowhide")) : $(this).parent().find(".colorselect .right").addClass("arrowhide")
        });


         $(document).on("click", ".coloritems a", function(event) {
            var imgUrl  = $(this).find('img.lazyloaded').data('src');
            var videoUrl  = $(this).find('img.lazyloaded').data('video');
            $('.coloritems li').removeClass('active'); 
            $(this).parent('li').addClass('active');
            var colorUrl = $(this).data('color');
            $('.colorshare .cutcolor a').html(colorUrl.replace("-"," "));
          
            $('.cutpreview').find('img').attr('src','');
            $('.cutpreview').find('video').attr('src', '');
            $('.cutpreview').find('img').attr('src', imgUrl);
            $('.cutpreview').find('video').attr('src', videoUrl);
            $('.cutpreview').find('img').css('display', 'block');
            $('.cutpreview').find('video').css('display', 'none');
            $('.cutpreview').css('background', 'transparent url(../img/gemstones/detail/grey.jpg) 15px 0 repeat-y;ne');
        })

          // $(document).on('mousemove',".cutpreview img",function(){
          //      $('.cutpreview').find('img').css('display', 'none');
          //      $('.cutpreview').find('video').css('display', 'block');
          //      $('.cutpreview').css('background', 'none');
          // })

      </script>
@endsection