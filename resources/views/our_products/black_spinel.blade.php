@extends('layouts.master')
@section('styles')
      <title>SPINEL</title>
      <link rel="stylesheet" href="../css/black_spinel.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <!-- Page Content -->
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
               @include('our_products.menu_left')
            </div>
            <!--/UdmComment-->
         <div class="article col-xs-12 col-sm-12 col-md-10">
   <!--/UdmComment-->
   <div class="row">
      <div class="component-product-details col-lg-12">
         <div class="product">
            <h2 class="title">                SWAROVSKI GENUINE SPINEL                              </h2>
         </div>
      </div>
      @if($langNetis == 'vi')
           <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                    Mỗi viên đá Black Spinel được cắt chính xác từ  nguyên liệu tốt nhất.
                     Thuộc nhóm spinel rất cứng của khoáng sản, Black Spinel  là một loại đá quý rất bền và có khả năng chống mài mòn hoàn hảo.  &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>. Black Spinel  là loại spinel hiếm nhất được tìm thấy trong tự nhiên.&nbsp;</p>
                    <!--  <p><a target="_self" href="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Where to buy</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      @else
      <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                    Every Swarovski Genuine Black Spinel is precision-cut to optimized proportions and brilliance from only the very best raw materials. As a member of the very hard spinel group of minerals, black spinel is a very durable and resistant genuine gemstone that is perfect for daily wear. &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>Black spinel is the rarest type of spinel found in nature and is said to be a protective stone.&nbsp;</p>
                   <!--   <p><a target="_self" href="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Where to buy</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif
      <div class="component-product-details featured col-xs-12 col-sm-12 col-md-6" style="z-index:100; margin-bottom: 30px; clear: both;">
        <!--  <a href="/contact-us/contact-us.en.html" style="padding: 0; margin: 0;">
         <button class="find-button btn btn-primary">
         Contact your local sales office
         </button>
         </a> -->
      </div>
      <div style="clear:both;float:none"></div>
      <div class="noGcn noFormatting noLinks noLists product-cutcontainer">
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Round_Brilliant-Black-30538-RGB-500p_2.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_black_spinel/Genuine_Black_Spinel_Castable---Pure_Brilliance_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_black_spinel/Genuine_Black_Spinel_Castable---Pure_Brilliance_1.mp4">
                  </video>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Genuine Spinel                                                
                           <h2>Round Brilliant Cut</h2>
                        </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail/3d_cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                    <a href="javascript:void(0)" data-color="black">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Round_Brilliant-Black-30538-RGB-500p_2.png"
                                       data-video="../img/gemstones/detail_black_spinel/Genuine_Black_Spinel_Castable---Pure_Brilliance_1.mp4"
                                    src="../img/gemstones/detail_black_spinel/Genuine_Black_Spinel-Round_Brilliant-Black-30538-RGB-500p_2.png">                                                
                                 </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                       <select>
                        <option value="1.00" data-foo="0">1.00 mm</option>
                        <option value="1.10" data-foo="1">1.10 mm</option>
                        <option value="1.20" data-foo="2">1.20 mm</option>
                        <option value="1.25" data-foo="3">1.25 mm</option>
                        <option value="1.30" data-foo="4">1.30 mm</option>
                        <option value="1.40" data-foo="5">1.40 mm</option>
                        <option value="1.50" data-foo="6">1.50 mm</option>
                        <option value="1.60" data-foo="7">1.60 mm</option>
                        <option value="1.70" data-foo="8">1.70 mm</option>
                        <option value="1.75" data-foo="9">1.75 mm</option>
                        <option value="1.80" data-foo="10">1.80 mm</option>
                        <option value="1.90" data-foo="11">1.90 mm</option>
                        <option value="2.00" data-foo="12">2.00 mm</option>
                        <option value="2.10" data-foo="13">2.10 mm</option>
                        <option value="2.20" data-foo="14">2.20 mm</option>
                        <option value="2.25" data-foo="15">2.25 mm</option>
                        <option value="2.30" data-foo="16">2.30 mm</option>
                        <option value="2.40" data-foo="17">2.40 mm</option>
                        <option value="2.50" data-foo="18">2.50 mm</option>
                        <option value="2.60" data-foo="19">2.60 mm</option>
                        <option value="2.75" data-foo="20">2.75 mm</option>
                        <option value="3.00" data-foo="21">3.00 mm</option>
                       </select>
                      <br>
                    </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Black</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 2 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Square_Princess-Black-18994-RGB-500px-0.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_black_spinel/Square_Princess_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_black_spinel/Square_Princess_1.mp4">
                  </video>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Genuine Spinel                                                
                           <h2>Square Princess Cut</h2>
                        </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_black_spinel/K2515_Square_Princess_Cut_0308_8.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Square_Princess-Black-18994-RGB-500px-0.png"
                                       data-video="../img/gemstones/detail_black_spinel/Square_Princess_1.mp4"
                                    src="../img/gemstones/detail_black_spinel/Genuine_Black_Spinel-Square_Princess-Black-18994-RGB-500px-0.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="2.50" data-foo="1">2.50 mm</option>
                           <option value="3.00" data-foo="2">3.00 mm</option>
                        </select>
                        <br>
                     </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Black</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 3 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Fan-Black-25183-RGB-500px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_black_spinel/Fan_2.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_black_spinel/Fan_2.mp4">
                  </video>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Genuine Spinel                                                
                           <h2>FAN CUT</h2>
                        </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_black_spinel/K2515_Marcasite_FanCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Fan-Black-25183-RGB-500px-0039.png"
                                       data-video="../img/gemstones/detail_black_spinel/Fan_2.mp4"
                                    src="../img/gemstones/detail_black_spinel/Genuine_Black_Spinel-Fan-Black-25183-RGB-500px-0039.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="4x3" data-foo="0">4x3 mm</option>
                           <option value="6x4.5" data-foo="1">6x4.5 mm</option>
                        </select>
                        <br>
                     </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Black</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>
        <!-- 4 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Arc-Black-28568-RGB-500px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_black_spinel/Arc_6.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_black_spinel/Arc_6.mp4">
                  </video>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                                              Swarovski Genuine Spinel                                                
                           <h2>Arc Cut</h2>
                        </div>
                     <img class="  lazyloaded img-fluid" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_black_spinel/K2515_K2515_ARC_CUT_2.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                        <!--    <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="black">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_black_spinel/big_img/Genuine_Black_Spinel-Arc-Black-28568-RGB-500px-0039.png"
                                       data-video="../img/gemstones/detail_black_spinel/Arc_6.mp4"
                                    src="../img/gemstones/detail_black_spinel/Genuine_Black_Spinel-Arc-Black-28568-RGB-500px-0039.png">                                                </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="4x2" data-foo="0">4x2 mm</option>
                        </select>
                        <br>
                     </span> 
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">Black</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <!-- <div class="borderblack screen"></div> -->
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

      </div>
   </div>
   <!--<div class="row contentrow"></div>-->
   <!--UdmComment-->
</div>
            <!--UdmComment-->
         </div>
         <!-- Return To Top ================================================== -->
         <div class="component-return-to-top clearfix" id="component-return-to-top">
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
 $(document).on("click", ".colorselect .left", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left; - 1 * position >= 55 ? ($(this).parent().find(".coloritems").css("left", position + 55), $(this).parent().find(".colorselect .right").removeClass("arrowhide")) : $(this).parent().find(".colorselect .left").addClass("arrowhide")
        }), 
        $(document).on("click", ".colorselect .right", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left,
                maxleft = $(this).parent().find(".coloritems").attr("rel");
            maxleft > -1 * position && maxleft >= 0 ? ($(this).parent().find(".coloritems").css("left", position - 55), $(this).parent().find(".colorselect .left").removeClass("arrowhide")) : $(this).parent().find(".colorselect .right").addClass("arrowhide")
        });


         $(document).on("click", ".coloritems a", function(event) {
            var imgUrl  = $(this).find('img.lazyloaded').data('src');
            var videoUrl  = $(this).find('img.lazyloaded').data('video');
            var parentElement = $(this).closest('.coloritems');
            parentElement.find('li').removeClass('active');
            // $('.coloritems li').removeClass('active'); 
            $(this).parent('li').addClass('active');
            var colorUrl = $(this).data('color');
            $(this).closest('.choice_gems').find('.colorshare .cutcolor a').html(colorUrl.replace("-"," "));
            var Component_Product =  $(this).closest('.component-product');
            Component_Product.find('.cutpreview').find('img').attr('src','');
            Component_Product.find('.cutpreview').find('video').attr('src', '');
            Component_Product.find('.cutpreview').find('img').attr('src', imgUrl);
            Component_Product.find('.cutpreview').find('video').attr('src', videoUrl);
            Component_Product.find('.cutpreview').find('img').css('display', 'block');
            Component_Product.find('.cutpreview').find('video').css('display', 'none');
           Component_Product.find('.cutpreview').css('background', 'transparent url(../img/gemstones/detail/grey.jpg) 15px 0 repeat-y;ne');
        })

          // $(document).on('mousemove',".cutpreview",function(){
          //      $(this).find('img').css('display', 'none');
          //      $(this).find('video').css('display', 'block');
          //      $(this).css('background', 'none');
          // })

</script>
@endsection