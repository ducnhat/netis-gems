@extends('layouts.master')
@section('styles')
    <title>TOPAZ</title>
    <link rel="stylesheet" href="../css/topaz.css">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <!-- Page Content -->
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
               @include('our_products.menu_left')
            </div>
            <!--/UdmComment-->
         <div class="article col-xs-12 col-sm-12 col-md-10">
   <!--/UdmComment-->
   <div class="row">
      <div class="component-product-details col-lg-12">
         <div class="product">
            <h2 class="title">                SWAROVSKI GENUINE TOPAZ                              </h2>
         </div>
      </div>
       @if($langNetis == 'vi')
         <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                   Vẻ đẹp của đá quý chất phụ thuộc vào nét cắt hoàn hảo. Nổi tiếng thế giới về độ chính xác trong cắt đá trang sức, Swarovski biến đá Topaz tự nhiên thành một viên ngọc của sự hoàn hảo và làm cho sản phẩm  lấp lánh hơn bao giờ hết.
                     <br>
                  Tất cả  đá Topaz Swarovski có nguồn gốc  đá nguyên liệu tự nhiên thuần khiết nhất. Swarovski luôn cố gắng bảo vệ những món quà của thiên nhiên bằng cam kết đạt tiêu chuẩn sản xuất sinh thái và có trách nhiệm với xã hội 
                    

                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p> Ngoài Topaz trắng là màu cho những dịp sang trọng và thể hiện nữ tính thuần khiết, Đá Topaz Swarovski cũng có nhiều màu sắc tươi sáng có từ sáng chế của Swarovski lên bề mặt đá mà không cần chiếu xạ, màu sắc bền và khả năng chống mài mòn cao..&nbsp;</p>
                     <!-- <p><a target="_self" href="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Where to buy</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
       @else
      <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                     The beauty of a high quality gemstone depends on a perfect cut. Renowned as the world leader in the precision-cutting of jewelry stones, Swarovski transforms natural topaz into a beautiful gem of stunning perfection. The precise cut reveals the true brilliance of this genuine gemstone, making products made with Swarovski Genuine Topaz sparkle like never before.
                     <br>
                      All Swarovski Genuine Topaz stones used in this product are naturally occurring gemstones made from the purest raw materials. Swarovski has always tried to protect the gifts of nature and is therefore committed to ecologically and socially responsible production standards.
                        &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                      While white topaz is the color of choice for elegant occasions and represents pure femininity, Swarovski Genuine Topaz is also available in a stunning array of bright colors for a more playful look. Swarovski’s patented surface enhancement process is environmentally sound – no stone is exposed to irradiation. The colors are durable and of proven resistance for daily wear.
                      &nbsp;</p>
          <!--            <p><a target="_self" href="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Where to buy</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif
      <div class="component-product-details featured col-xs-12 col-sm-12 col-md-6" style="z-index:100; margin-bottom: 30px; clear: both;">
        <!--  <a href="/contact-us/contact-us.en.html" style="padding: 0; margin: 0;">
         <button class="find-button btn btn-primary">
         Contact your local sales office
         </button>
         </a> -->
      </div>
      <div style="clear:both;float:none"></div>
      <div class="noGcn noFormatting noLinks noLists product-cutcontainer">
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/big_img/WhiteAntique_1.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/big_img/Topaz-Pure_Brilliance-White-FULLRES.mp4_large_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/big_img/Topaz-Pure_Brilliance-White-FULLRES.mp4_large_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                           Genuine Topaz                        
                           <h2>Round Brilliant Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail/3d_cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/big_img/WhiteAntique_1.png"
                                       data-video="../img/gemstones/detail_topaz/big_img/Topaz-Pure_Brilliance-White-FULLRES.mp4_large_1.mp4"
                                    src="../img/gemstones/detail_topaz/WhiteAntique_naturalbrilliantcut.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/big_img/Topaz-Natural_Brilliance-Aqua_Blue-xxxxx-RGB-0043_v2.png"
                                 data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_Aquablue.mp4"
                                  src="../img/gemstones/detail_topaz/Topaz-Natural_Brilliance-Aqua_Blue-xxxxx-RGB-0043_v2.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Ice_Blue (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_iceblue.mp4"
                                  src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Ice_Blue.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Paraiba (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Topaz-Natural_Brilliance-Paraiba-xxxxx-FULLRES.mp4_terminal_.mp4"
                                  src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Paraiba.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_Kashmir.mp4" src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Rainforest (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_rainforest.mp4" src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Rainforest.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz-Natural_Brilliance-Honey-xxxxx-CMYK-4000px-0043 (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_Honey.mp4" src="../img/gemstones/detail_topaz/Topaz-Natural_Brilliance-Honey-xxxxx-CMYK-4000px-0043.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Poppy.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_poppy.mp4" src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Poppy.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Swarovski_Genuine-Topaz_Natural-Brilliance_Misty-Rose_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Swarovski_Genuine-Topaz_Natural-Brilliance_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/Swarovski_Genuine-Topaz_Natural-Brilliance_Misty-Rose_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Swarovski_Genuine-Topaz_Natural-Brilliance_Peach_Front.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Topaz-Natural_Brilliance-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/Swarovski_Genuine-Topaz_Natural-Brilliance_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Baby_Pink.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_babypink.mp4" src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Baby_Pink.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Pink.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance.mp4_pink.mp4" src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Pink.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Natural_Brilliance_Blazing_Red.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_Blazingred.mp4" src="../img/gemstones/detail_topaz/Topaz_Natural_Brilliance_Blazing_Red.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz-Natural_Brilliance-Violac-CMYK-2000px-0043 (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Natural_Brilliance_violac.mp4" src="../img/gemstones/detail_topaz/Topaz-Natural_Brilliance-Violac-CMYK-2000px-0043.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                       <select>
                        <option value="0.80" data-foo="0">0.80 mm</option>
                        <option value="0.90" data-foo="1">0.90 mm</option>
                        <option value="1.00" data-foo="2">1.00 mm</option>
                        <option value="1.10" data-foo="3">1.10 mm</option>
                        <option value="1.20" data-foo="4">1.20 mm</option>
                        <option value="1.25" data-foo="5">1.25 mm</option>
                        <option value="1.30" data-foo="6">1.30 mm</option>
                        <option value="1.40" data-foo="7">1.40 mm</option>
                        <option value="1.50" data-foo="8">1.50 mm</option>
                        <option value="1.60" data-foo="9">1.60 mm</option>
                        <option value="1.70" data-foo="10">1.70 mm</option>
                        <option value="1.75" data-foo="11">1.75 mm</option>
                        <option value="1.80" data-foo="12">1.80 mm</option>
                        <option value="1.90" data-foo="13">1.90 mm</option>
                        <option value="2.00" data-foo="14">2.00 mm</option>
                        <option value="2.25" data-foo="15">2.25 mm</option>
                        <option value="2.50" data-foo="16">2.50 mm</option>
                        <option value="3.00" data-foo="17">3.00 mm</option>
                        <option value="3.50" data-foo="18">3.50 mm</option>
                        <option value="4.00" data-foo="19">4.00 mm</option>
                        <option value="4.50" data-foo="20">4.50 mm</option>
                        <option value="5.00" data-foo="21">5.00 mm</option>
                        <option value="6.00" data-foo="22">6.00 mm</option>
                        <option value="7.00" data-foo="23">7.00 mm</option>
                        <option value="8.00" data-foo="24">8.00 mm</option>
                        <option value="9.00" data-foo="25">9.00 mm</option>
                        <option value="10.00" data-foo="26">10.00 mm</option>
                      </select>
                      <br>
                    </span>
                    <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                    </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>
        <!-- 2 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-White-14256-RGB-500px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/big_img/Square_Princess_white.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/big_img/../img/gemstones/detail_topaz/big_img/Square_Princess_white.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>Square Princess Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_Square_Princess_Cut_0308.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-White-14256-RGB-500px-0039.png"
                                       data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_white.mp4"
                                    src="../img/gemstones/detail_topaz/Passion_Topaz-Square_Princess-White-14256-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Aqua_Blue-21664-RGB-500px-0039.png"
                                 data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_Aquablue.mp4"
                                  src="../img/gemstones/detail_topaz/Passion_Topaz-Square_Princess-Aqua_Blue-21664-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Ice_Blue-17906-RGB-500px-0039.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_iceblue.mp4"
                                  src="../img/gemstones/detail_topaz/Passion_Topaz-Square_Princess-Ice_Blue-17906-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Paraiba-21237-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_Paraiba.mp4"
                                  src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Paraiba-21237-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Topaz_Square_Princess_Kashmir.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_Kashmir.mp4" src="../img/gemstones/detail_topaz/Topaz_Square_Princess_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Rainforest-24265-RGB-500px-003 (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_Rainforest.mp4" src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Rainforest-24265-RGB-500px-003.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Honey-23863-RGB-500px-0039.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_Honey.mp4" src="../img/gemstones/detail_topaz/Passion_Topaz-Square_Princess-Honey-23863-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Passion_Topaz-Square_Princess-Poppy-19349-RGB-500px-0039.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_poppy.mp4" src="../img/gemstones/detail_topaz/Passion_Topaz-Square_Princess-Poppy-19349-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Swarovski_Genuine-Topaz_Square-Princess_Misty-Rose_Front.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Swarovski_Genuine-Topaz_Square-Princess_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/Swarovski_Genuine-Topaz_Square-Princess_Misty-Rose_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Swarovski_Genuine-Topaz_Square-Princess_Peach_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Topaz-Square_Princess-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/Swarovski_Genuine-Topaz_Square-Princess_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/CCG_Passion_Topaz-Square_Princess-Baby_Pink-27125-RGB-500px- (1).png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_Babypink.mp4" src="../img/gemstones/detail_topaz/CCG_Passion_Topaz-Square_Princess-Baby_Pink-27125-RGB-500px-.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/CCG_Passion_Topaz-Square_Princess-Pink-20509-CMYK-4000px-003.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_1_PINK.mp4" src="../img/gemstones/detail_topaz/CCG_Passion_Topaz-Square_Princess-Pink-20509-CMYK-4000px-003.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/CCG_Passion_Topaz-Square_Princess-Blazing_Red-23324-RGB-500p.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_Blazingred.mp4" src="../img/gemstones/detail_topaz/CCG_Passion_Topaz-Square_Princess-Blazing_Red-23324-RGB-500p.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/big_img/Centria_Cut_Genuine_Passion_Topaz-Square_Princess-Violac-CMY.png"
                                  data-video="../img/gemstones/detail_topaz/big_img/Square_Princess_violac.mp4" src="../img/gemstones/detail_topaz/Centria_Cut_Genuine_Passion_Topaz-Square_Princess-Violac-CMY.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                        <option value="2.00" data-foo="0">2.00 mm</option>
                        <option value="2.50" data-foo="1">2.50 mm</option>
                        <option value="3.00" data-foo="2">3.00 mm</option>
                        <option value="3.50" data-foo="3">3.50 mm</option>
                        <option value="4.00" data-foo="4">4.00 mm</option>
                        <option value="4.50" data-foo="5">4.50 mm</option>
                        <option value="5.00" data-foo="6">5.00 mm</option>
                        <option value="6.00" data-foo="7">6.00 mm</option>
                        <option value="7.00" data-foo="8">7.00 mm</option>
                        <option value="8.00" data-foo="9">8.00 mm</option>
                        <option value="10.00" data-foo="10">10.00 mm</option>
                      </select>
                      <br>
                    </span>
                    <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                    </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>
    
       <!--  3 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-White-27718-RGB-500px-0039 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_2.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_2.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>MARQUISE STAR CUT</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_Marquise_Star_Cut_0308.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-White-27718-RGB-500px-0039 (1).png"
                                       data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_2.mp4"
                                    src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-White-27718-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Aqua_Blue-27724-RGB-500px-00 (1).png"
                                 data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_3.mp4"
                                  src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Aqua_Blue-27724-RGB-500px-00.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Ice_Blue-27730-RGB-500px-003 (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_iceblue.mp4"
                                  src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Ice_Blue-27730-RGB-500px-003.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Paraiba-27296-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_paraiba.mp4"
                                  src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Paraiba-27296-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Topaz_Marquise_Star_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Topaz_Marquise_Star_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Rainforest-27302-RGB-500px-0 (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_rainforest.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Rainforest-27302-RGB-500px-0.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Honey-27312-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_honey.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Honey-27312-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Poppy-27307-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_poppy.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Poppy-27307-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Swarovski_Genuine-Topaz_Centria-Cut-Marquise-Star_Misty-Rose (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Swarovski_Genuine-Topaz_Centria-Cut-Marquise-Star_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Swarovski_Genuine-Topaz_Centria-Cut-Marquise-Star_Misty-Rose.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Swarovski_Genuine-Topaz_Centria-Cut-Marquise-Star_Peach_Fron (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Topaz-Marquise_Star-Peach_large_2.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Swarovski_Genuine-Topaz_Centria-Cut-Marquise-Star_Peach_Fron.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Baby_Pink-27317-RGB-500px-00 (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_babypink.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Baby_Pink-27317-RGB-500px-00.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Pink-27300-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_pink.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Pink-27300-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Blazing_Red-27292-RGB-500px- (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_red.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/CCG_Passion_Topaz-Marquise_Star-Blazing_Red-27292-RGB-500px-.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Centria_Cut_Genuine_Passion_Topaz-Marquise_Star-Violac-CMYK- (1).png"
                                  data-video="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Marquise_Star_1_violac.mp4" src="../img/gemstones/detail_topaz/MARQUISE STAR CUT/Centria_Cut_Genuine_Passion_Topaz-Marquise_Star-Violac-CMYK-.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                        <option value="5x2.5" data-foo="0">5x2.5 mm</option>
                        <option value="6x3" data-foo="1">6x3 mm</option>
                        <option value="7x3.5" data-foo="2">7x3.5 mm</option>
                        <option value="8x4" data-foo="3">8x4 mm</option>
                        <option value="10x5" data-foo="4">10x5 mm</option>
                      </select>
                      <br>
                    </span>
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 

         <!-- 4 -->

          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-White-27716-RGB-500px-0039 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_white.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_white.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>Oval Star Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_Oval_Star_Cut_0308.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-White-27716-RGB-500px-0039 (1).png"
                                       data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_white.mp4"
                                    src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-White-27716-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Aqua_Blue-27722-RGB-500px-0039 (1).png"
                                 data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_aquablue.mp4"
                                  src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Aqua_Blue-27722-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Ice_Blue-27728-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_Iceblue.mp4"
                                  src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Ice_Blue-27728-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Paraiba-27160-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_Paraiba.mp4"
                                  src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Paraiba-27160-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/Topaz_Oval_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Kashmir.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/Topaz_Oval_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Rainforest-27166-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_Rainforest.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Rainforest-27166-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Honey-27182-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_Honey.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Honey-27182-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Poppy-27174-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_poppy.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Poppy-27174-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Oval-Star_Misty-Rose_Fro (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Oval-Star_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Oval-Star_Misty-Rose_Fro.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Oval-Star_Peach_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Topaz-Oval_Star-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Oval-Star_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Baby_Pink-27190-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_Babypink.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Baby_Pink-27190-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/Centria_Cut_Genuine_Passion_Topaz-Oval_Star-Pink-CMYK-1000px (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_Pink.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/Centria_Cut_Genuine_Passion_Topaz-Oval_Star-Pink-CMYK-1000px.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Blazing_Red-27156-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Star_Blazingred.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/CCG_Passion_Topaz-Oval_Star-Blazing_Red-27156-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Oval_Star_Cut/Centria_Cut_Genuine_Passion_Topaz-Oval_Star-Violac-CMYK-2000 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Oval_Star_Cut/Oval_Violac.mp4" src="../img/gemstones/detail_topaz/Oval_Star_Cut/Centria_Cut_Genuine_Passion_Topaz-Oval_Star-Violac-CMYK-2000.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                        <option value="5x3" data-foo="0">5x3 mm</option>
                        <option value="6x4" data-foo="1">6x4 mm</option>
                        <option value="7x5" data-foo="2">7x5 mm</option>
                        <option value="8x6" data-foo="3">8x6 mm</option>
                        <option value="9x7" data-foo="4">9x7 mm</option>
                        <option value="10x8" data-foo="5">10x8 mm</option>
                        <option value="11x9" data-foo="6">11x9 mm</option>
                        <option value="12x10" data-foo="7">12x10 mm</option>
                      </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 
  
         <!-- 5 -->

          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-White-27720-RGB-500px-0039 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>Pear Star Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_Pear_Star_Cut_0308.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-White-27720-RGB-500px-0039 (1).png"
                                       data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_1.mp4"
                                    src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-White-27720-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Aqua_Blue-27726-RGB-500px-0039 (1).png"
                                 data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_2.mp4"
                                  src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Aqua_Blue-27726-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Ice_Blue-27732-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_3.mp4"
                                  src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Ice_Blue-27732-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Paraiba-27234-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_4.mp4"
                                  src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Paraiba-27234-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz_Pear_Star_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_1.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz_Pear_Star_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Rainforest-27240-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_5.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Rainforest-27240-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Honey-27254-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_6.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Honey-27254-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Poppy-27247-RGB-500px-0039_ (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_7.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Poppy-27247-RGB-500px-0039_.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose_Fro (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose_Fro.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Peach_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz-Pear_Star-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Baby_Pink-27261-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_8.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Baby_Pink-27261-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Pink-27238-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_9.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Pink-27238-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Blazing_Red-27230-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_10.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Blazing_Red-27230-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Centria_Cut_Genuine_Passion_Topaz-Pear_Star-Violac (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_11.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Centria_Cut_Genuine_Passion_Topaz-Pear_Star-Violac.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                        <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                        <option value="5x3" data-foo="0">5x3 mm</option>
                        <option value="6x4" data-foo="1">6x4 mm</option>
                        <option value="7x5" data-foo="2">7x5 mm</option>
                        <option value="8x6" data-foo="3">8x6 mm</option>
                        <option value="9x7" data-foo="4">9x7 mm</option>
                        <option value="10x8" data-foo="5">10x8 mm</option>
                        <option value="11x9" data-foo="6">11x9 mm</option>
                        <option value="12x10" data-foo="7">12x10 mm</option>
                      </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 

        <!-- 6 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-Ice_Blue-26795-RGB-500px-0039 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Trilight_2.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Trilight_2.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>TRILIGHT CUT</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_Trilight_cut_17076.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-White-26921-RGB-500px-0039 (1).png"
                                       data-video="../img/gemstones/detail_topaz/TRILIGHT_CUT/Trilight_2.mp4"
                                    src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-White-26921-RGB-500px-0039.png">                                                </a>
                                 </li>
                                <!--  <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Aqua_Blue-27726-RGB-500px-0039 (1).png"
                                 data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_2.mp4"
                                  src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Aqua_Blue-27726-RGB-500px-0039.png">                                                </a>
                                 </li> -->
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-Ice_Blue-26795-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/TRILIGHT_CUT/Trilight_1.mp4"
                                  src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-Ice_Blue-26795-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-Paraiba-26797-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/TRILIGHT_CUT/Trilight_(2)_2.mp4"
                                  src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-Paraiba-26797-RGB-500px-0039.png">                                                </a>
                                 </li>
                               <!--   <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz_Pear_Star_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_1.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz_Pear_Star_Kashmir.png">                                                </a>
                                 </li> -->
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-Rainforest-26799-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/TRILIGHT_CUT/Trilight_(2)_1.mp4" src="../img/gemstones/detail_topaz/TRILIGHT_CUT/Passion_Topaz-Trilight-Rainforest-26799-RGB-500px-0039.png">                                                </a>
                                 </li>
                       <!--           <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Honey-27254-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_6.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Honey-27254-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Poppy-27247-RGB-500px-0039_ (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_7.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Poppy-27247-RGB-500px-0039_.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose_Fro (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose_Fro.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Peach_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz-Pear_Star-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Baby_Pink-27261-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_8.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Baby_Pink-27261-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Pink-27238-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_9.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Pink-27238-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Blazing_Red-27230-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_10.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Blazing_Red-27230-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Centria_Cut_Genuine_Passion_Topaz-Pear_Star-Violac (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_11.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Centria_Cut_Genuine_Passion_Topaz-Pear_Star-Violac.png">                                                </a>
                                 </li> -->
                              </ul>
                           </li>
                           <li>                            </li>
                         <!--   <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                        <option value="5x2.5" data-foo="0">5x2.5 mm</option>
                        <option value="6x3" data-foo="1">6x3 mm</option>
                        <option value="7x3.5" data-foo="2">7x3.5 mm</option>
                        <option value="8x4" data-foo="3">8x4 mm</option>
                        <option value="10x5" data-foo="4">10x5 mm</option>
                      </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 
         <!-- 7 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-White-28550-RGB-500px-0039 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/ARC_CUT/Arc_1.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/ARC_CUT/Arc_1.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>ARC CUT</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_K2515_ARC_CUT.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-White-28550-RGB-500px-0039 (1).png"
                                       data-video="../img/gemstones/detail_topaz/ARC_CUT/Arc_1.mp4"
                                    src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-White-28550-RGB-500px-0039.png">                                                </a>
                                 </li>
                                <!--  <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Aqua_Blue-27726-RGB-500px-0039 (1).png"
                                 data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_2.mp4"
                                  src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Aqua_Blue-27726-RGB-500px-0039.png">                                                </a>
                                 </li> -->
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Ice_Blue-28551-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/ARC_CUT/Arc_2.mp4"
                                  src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Ice_Blue-28551-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Paraiba-28554-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/ARC_CUT/Arc_3.mp4"
                                  src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Paraiba-28554-RGB-500px-0039.png">                                                </a>
                                 </li>
                               <!--   <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz_Pear_Star_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_1.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz_Pear_Star_Kashmir.png">                                                </a>
                                 </li> -->
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Rainforest-28553-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/ARC_CUT/Arc_4.mp4" src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Rainforest-28553-RGB-500px-0039.png">                                                </a>
                                 </li>
                             <!--     <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Honey-27254-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_6.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Honey-27254-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Poppy-27247-RGB-500px-0039_ (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_7.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Poppy-27247-RGB-500px-0039_.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose_Fro (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Misty-Rose_Fro.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Peach_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Topaz-Pear_Star-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Pear-Star_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Baby_Pink-27261-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_8.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Baby_Pink-27261-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Pink-27238-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_9.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/CCG_Passion_Topaz-Pear_Star-Pink-27238-RGB-500px-0039.png">                                                </a>
                                 </li> -->
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Blazing_Red-28552-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/ARC_CUT/Arc_5.mp4" src="../img/gemstones/detail_topaz/ARC_CUT/Passion_Topaz-Arc-Blazing_Red-28552-RGB-500px-0039.png">                                                </a>
                                 </li>
                            <!--      <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Centria_Cut_Genuine_Passion_Topaz-Pear_Star-Violac (1).png"
                                  data-video="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Pear_Star_11.mp4" src="../img/gemstones/detail_topaz/PEAR_STAR_CUT/Centria_Cut_Genuine_Passion_Topaz-Pear_Star-Violac.png">                                                </a>
                                 </li> -->
                              </ul>
                           </li>
                           <li>                            </li>
                         <!--   <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                        <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                       <option value="4x2" data-foo="0">4x2 mm</option>
                      </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 
        
      <!-- 8 -->
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-White-27322-RGB-500px-0039 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/HEART_CUT/Heart_2.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/HEART_CUT/Heart_2.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>Heart Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_Heart_05060.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-White-27322-RGB-500px-0039 (1).png"
                                       data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_2.mp4"
                                    src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-White-27322-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Aqua_Blue-27327-RGB-500px-0039 (1).png"
                                 data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_3.mp4"
                                  src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Aqua_Blue-27327-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Ice_Blue-27332-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_4.mp4"
                                  src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Ice_Blue-27332-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Paraiba-27347-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_5.mp4"
                                  src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Paraiba-27347-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/Topaz_Heart_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_1.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/Topaz_Heart_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Rainforest-27362-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_6.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Rainforest-27362-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Honey-27372-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_7.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Honey-27372-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Poppy-27367-RGB-500px-0039_ (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_8.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Poppy-27367-RGB-500px-0039_.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Heart_Misty-Rose_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Heart_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Heart_Misty-Rose_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Heart_Peach_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Topaz-Heart-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/Swarovski_Genuine-Topaz_Centria-Cut-Heart_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Baby_Pink-27377-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_9.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Baby_Pink-27377-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Pink-27357-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_10.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Pink-27357-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Blazing_Red-27337-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_11.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Blazing_Red-27337-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Violac-33314-CMYK-2000px-0043 (1).png"
                                  data-video="../img/gemstones/detail_topaz/HEART_CUT/Heart_12.mp4" src="../img/gemstones/detail_topaz/HEART_CUT/CCG_Passion_Topaz-Heart-Violac-33314-CMYK-2000px-0043.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                        <option value="3.00" data-foo="0">3.00 mm</option>
                        <option value="4.00" data-foo="1">4.00 mm</option>
                        <option value="5.00" data-foo="2">5.00 mm</option>
                        <option value="6.00" data-foo="3">6.00 mm</option>
                        <option value="7.00" data-foo="4">7.00 mm</option>
                        <option value="8.00" data-foo="5">8.00 mm</option>
                        <option value="9.00" data-foo="6">9.00 mm</option>
                      </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 

      <!-- 9 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-White-27382-R (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_white.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_white.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>Antique Cushion Checkerboard Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/Antique_Cushion_square_06060.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-White-27382-R (1).png"
                                       data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_white.mp4"
                                    src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-White-27382-R.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Aqua_Blue-273 (1).png"
                                 data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_aquablue.mp4"
                                  src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Aqua_Blue-273.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Ice_Blue-2739 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Cushion_Checkerboard_1iceblue.mp4"
                                  src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Ice_Blue-2739.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Paraiba-27422 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_paraiba.mp4"
                                  src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Paraiba-27422.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Topaz_Antique_Cushion_Checkerboard_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_kashmir.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Topaz_Antique_Cushion_Checkerboard_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Rainforest-27502_2 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_rainforest.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Rainforest-27502_2.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Honey-27462-R (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_honey.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Honey-27462-R.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Poppy-27454-R (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_poppy.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Poppy-27454-R.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Checkerb (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Checkerb.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Checkerb.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Check_1 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Topaz-Antique_Cushion_Checkerboard-Peach_terminal_1.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Check_1.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Baby_Pink-_2 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_Babypink.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Baby_Pink-_2.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Pink-27499-RGB-5_2 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_pink.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Pink-27499-RGB-5_2.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Blazing_Red-2 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_BlazingRed.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Checkerboard-Blazing_Red-2.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Violac-33327-CMY_2 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/Antique_Cushion_Checkerboard_violac.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Checkerboard_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Violac-33327-CMY_2.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                        <option value="5.00" data-foo="0">5.00 mm</option>
                        <option value="6.00" data-foo="1">6.00 mm</option>
                        <option value="7.00" data-foo="2">7.00 mm</option>
                        <option value="8.00" data-foo="3">8.00 mm</option>
                        <option value="10.00" data-foo="4">10.00 mm</option>
                        <option value="6x4" data-foo="5">6x4 mm</option>
                        <option value="8x6" data-foo="6">8x6 mm</option>
                        <option value="10x8" data-foo="7">10x8 mm</option>
                      </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 
       <!-- 10 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-White-27478-RGB-500 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_white.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_white.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>Antique Cushion Normal Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/Antique_Cushion.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-White-27478-RGB-500 (1).png"
                                       data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_white.mp4"
                                    src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-White-27478-RGB-500.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Aqua_Blue-27481-RGB (1).png"
                                 data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_aquablue.mp4"
                                  src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Aqua_Blue-27481-RGB.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Ice_Blue-27484-RGB- (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_iceblue.mp4"
                                  src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Ice_Blue-27484-RGB-.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Paraiba-27493-RGB-5 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_paraiba.mp4"
                                  src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Paraiba-27493-RGB-5.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Topaz_Antique_Cushion_Normal_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_1.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Topaz_Antique_Cushion_Normal_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Rainforest-27502-RG (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_rainforrest.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Rainforest-27502-RG.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Honey-27508-RGB-500 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_honey.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Honey-27508-RGB-500.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Poppy-27505-RGB-500 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_poppy.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Poppy-27505-RGB-500.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Normal_M (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Normal_M.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Normal_M.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Normal_P (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Topaz-Antique_Cushion-Peach_terminal_1.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Antique-Cushion-Normal_P.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Baby_Pink-27511-RGB (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_babypink.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Baby_Pink-27511-RGB.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Pink-27499-RGB-500p (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_pink.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Pink-27499-RGB-500p.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Blazing_Red-27487-R (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_blazing_red.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Blazing_Red-27487-R.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Violac-33327-CMYK-1 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/Antique_Cushion_Normal_violac.mp4" src="../img/gemstones/detail_topaz/Antique_Cushion_Normal_Cut/CCG_Passion_Topaz-Antique_Cushion_Normal-Violac-33327-CMYK-1.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                       <option value="8x6" data-foo="0">8x6 mm</option>
                       <option value="9x7" data-foo="1">9x7 mm</option>
                       <option value="10x8" data-foo="2">10x8 mm</option>
                     </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div> 

        <!-- 11 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-White-27514-RGB-500px-0039 (1).png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_2.mp4">
                     <source type="video/mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_2.mp4">
                  </video>
                </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                 <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative;">
                           Genuine Topaz                        
                           <h2>Octagon Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/gemstones/detail_topaz/K2515_Octagon_05060_2.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="387" value="1">
                                 <li class="active">                                                <a href="javascript:void(0)" data-color="white">                                                    <span class="glow"></span>                                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-White-27514-RGB-500px-0039 (1).png"
                                       data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_2.mp4"
                                    src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-White-27514-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="aqua-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Pastel Light" 
                                  data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Aqua_Blue-27517-RGB-500px-0039.png"
                                 data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_3.mp4"
                                  src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Aqua_Blue-27517-RGB-500px-0039 (1).png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="ice-blue">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" 
                                 data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Ice_Blue-27520-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_4.mp4"
                                  src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Ice_Blue-27520-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="paraiba">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Paraiba-27529-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon.mp4"
                                  src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Paraiba-27529-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="kashmir">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/Topaz_Octagon_Kashmir (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_1.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/Topaz_Octagon_Kashmir.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="rainforest">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Rainforest-27538-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_5.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Rainforest-27538-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="honey">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Honey-27544-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_6.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Honey-27544-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="poppy">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Poppy-27541-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_7.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Poppy-27541-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="misty-rose">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Octagon_Misty-Rose_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Octagon_Misty-Rose.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Octagon_Misty-Rose_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="peach">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Octagon_Peach_Front (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Topaz-Octagon-Peach_terminal.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/Swarovski_Genuine-Topaz_Centria-Cut-Octagon_Peach_Front.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="baby-pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Baby_Pink-27547-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_8.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Baby_Pink-27547-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="pink">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/Centria_Cut_Genuine_Passion_Topaz-Octagon-Pink-CMYK-1000px-0 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_9.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/Centria_Cut_Genuine_Passion_Topaz-Octagon-Pink-CMYK-1000px-0.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="blazing-red">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Blazing_Red-27523-RGB-500px-0039 (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_10.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/CCG_Passion_Topaz-Octagon-Blazing_Red-27523-RGB-500px-0039.png">                                                </a>
                                 </li>
                                 <li>                                                <a href="javascript:void(0)" data-color="violac">                                                    <span class="glow"></span>                                                    <img class="  lazyloaded" data-src="../img/gemstones/detail_topaz/Octagon_Cut/Centria_Cut_Genuine_Passion_Topaz-Octagon-Violac-CMYK-2000px (1).png"
                                  data-video="../img/gemstones/detail_topaz/Octagon_Cut/Octagon_11.mp4" src="../img/gemstones/detail_topaz/Octagon_Cut/Centria_Cut_Genuine_Passion_Topaz-Octagon-Violac-CMYK-2000px.png">                                                </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                    <span class="cutsizes">
                      <select>
                       <option value="8x6" data-foo="0">8x6 mm</option>
                       <option value="9x7" data-foo="1">9x7 mm</option>
                       <option value="10x8" data-foo="2">10x8 mm</option>
                      </select>
                      <br>
                    </span> 
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
           <!--  <div class="borderblack screen"></div> -->
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>   


      </div>
   </div>
   <!--<div class="row contentrow"></div>-->
   <!--UdmComment-->
</div>
            <!--UdmComment-->
         </div>
         <!-- Return To Top ================================================== -->
       <!--   <div class="component-return-to-top clearfix" id="component-return-to-top">
         </div> -->
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
      $('ul.coloritems').filter(function(){
         if($(this).children("li").length <= 5){
               $(this).css('marginLeft', '0px');
               // $(this).closest('.select-color').css('marginLeft', '0px');
         } 
      })  
   });
 $(document).on("click", ".colorselect .left", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left; - 1 * position >= 48 ? ($(this).parent().find(".coloritems").css("left", position + 48), $(this).parent().find(".colorselect .right").removeClass("arrowhide")) : $(this).parent().find(".colorselect .left").addClass("arrowhide")
        }), 
        $(document).on("click", ".colorselect .right", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left,
                maxleft = $(this).parent().find(".coloritems").attr("rel");
            maxleft > -1 * position && maxleft >= 0 ? ($(this).parent().find(".coloritems").css("left", position - 48), $(this).parent().find(".colorselect .left").removeClass("arrowhide")) : $(this).parent().find(".colorselect .right").addClass("arrowhide")
        });


          $(document).on("click", ".coloritems a", function(event) {
            var imgUrl  = $(this).find('img.lazyloaded').data('src');
            var videoUrl  = $(this).find('img.lazyloaded').data('video');
            var parentElement = $(this).closest('.coloritems');
            parentElement.find('li').removeClass('active');
            // $('.coloritems li').removeClass('active'); 
            $(this).parent('li').addClass('active');
            var colorUrl = $(this).data('color');
            $(this).closest('.choice_gems').find('.colorshare .cutcolor a').html(colorUrl.replace("-"," "));
            var Component_Product =  $(this).closest('.component-product');
            Component_Product.find('.cutpreview').find('img').attr('src','');
            Component_Product.find('.cutpreview').find('video').attr('src', '');
            Component_Product.find('.cutpreview').find('img').attr('src', imgUrl);
            Component_Product.find('.cutpreview').find('video').attr('src', videoUrl);
            Component_Product.find('.cutpreview').find('img').css('display', 'block');
            Component_Product.find('.cutpreview').find('video').css('display', 'none');
           Component_Product.find('.cutpreview').css('background', 'transparent url(../img/gemstones/detail/grey.jpg) 15px 0 repeat-y;ne');
        })

          // $(document).on('mousemove',".cutpreview",function(){
          //      $(this).find('img').css('display', 'none');
          //      $(this).find('video').css('display', 'block');
          //      $(this).css('background', 'none');
          // })

</script>
@endsection