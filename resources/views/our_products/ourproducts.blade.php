@extends('layouts.master')
@section('styles')
      <link rel="stylesheet" href="css/our_products.css">
      <title>{{trans('products.products_home')}}</title>
@endsection
@section('content')
<div class="container container-body">
   <div class="row">
      <div class="carousel-article col-md-12 hero">
         <div class="component-carousel carousel-title hero" id="component-carousel-hero-5292817">
            <div id="carousel-hero-5292817" class="carousel slide carousel-title">
               <ol class="carousel-indicators hide">
                  <li  data-slide-to="0" class="active"></li>
               </ol>
               <div class="carousel-inner">
                  <div class="item active" id="carousel-carousel-hero-5292817-item-0">
                     <div class="image-container" data-href="">
                        <div class="image-overlay is-loaded" ></div>
                        <a href="">
                           <div class="overlay" style="position:absolute; right:180px; width:350px; height:auto; z-index:100;">
                              <h1>
                                 <span style="position:relative; font-size:16px; color:#fff; text-decoration:none;"></span>
                              </h1>
                              <br>
                              <span style="position:relative; font-size:16px; top:-50px; color:#ccc; text-decoration:none;"></span>
                           </div>
                        </a>
                        <img alt="Genuine Gemstones and Created Stones" class=" img-responsive lazyloaded" src="img/bg_our_products.jpg">
                     </div>
                     <div class="container">
                        <div class="carousel-caption hide">     
                           <p class="slide-meta "></p>
                           <h1 class="h2 slide-title">SWAROVSKI GEMSTONES TM’s Product</h1>
                        </div>
                     </div>
                  </div>
               </div>
               <a class="left carousel-control hide" href="#carousel-hero-5292817">
                  <span class="ir ir-chevron ir-chevron-left ir-theme-light"></span>
               </a> 
               <a class="right carousel-control hide" href="#carousel-hero-5292817" data-slide="next">
                  <span class="ir ir-chevron ir-chevron-right ir-theme-light"></span>
               </a>  
            </div>
            <div class="carousel-heading ">
               <p class="slide-meta "></p>
               <h1 class="h2 slide-title" style="font-size: 38px !important;text-transform: uppercase;">SWAROVSKI GEMSTONES TM’s Product
               </h1>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="content col-md-12">
         <div class="row contentrow noFormatting noLists noLinks">
            <div class="component-promo featured col-xs-12 col-sm-6">
               <div class="row black">
                  <div class="col-xs-12">
                     <a href="/genuine-gemstones" target="">
                        <div class="image-container">
                           <img alt="Swarovski Genuine Gemstones" class="img-responsive lazyloaded" src="img/our_products_img1.jpg">                            
                        </div>
                     </a>
                  </div>
                  <div class="col-xs-12 text-center">
                     <div class="component-promo-text">                   
                        <p class="slide-meta "></p>                        
                        <h4 class="title">SWAROVSKI {{trans('products.genuine_gemstones')}}
                           <br>
                        </h4>
                        <div>
                           <p>
                              <b></b>
                           </p>
                        </div>
                        <p class="related-link">                           
                           <a href="/genuine-gemstones" target="">  {{trans('products.discover_more')}} 
                           </a>                        
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="component-promo featured col-xs-12 col-sm-6">
               <div class="row black">
                  <div class="col-xs-12">
                     <a href="/created-stones" target="">
                        <div class="image-container">
                           <img alt="sRGB_SGE_Zirconia_Innovations_2018_Final.jpg" class=" img-responsive lazyloaded" src="img/our_products_img2.jpg">                                                        
                        </div>
                     </a>
                  </div>
                  <div class="col-xs-12 text-center">
                     <div class="component-promo-text">                                    
                        <p class="slide-meta "></p>                        
                        <h4 class="title">SWAROVSKI {{trans('products.created_stones')}}<br>
                        </h4>
                        <div>
                           <p>
                              <please></please>
                           </p>
                        </div>
                        <p class="related-link">                            
                           <a href="/created-stones" target="">                                {{trans('products.discover_more')}}                            
                           </a>                        
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection