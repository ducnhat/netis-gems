@extends('layouts.master')
@section('styles')
<title>ZIRCONIA</title>
      <link rel="stylesheet" href="../css/zirconia.css">
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <!-- Page Content -->
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
              @include('our_products.menu_left')
            </div>
            <!--/UdmComment-->
         <div class="article col-xs-12 col-sm-12 col-md-10">
   <!--/UdmComment-->
   <div class="row">
      <div class="component-product-details col-lg-12">
         <div class="product">
            <h2 class="title">                SWAROVSKI GENUINE ZIRCONIA                              </h2>
         </div>
      </div>
      @if($langNetis == 'vi')
         <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                   Đá Zirconia Swarovski là loại đá zirconia đầu tiên trong lịch sử tuân theo các tiêu chuẩn phân loại chất lượng cắt kim cương của Viện Gemological Kỳ (GIA)- tổ chức  hàng đầu thế giới về phân loại kim cương. Độ chiếu sáng hoàn hảo giúp đá Zirconia Swarovski có chất lượng tốt nhất và là  đá zirconia giống kim cương nhất hiện nay. Màu sắc hoàn hảo và nét cắt tinh tế lấy cảm hứng từ những viên kim cương  nổi tiếng xuất phát từ lịch sử hơn một trăm năm kinh nghiệm của Swarovski .     &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>Đá Zirconia mang sắc thái sinh động với mặt đá được nhuộm màu theo phương pháp nhiệt được cấp bằng sáng chế của Swarovski TCF ™ , thân thiện môi trường và không phóng xạ.  
                     <br>
                        MINH CHỨNG VỀ CHẤT LƯỢNG
                        <br>
                        Mỗi viên đá Zirconia Swarovski được khắc chữ  "Swarovski Zirconia" bằng laser cực nhỏ và mắt thường hoàn toàn không thấy. Xác thực  viên đá  bạn đang mua là đá zirconia của Swarovski có độ chính xác cao và độ sáng hoàn hảo nhất 
                     &nbsp;</p>
                     <p><a target="_self" href="https://www.swarovski-gemstones.com/download/SWAROVSKI_ZIRCONIA_2018_ONLINE-CATALOG.pdf" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">Xem hình ảnh catalog đá CZ của Swarovski</a></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @else
      <div class="component-product-details col-xs-12 col-sm-12 col-md-6" style="z-index:1000">
         <div class=" product">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>
                    Swarovski Zirconia is the first zirconia in history to adhere to the comprehensive set of standards for grading diamond cut quality of the Gemological Institute of America (GIA), the world’s foremost authority on diamond grading. The unmatched brilliance of Swarovski Zirconia makes it the finest and most diamond-like zirconia available today. Perfectly saturated colors and sophisticated cuts, which were inspired by the most famous fancy diamonds and stem from Swarovski’s history of more than a hundred years of precision-cutting expertise, place Swarovski Zirconia in a class of its own.       &nbsp;
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="component-product-details featured col-xs-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-md-6" style="z-index:100">
         <div class="">
            <div class="col-xs-12">
               <div class="component-product-text-gem">
                  <p class="slide-meta hidden"></p>
                  <div class="noGcn">
                     <p>The vivid shades are created with Swarovski’s patented surface enhancement process TCF™ (Thermal Color Fusion) that is environmentally sound and does not expose the stones to irradiation. The colors are durable and of proven resistance for daily wear.
                        <br>
                        THE PROOF OF QUALITY
                        <br>
                        Every single Swarovski Zirconia is marked with a microscopic “Swarovski Zirconia” laser engraving that is completely invisible to the naked eye. This quality seal serves as your assurance that you are acquiring a product set with zirconia of the highest precision and brilliance cut by Swarovski.
                        &nbsp;</p>
                    <!--  <p><a target="_self" href="https://www.swarovski-gemstones.com/download/SWAROVSKI_ZIRCONIA_2018_ONLINE-CATALOG.pdf" class="" data-gentics-gcn-url="https://www.swarovski-gemstones.com/contact-us/wheretobuy.en.html">View our Swarovski Zirconia product catalog</a></p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif
      <div class="component-product-details featured col-xs-12 col-sm-12 col-md-6" style="z-index:100; margin-bottom: 30px; clear: both;">
       <!--   <a href="/contact-us/contact-us.en.html" style="padding: 0; margin: 0;">
         <button class="find-button btn btn-primary">
         Contact your local sales office
         </button>
         </a> -->
      </div>
      <div style="clear:both;float:none"></div>
      <div class="noGcn noFormatting noLinks noLists product-cutcontainer">
        <div class="component-headline">    
          <div class="headline_title"><h2>Classics</h2>
          </div> 
          <div class="headline_text"></div>
        </div>
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-White-28286-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-White-28286-FULLRES.mp4_large.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-White-28286-FULLRES.mp4_large.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                           Swarovski Zirconia                           
                           <h2>Round Pure Brilliance Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/K2515_Round_Brilliant_Cut_0308_4.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="819" value="1">
                                <li class="active">                                                
                                    <a href="javascript:void(0)" data-color="white">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-White-28286-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-White-28286-FULLRES.mp4_large.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-White-28286-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="greyish-blue">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Round-Pure-Brilliance_Greyish-Blue_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Swarovski_Zirconia_Round-Pure-Brilliance_Greyish-Blue.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Swarovski_Zirconia_Round-Pure-Brilliance_Greyish-Blue_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Fancy_Blue_Light-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirc_Fancylightblue_Pure_Brilliance_Round_(1)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Blue_Light-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Fancy_Blue-31166-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirc_Fanc_Blue_Oval_Diamond_(3)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Blue-31166-FULLRES.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="arctic-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Artic_Blue.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/round_3.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Artic_Blue.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="frost-mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Mint_Frosty-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Mint_Frosty-xxxxx-FULLRES.mp4_large.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Mint_Frosty-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Mint-28508-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/zircmintPure_Brilliance_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Mint-28508-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Fancy_Light_Green-xxxxx-RGB-4000px-.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Light_Green-xxxxx-FULLRES.mp4.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Light_Green-xxxxx-RGB-4000px-.png">                                                
                                 </a>
                                 </li>
                                <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Fancy_Green-00000-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirc_Fancygreen_Pure_Brilliance_round_(1)_1_FancyGreen.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Green-00000-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia_Round_Pure_Brilliance_Green.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zircgreenpurebrilliance_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia_Round_Pure_Brilliance_Green.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="spring-green">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Round-Pure-Brilliance_Spring-Green_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Spring_Green-xxxxx-FULLRES.mp4_l_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Swarovski_Zirconia_Round-Pure-Brilliance_Spring-Green_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Round-Pure-Brilliance_Fancy-Morganite_Fro.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Morganite-xxxxx-FULLRES.mp4_l.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Swarovski_Zirconia_Round-Pure-Brilliance_Fancy-Morganite_Fro.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Pink-28341-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/zircpinkPure_Brilliance_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Pink-28341-FULLRES.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-pink-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Fancy_Pink-28440-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirc_Fancy_Pink_Pure_Brilliance_(1)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Pink-28440-FULLRES.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="red-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Red.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/zircredPure_Brilliance_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Red.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="red-dark-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Red_Dark-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Pure_Brilliance_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Red_Dark-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="purplish-pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Round-Pure-Brilliance_Purplish-Pink_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Swarovski_Zirconia_Round-Pure-Brilliance_Purplish-Pink.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Swarovski_Zirconia_Round-Pure-Brilliance_Purplish-Pink_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-purple-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Fancy_Purple-00000-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Purple-00000-FULLRES.mp4_larg.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Fancy_Purple-00000-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-champagne-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Champagne-28399-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirc_Champ_Pure_Brilliance_(1)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Champagne-28399-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-yellow-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia_Round_Pure_Brilliance_Fancy_Yellow.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirc_FancyYellow_Pure_Brilliance_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia_Round_Pure_Brilliance_Fancy_Yellow.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="amber-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Amber.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/round_4.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Amber.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="caramel-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Caramel-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Caramel-xxxxx-FULLRES.mp4_large_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Caramel-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="black">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/big_img/Zirconia-Pure_Brilliance-Black-29855-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia_PB_Black_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Pure_Brilliance_Cut/Zirconia-Pure_Brilliance-Black-29855-FULLRES.png">                                                
                                 </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="0.70" data-foo="0">0.70 mm</option>
                           <option value="0.80" data-foo="1">0.80 mm</option>
                           <option value="0.90" data-foo="2">0.90 mm</option>
                           <option value="1.00" data-foo="3">1.00 mm</option>
                           <option value="1.10" data-foo="4">1.10 mm</option>
                           <option value="1.20" data-foo="5">1.20 mm</option>
                           <option value="1.25" data-foo="6">1.25 mm</option>
                           <option value="1.30" data-foo="7">1.30 mm</option>
                           <option value="1.40" data-foo="8">1.40 mm</option>
                           <option value="1.50" data-foo="9">1.50 mm</option>
                           <option value="1.60" data-foo="10">1.60 mm</option>
                           <option value="1.70" data-foo="11">1.70 mm</option>
                           <option value="1.75" data-foo="12">1.75 mm</option>
                           <option value="1.80" data-foo="13">1.80 mm</option>
                           <option value="1.90" data-foo="14">1.90 mm</option>
                           <option value="2.00" data-foo="15">2.00 mm</option>
                           <option value="2.10" data-foo="16">2.10 mm</option>
                           <option value="2.20" data-foo="17">2.20 mm</option>
                           <option value="2.25" data-foo="18">2.25 mm</option>
                           <option value="2.30" data-foo="19">2.30 mm</option>
                           <option value="2.40" data-foo="20">2.40 mm</option>
                           <option value="2.50" data-foo="21">2.50 mm</option>
                           <option value="2.60" data-foo="22">2.60 mm</option>
                           <option value="2.70" data-foo="23">2.70 mm</option>
                           <option value="2.75" data-foo="24">2.75 mm</option>
                           <option value="2.80" data-foo="25">2.80 mm</option>
                           <option value="2.90" data-foo="26">2.90 mm</option>
                           <option value="3.00" data-foo="27">3.00 mm</option>
                           <option value="3.10" data-foo="28">3.10 mm</option>
                           <option value="3.20" data-foo="29">3.20 mm</option>
                           <option value="3.25" data-foo="30">3.25 mm</option>
                           <option value="3.30" data-foo="31">3.30 mm</option>
                           <option value="3.40" data-foo="32">3.40 mm</option>
                           <option value="3.50" data-foo="33">3.50 mm</option>
                           <option value="3.75" data-foo="34">3.75 mm</option>
                           <option value="4.00" data-foo="35">4.00 mm</option>
                           <option value="4.25" data-foo="36">4.25 mm</option>
                           <option value="4.50" data-foo="37">4.50 mm</option>
                           <option value="4.75" data-foo="38">4.75 mm</option>
                           <option value="5.00" data-foo="39">5.00 mm</option>
                           <option value="5.25" data-foo="40">5.25 mm</option>
                           <option value="5.50" data-foo="41">5.50 mm</option>
                           <option value="5.75" data-foo="42">5.75 mm</option>
                           <option value="6.00" data-foo="43">6.00 mm</option>
                           <option value="6.25" data-foo="44">6.25 mm</option>
                           <option value="6.50" data-foo="45">6.50 mm</option>
                           <option value="7.00" data-foo="46">7.00 mm</option>
                           <option value="7.50" data-foo="47">7.50 mm</option>
                           <option value="8.00" data-foo="48">8.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 2 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-White-28905-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-White-28905-FULLRES.mp4_large.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-White-28905-FULLRES.mp4_large.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                           Swarovski Zirconia                           
                           <h2>Square Princess Pure Brilliance Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Square_Princess_PB_Cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="723" value="1">
                                <li class="active">                                                
                                    <a href="javascript:void(0)" data-color="white">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-White-28905-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-White-28905-FULLRES.mp4_large.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-White-28905-FULLRES_2.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="greyish-blue">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Greyish-B.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Greyish-B.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Greyish-B.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Blue_Light-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirc_Fancylightblue_Pure_Brilliance_Round_(2)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Blue_Light-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia_Square_Princess_Fancy_Blue_0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/square.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia_Square_Princess_Fancy_Blue_0043.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="arctic-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Arctic_Blue-29526-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirc_ArcticBlue_Square_Princess_Star_(1)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Arctic_Blue-29526-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="frost-mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Mint_Frosty-xxxxx-RGB-4000px-0.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Mint_Frosty-xxxxx-FULLRES.mp4_.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Mint_Frosty-xxxxx-RGB-4000px-0.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Mint-29527-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/zircmintSquare_Princess_Star_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Mint-29527-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Fancy_Light_Green-xxxxx-RGB-40.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Fancy_Light_Green-xxxxx-FULLRE.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Fancy_Light_Green-xxxxx-RGB-40.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Fancy_Green-00000-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirc_Fancygreen_Pure_Brilliance_round_(2)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Fancy_Green-00000-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia_Square_Princess_Pure_Brilliance_Green_0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/ZircgreenSquare_Princess_Star_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia_Square_Princess_Pure_Brilliance_Green_0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="spring-green">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Spring-Gr.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Pure_Brilliance-Spring_Green-xxxxx-.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Spring-Gr.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Fancy-Mor.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Pure_Brilliance-Fancy_Morganite-xxx.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Fancy-Mor.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Pink-28921-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/zircpinkSquare_Princess_Star_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Pink-28921-FULLRES.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-pink-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Fancy_Pink.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirc_Fancy_Pink_SquarePrincess_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Fancy_Pink.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="red-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Red-29524-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/zircredSquare_Princess_Star_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Red-29524-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="red-dark-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Red_Dark-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Square_Princess_Star_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Red_Dark-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="purplish-pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Purplish-.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Purplish-.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Swarovski_Zirconia_Square-Princess-Pure-Brilliance_Purplish-.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-purple-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia_Square_Princess_PB_Cut_Fancy_Purple.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirc_FancyPurple_squareprincess_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia_Square_Princess_PB_Cut_Fancy_Purple.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-champagne-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Champagne-28936-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirc_Champ_Square_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Champagne-28936-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-yellow-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia_Square_Princess_Fancy_Yellow.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirc_Fancy_Yellow_Square_Princess_Star_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia_Square_Princess_Fancy_Yellow.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="amber-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/big_img/Zirconia-Square_Princess_Star-Amber-29775-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Amber_Oval_Diamond_(4)_1.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Princess_Pure_Brilliance_Cut/Zirconia-Square_Princess_Star-Amber-29775-FULLRES.png">                                                
                                 </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="1.50" data-foo="0">1.50 mm</option>
                           <option value="2.00" data-foo="1">2.00 mm</option>
                           <option value="2.50" data-foo="2">2.50 mm</option>
                           <option value="2.75" data-foo="3">2.75 mm</option>
                           <option value="3.00" data-foo="4">3.00 mm</option>
                           <option value="3.50" data-foo="5">3.50 mm</option>
                           <option value="4.00" data-foo="6">4.00 mm</option>
                           <option value="5.00" data-foo="7">5.00 mm</option>
                           <option value="6.00" data-foo="8">6.00 mm</option>
                           <option value="7.00" data-foo="9">7.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 3 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-White-28966-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                           Swarovski Zirconia                           
                           <h2>Marquise Pure Brilliance Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/K2515_Marquise_Diamond_PB_Cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="771" value="1">
                                <li class="active">                                                
                                    <a href="javascript:void(0)" data-color="white">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-White-28966-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-White-28966-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="greyish-blue">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Marquise-Pure-Brilliance_Greyish-Blue_Fro.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Greyish Blue - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski_Zirconia_Marquise-Pure-Brilliance_Greyish-Blue_Fro.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Blue_Light-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Light Blue (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Blue_Light-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia_Marquise_Fancy_Diamond_Fancy_Blue.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Blue (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia_Marquise_Fancy_Diamond_Fancy_Blue.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="arctic-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Arctic_Blue-29597-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Arctic Blue (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Arctic_Blue-29597-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="frost-mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Mint_Frosty-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Frost Mint (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Mint_Frosty-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Mint-29601-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Mint (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Mint-29601-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Fancy_Light_Green-xxxxx-RGB-4000px.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Light Green (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Fancy_Light_Green-xxxxx-RGB-4000px.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Fancy_Green-00000-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Green (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Fancy_Green-00000-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Green-29605-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Green (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Green-29605-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="spring-green">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Marquise-Pure-Brilliance_Spring-Green_Fro.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Spring Green - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski_Zirconia_Marquise-Pure-Brilliance_Spring-Green_Fro.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Marquise-Pure-Brilliance_Fancy-Morganite_.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Morganite - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski_Zirconia_Marquise-Pure-Brilliance_Fancy-Morganite_.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Pink-28974-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Pink - 4x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Pink-28974-FULLRES.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-pink-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia_Marquise_Navette_Fancy_Pink_29613_CMYK_2000px_0039.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Pink (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia_Marquise_Navette_Fancy_Pink_29613_CMYK_2000px_0039.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="red-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Red-29609-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Red (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Red-29609-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="red-dark-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Red_Dark-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Red Dark (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Red_Dark-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="purplish-pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Marquise-Pure-Brilliance_Purplish-Pink_Fr.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Purplish Pink - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski_Zirconia_Marquise-Pure-Brilliance_Purplish-Pink_Fr.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-purple-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia_Marquise_Diamond_PB_Cut_Fancy_Purple.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Purple (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia_Marquise_Diamond_PB_Cut_Fancy_Purple.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-champagne-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Champagne-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Champagne (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Champagne-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-yellow-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia_Marquise_Fancy_Diamond_Fancy_Yellow.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Fancy Yellow (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia_Marquise_Fancy_Diamond_Fancy_Yellow.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="amber-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Amber-29735-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Amber (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Amber-29735-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="caramel-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Caramel-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Swarovski Zirconia - Marquise Pure Brilliance Cut - Caramel (TCF) - 3x1.5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Marquise_Pure_Brilliance_Cut/Zirconia-Marquise_Diamond-Caramel-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x1.5" data-foo="0">3x1.5 mm</option>
                           <option value="4x2" data-foo="1">4x2 mm</option>
                           <option value="5x2.5" data-foo="2">5x2.5 mm</option>
                           <option value="6x3" data-foo="3">6x3 mm</option>
                           <option value="7x3.5" data-foo="4">7x3.5 mm</option>
                           <option value="8x4" data-foo="5">8x4 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 4 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-White-28980-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                           Swarovski Zirconia                           
                           <h2>Pear Pure Brilliance Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/K2515_Pear_Diamond_PB_Cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="771" value="1">
                                <li class="active">                                                
                                    <a href="javascript:void(0)" data-color="white">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-White-28980-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-White-28980-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="greyish-blue">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Pear-Pure-Brilliance_Greyish-Blue_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Greyish Blue - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski_Zirconia_Pear-Pure-Brilliance_Greyish-Blue_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Blue_Light-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Light Blue (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Blue_Light-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia_Pear_Fancy_Diamond_Fancy_Blue_0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Blue (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia_Pear_Fancy_Diamond_Fancy_Blue_0043.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="arctic-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Marquise_Diamond-Arctic_Blue-29597-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Arctic Blue (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Arctic_Blue-29572-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="frost-mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Mint_Frosty-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Frost Mint (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Mint_Frosty-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Mint-29577-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Mint (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Mint-29577-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Fancy_Light_Green-xxxxx-RGB-4000px-004.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Light Green (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Fancy_Light_Green-xxxxx-RGB-4000px-004.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Fancy_Green-00000-FULLRES_2.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Green (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Fancy_Green-00000-FULLRES_2.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia_Pear_Diamond_Green_0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Green (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia_Pear_Diamond_Green_0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="spring-green">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Pear-Pure-Brilliance_Spring-Green_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Light Green (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski_Zirconia_Pear-Pure-Brilliance_Spring-Green_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Pear-Pure-Brilliance_Fancy-Morganite_Fron.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Pink (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski_Zirconia_Pear-Pure-Brilliance_Fancy-Morganite_Fron.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Pink-28989-CMYK-0043_Kopie.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Pink - 5x3 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Pink-28989-CMYK-0043_Kopie.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-pink-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia_Pear_Diamond_Fancy_Pink_29592_neuer_schliff_CMYK_20.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Pink (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia_Pear_Diamond_Fancy_Pink_29592_neuer_schliff_CMYK_20.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="red-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Red.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Red (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Red.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="red-dark-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Red_Dark-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Red Dark (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Red_Dark-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="purplish-pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Pear-Pure-Brilliance_Purplish-Pink_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Purplish Pink - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski_Zirconia_Pear-Pure-Brilliance_Purplish-Pink_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-purple-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia_Pear_Diamond_PB_Cut_Fancy_Purple.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Purple (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia_Pear_Diamond_PB_Cut_Fancy_Purple.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-champagne-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Champagne-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Champagne (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Champagne-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-yellow-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia_Pear_Fancy_Diamond_Fancy_Yellow_0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Fancy Yellow (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia_Pear_Fancy_Diamond_Fancy_Yellow_0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="amber-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Amber-29741-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Amber (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Amber-29741-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="caramel-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/big_img/Zirconia-Pear_Diamond-Caramel-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Swarovski Zirconia - Pear Pure Brilliance Cut - Caramel (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pear_Pure_Brilliance_Cut/Zirconia-Pear_Diamond-Caramel-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x2" data-foo="0">3x2 mm</option>
                           <option value="5x3" data-foo="1">5x3 mm</option>
                           <option value="6x4" data-foo="2">6x4 mm</option>
                           <option value="7x5" data-foo="3">7x5 mm</option>
                           <option value="8x5" data-foo="4">8x5 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 5 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-White-29001-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                           Swarovski Zirconia                           
                           <h2>Oval Pure Brilliance Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/K2515_Oval_Diamond_PB_Cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li>
                           <li>
                              <ul class="coloritems" rel="771" value="1">
                                <li class="active">                                                
                                    <a href="javascript:void(0)" data-color="white">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-White-29001-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-White-29001-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="greyish-blue">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Oval-Pure-Brilliance_Greyish-Blue_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Greyish Blue - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski_Zirconia_Oval-Pure-Brilliance_Greyish-Blue_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Blue_Light-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Light Blue (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Blue_Light-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia_Oval_Fancy_Diamond_Fancy_Blue.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Blue (TCF) - 3x5 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia_Oval_Fancy_Diamond_Fancy_Blue.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="arctic-blue-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Arctic_Blue-29617-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Arctic Blue (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Arctic_Blue-29617-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="frost-mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Mint_Frosty-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Frost Mint (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Mint_Frosty-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Mint-29622-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Mint (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Mint-29622-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-light-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Fancy_Light_Green-xxxxx-RGB-4000px-004.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Light Green (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Fancy_Light_Green-xxxxx-RGB-4000px-004.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Fancy_Green-00000-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Green (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Fancy_Green-00000-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="green-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia_Oval_Diamond_Green.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Green (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia_Oval_Diamond_Green.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="spring-green">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Oval-Pure-Brilliance_Spring-Green_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Spring Green - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski_Zirconia_Oval-Pure-Brilliance_Spring-Green_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Oval-Pure-Brilliance_Fancy-Morganite_Fron.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Morganite - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski_Zirconia_Oval-Pure-Brilliance_Fancy-Morganite_Fron.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Pink-29104-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Pink - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Pink-29104-FULLRES.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-pink-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia_Oval_Diamond_Fancy_Pink_29637_neuer_schliff_CMYK_20.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Pink (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia_Oval_Diamond_Fancy_Pink_29637_neuer_schliff_CMYK_20.png">                                                
                                 </a>
                                 </li>
                                  <li>                                                
                                    <a href="javascript:void(0)" data-color="red-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Red-29632-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Red (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Red-29632-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="red-dark-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Red_Dark-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Red Dark (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Red_Dark-xxxxx-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="purplish-pink">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Swarovski_Zirconia_Oval-Pure-Brilliance_Purplish-Pink_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Purplish Pink - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski_Zirconia_Oval-Pure-Brilliance_Purplish-Pink_Front.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-purple-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia_Oval_Diamond_PB_Cut_Fancy_Purple.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Purple (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia_Oval_Diamond_PB_Cut_Fancy_Purple.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-champagne-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Champagne-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Champagne (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Champagne-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="fancy-yellow-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia_Oval_Fancy_Diamond_Fancy_Yellow.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Fancy Yellow (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia_Oval_Fancy_Diamond_Fancy_Yellow.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="amber-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Amber-29746-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Amber (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Amber-29746-FULLRES.png">                                                
                                 </a>
                                 </li>
                                 <li>                                                
                                    <a href="javascript:void(0)" data-color="caramel-(tcf)">                                                    
                                       <span class="glow"></span>                                                    
                                       <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/big_img/Zirconia-Oval_Diamond-Caramel-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Swarovski Zirconia - Oval Pure Brilliance Cut - Caramel (TCF) - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Oval_Pure_Brilliance_Cut/Zirconia-Oval_Diamond-Caramel-xxxxx-RGB-4000px-0043.png">                                                
                                 </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                           <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li>
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x2" data-foo="0">3x2 mm</option>
                           <option value="5x3" data-foo="1">5x3 mm</option>
                           <option value="6x4" data-foo="2">6x4 mm</option>
                           <option value="7x5" data-foo="3">7x5 mm</option>
                           <option value="8x5" data-foo="4">8x5 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 6 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Heart_Cut/big_img/Zirconia-13035400_Heart-White-xxxxx-FULLRES_0043_Kopie.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Heart_Cut/Swarovski Zirconia - Heart Cut - White - 3x3 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Heart_Cut/Swarovski Zirconia - Heart Cut - White - 3x3 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Heart Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Heart_Cut/K2515_Heart_05060_2.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Heart_Cut/big_img/Zirconia-13035400_Heart-White-xxxxx-FULLRES_0043_Kopie.png"
                                       data-video="../img/createdstones/detail_zirconia/Heart_Cut/Swarovski Zirconia - Heart Cut - White - 3x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Heart_Cut/Zirconia-13035400_Heart-White-xxxxx-FULLRES_0043_Kopie.png">                                                
                                  </a>
                                 </li>
                                  <li>                                                
                                  <a href="javascript:void(0)" data-color="fancy-pink-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Heart_Cut/big_img/Zirconia-13035400_Heart-Fancy_Pink-xxxxx-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Heart_Cut/Swarovski Zirconia - Heart Cut - Fancy Pink (TCF) - 3x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Heart_Cut/Zirconia-13035400_Heart-Fancy_Pink-xxxxx-4000px-0043.png">                                                
                                  </a>
                                 </li>
                                   <li>                                                
                                  <a href="javascript:void(0)" data-color="red-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Heart_Cut/big_img/Zirconia-13035400_Heart-Red-xxxxx-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Heart_Cut/Swarovski Zirconia - Heart Cut - Red (TCF) - 3x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Heart_Cut/Zirconia-13035400_Heart-Red-xxxxx-4000px-0043.png">                                                
                                  </a>
                                 </li>
                                 <li>                                                
                                  <a href="javascript:void(0)" data-color="red-dark-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Heart_Cut/big_img/Zirconia-Heart-Red_Dark-xxxxx-RGB-4000px-0043.png"
                                       data-video="../img/createdstones/detail_zirconia/Heart_Cut/Swarovski Zirconia - Heart Cut - Red Dark (TCF) - 3x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Heart_Cut/Zirconia-Heart-Red_Dark-xxxxx-RGB-4000px-0043.png">                                                
                                  </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                         <!--   <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x3" data-foo="0">3x3 mm</option>
                           <option value="4x4" data-foo="1">4x4 mm</option>
                           <option value="5x5" data-foo="2">5x5 mm</option>
                           <option value="6x6" data-foo="3">6x6 mm</option>
                        </select>
                        <br>
                     </span>
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>  
        
        <!-- 7 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Cushion_Cut/Swarovski_Zirconia_Cushion_Cut.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Cushion_Cut/Swarovski Zirconia - Cushion Cut - White - 4x4 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Cushion_Cut/Swarovski Zirconia - Cushion Cut - White - 4x4 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Cushion Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Cushion_Cut/K2515_CUSHION_CUT_06066.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Cushion_Cut/Swarovski_Zirconia_Cushion_Cut.png"
                                       data-video="../img/createdstones/detail_zirconia/Cushion_Cut/Swarovski Zirconia - Cushion Cut - White - 4x4 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Cushion_Cut/Swarovski_Zirconia_Cushion_Cut1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="4x4" data-foo="0">4x4 mm</option>
                           <option value="5x5" data-foo="1">5x5 mm</option>
                           <option value="6x6" data-foo="2">6x6 mm</option>
                           <option value="7x7" data-foo="3">7x7 mm</option>
                           <option value="8x8" data-foo="4">8x8 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 8 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Octagon_Step_Cut/Zirconia_Octagon_Step_White_00431.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Octagon_Step_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Octagon_Step_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Octagon Step Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Octagon_Step_Cut/K2515_OctagonStepCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Octagon_Step_Cut/Zirconia_Octagon_Step_White_00431.png"
                                       data-video="../img/createdstones/detail_zirconia/Octagon_Step_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Octagon_Step_Cut/Zirconia_Octagon_Step_White_0043.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="5x3" data-foo="0">5x3 mm</option>
                           <option value="6x4" data-foo="1">6x4 mm</option>
                           <option value="7x5" data-foo="2">7x5 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 9 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Square_Step_Cut/Zirconia-Square_Step-White-29769-CMYK-0043_Kopie.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Square_Step_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Square_Step_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Square Step Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Square_Step_Cut/K2515_SquareStepCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Square_Step_Cut/Zirconia-Square_Step-White-29769-CMYK-0043_Kopie.png"
                                       data-video="../img/createdstones/detail_zirconia/Square_Step_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Square_Step_Cut/Zirconia-Square_Step-White-29769-CMYK-0043_Kopie1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="2.50" data-foo="1">2.50 mm</option>
                           <option value="3.00" data-foo="2">3.00 mm</option>
                           <option value="4.00" data-foo="3">4.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 10 -->
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Setting_Square_Princess/Zirconia-Square_Princess_Star-White-28905-FULLRES1.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Setting_Square_Princess/Swarovski Zirconia - Inv. Setting Square Princess Pure Brilliance - White - 2.00 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Setting_Square_Princess/Swarovski Zirconia - Inv. Setting Square Princess Pure Brilliance - White - 2.00 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Inv. Setting Square Princess Pure Brilliance</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Setting_Square_Princess/K2515_Square_Princess_Cut_0308_7.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Setting_Square_Princess/Zirconia-Square_Princess_Star-White-28905-FULLRES1.png"
                                       data-video="../img/createdstones/detail_zirconia/Setting_Square_Princess/Swarovski Zirconia - Inv. Setting Square Princess Pure Brilliance - White - 2.00 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Setting_Square_Princess/Zirconia-Square_Princess_Star-White-28905-FULLRES.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                    <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="2.50" data-foo="1">2.50 mm</option>
                           <option value="3.00" data-foo="2">3.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 11 -->
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Baguette_Princess_Pure/Zirconia-Baguette_Step-White-500x500px1.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Baguette_Princess_Pure/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Baguette_Princess_Pure/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Baguette Princess Pure Brilliance Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Baguette_Princess_Pure/K2515_BaguettePrincessCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Baguette_Princess_Pure/Zirconia-Baguette_Step-White-500x500px1.png"
                                       data-video="../img/createdstones/detail_zirconia/Baguette_Princess_Pure/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Baguette_Princess_Pure/Zirconia-Baguette_Step-White-500x500px.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x2" data-foo="0">3x2 mm</option>
                           <option value="4x2" data-foo="1">4x2 mm</option>
                           <option value="5x2.5" data-foo="2">5x2.5 mm</option>
                           <option value="6x3" data-foo="3">6x3 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>


         <!-- 12 -->
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Baguette_Step_Cut/Zirconia-Elongated_Baguette-White-29037-CMYK-4000px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Baguette_Step_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Baguette_Step_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Baguette Step Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Baguette_Step_Cut/K2515_Baguette2StepCut_NEU.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Baguette_Step_Cut/Zirconia-Elongated_Baguette-White-29037-CMYK-4000px-0039.png"
                                       data-video="../img/createdstones/detail_zirconia/Baguette_Step_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Baguette_Step_Cut/Zirconia-Elongated_Baguette-White-29037-CMYK-4000px-00391.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x2" data-foo="0">3x2 mm</option>
                           <option value="4x2" data-foo="1">4x2 mm</option>
                           <option value="5x2.5" data-foo="2">5x2.5 mm</option>
                           <option value="6x3" data-foo="3">6x3 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 13 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Tapered_Baguette_Cut/Zirconia-Tapered_Baguette-White-29030-CMYK-4000px-00391.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Tapered_Baguette_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Tapered_Baguette_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Tapered Baguette Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Tapered_Baguette_Cut/K2515_TaperedBaguetteCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Tapered_Baguette_Cut/Zirconia-Tapered_Baguette-White-29030-CMYK-4000px-00391.png"
                                       data-video="../img/createdstones/detail_zirconia/Tapered_Baguette_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Tapered_Baguette_Cut/Zirconia-Tapered_Baguette-White-29030-CMYK-4000px-0039.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.5x1.5x1" data-foo="0">2.5x1.5x1 mm</option>
                           <option value="2.5x2x1.5" data-foo="1">2.5x2x1.5 mm</option>
                           <option value="3x2x1" data-foo="2">3x2x1 mm</option>
                           <option value="3x2.5x1.5" data-foo="3">3x2.5x1.5 mm</option>
                           <option value="3.5x1.5x1" data-foo="4">3.5x1.5x1 mm</option>
                           <option value="3.5x2.5x1.5" data-foo="5">3.5x2.5x1.5 mm</option>
                           <option value="4x2x1.5" data-foo="6">4x2x1.5 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 14 -->
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Triangle_Step_Cut/Zirconia-Triangle_Step-White-29139-CMYK-4000px-0039.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Triangle_Step_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Triangle_Step_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Triangle Step Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Triangle_Step_Cut/K2515_TriangleStepCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Triangle_Step_Cut/Zirconia-Triangle_Step-White-29139-CMYK-4000px-0039.png"
                                       data-video="../img/createdstones/detail_zirconia/Triangle_Step_Cut/Swarovski Zirconia - Baguette Princess Pure Brilliance Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Triangle_Step_Cut/Zirconia-Triangle_Step-White-29139-CMYK-4000px-00391.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3.00" data-foo="0">3.00 mm</option>
                           <option value="4.00" data-foo="1">4.00 mm</option>
                           <option value="5.00" data-foo="2">5.00 mm</option>
                           <option value="6.00" data-foo="3">6.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>
        
         <div class="component-headline">    
          <div class="headline_title">
            <h2><br>CLASSICS WITH A TWIST</h2>
          </div> 
          <div class="headline_text"></div>
         </div>

         <!-- 15 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/88_Facets_Cut/Zirconia_88_Facets_White.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/88_Facets_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/88_Facets_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>88 Facets Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/88_Facets_Cut/K2515_88_facets_cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/88_Facets_Cut/Zirconia_88_Facets_White.png"
                                       data-video="../img/createdstones/detail_zirconia/88_Facets_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/88_Facets_Cut/Zirconia_88_Facets_White1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="2.50" data-foo="1">2.50 mm</option>
                           <option value="3.00" data-foo="2">3.00 mm</option>
                           <option value="3.50" data-foo="3">3.50 mm</option>
                           <option value="4.00" data-foo="4">4.00 mm</option>
                           <option value="4.50" data-foo="5">4.50 mm</option>
                           <option value="5.00" data-foo="6">5.00 mm</option>
                           <option value="6.00" data-foo="7">6.00 mm</option>
                           <option value="6.50" data-foo="8">6.50 mm</option>
                           <option value="7.00" data-foo="9">7.00 mm</option>
                           <option value="8.00" data-foo="10">8.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>
         
          <!-- 16 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/100_Facets_Cut/Swarovski_Zirconia_100-Facets_White_Front_21.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/100_Facets_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/100_Facets_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>100 Facets Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/100_Facets_Cut/Swarovski_Zirconia_100-Facets_00210_2.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/100_Facets_Cut/Swarovski_Zirconia_100-Facets_White_Front_21.png"
                                       data-video="../img/createdstones/detail_zirconia/100_Facets_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/100_Facets_Cut/Swarovski_Zirconia_100-Facets_White_Front_2.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="3.00" data-foo="1">3.00 mm</option>
                           <option value="4.00" data-foo="2">4.00 mm</option>
                           <option value="5.00" data-foo="3">5.00 mm</option>
                           <option value="6.00" data-foo="4">6.00 mm</option>
                           <option value="6.50" data-foo="5">6.50 mm</option>
                           <option value="7.00" data-foo="6">7.00 mm</option>
                           <option value="8.00" data-foo="7">8.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>
         
          <!-- 17 -->

          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/120_Facets_Heritage_Cut/Zirconia-120_Facets_Cut-White-xxxxx-CMYK-4000px-0039_21.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/120_Facets_Heritage_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/120_Facets_Heritage_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>120 Facets Heritage Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/120_Facets_Heritage_Cut/K2515_120facetsCut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/120_Facets_Heritage_Cut/Zirconia-120_Facets_Cut-White-xxxxx-CMYK-4000px-0039_21.png"
                                       data-video="../img/createdstones/detail_zirconia/120_Facets_Heritage_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/120_Facets_Heritage_Cut/Zirconia-120_Facets_Cut-White-xxxxx-CMYK-4000px-0039_2.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3.00" data-foo="0">3.00 mm</option>
                           <option value="4.00" data-foo="1">4.00 mm</option>
                           <option value="5.00" data-foo="2">5.00 mm</option>
                           <option value="6.00" data-foo="3">6.00 mm</option>
                           <option value="6.50" data-foo="4">6.50 mm</option>
                           <option value="7.00" data-foo="5">7.00 mm</option>
                           <option value="8.00" data-foo="6">8.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

          <!-- 18 -->
          <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Imperial_Mosaic_Cut/Zirconia_Imperial_Mosaic_White_00431.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Imperial_Mosaic_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Imperial_Mosaic_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Imperial Mosaic Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Imperial_Mosaic_Cut/K2515_ImperialCut_0708.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Imperial_Mosaic_Cut/Zirconia_Imperial_Mosaic_White_00431.png"
                                       data-video="../img/createdstones/detail_zirconia/Imperial_Mosaic_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Imperial_Mosaic_Cut/Zirconia_Imperial_Mosaic_White_0043.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3.00" data-foo="0">3.00 mm</option>
                           <option value="4.00" data-foo="1">4.00 mm</option>
                           <option value="5.00" data-foo="2">5.00 mm</option>
                           <option value="6.00" data-foo="3">6.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                 </div> 
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 19 -->
         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Antique_Cushion_Checkerboard_Cut/Zirconia-Antique_Cushion_Checkerboard-White-xxxxx-RGB-4000px.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Antique_Cushion_Checkerboard_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Antique_Cushion_Checkerboard_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Antique Cushion Checkerboard Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Antique_Cushion_Checkerboard_Cut/Antique-Cushion_square_cut-onwhite.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Antique_Cushion_Checkerboard_Cut/Zirconia-Antique_Cushion_Checkerboard-White-xxxxx-RGB-4000px.png"
                                       data-video="../img/createdstones/detail_zirconia/Antique_Cushion_Checkerboard_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Antique_Cushion_Checkerboard_Cut/Zirconia-Antique_Cushion_Checkerboard-White-xxxxx-RGB-4000px1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x3" data-foo="0">3x3 mm</option>
                           <option value="4x4" data-foo="1">4x4 mm</option>
                           <option value="5x5" data-foo="2">5x5 mm</option>
                           <option value="6x6" data-foo="3">6x6 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 20 -->

         <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Octagon_Sun_Cut/Swarovski-Zirconia-Octagon_Sun-White-Front1.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Octagon_Sun_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Octagon_Sun_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Octagon Sun Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Octagon_Sun_Cut/OctagonSun.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Octagon_Sun_Cut/Swarovski-Zirconia-Octagon_Sun-White-Front1.png"
                                       data-video="../img/createdstones/detail_zirconia/Octagon_Sun_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Octagon_Sun_Cut/Swarovski-Zirconia-Octagon_Sun-White-Front.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3.00" data-foo="0">3.00 mm</option>
                           <option value="4.00" data-foo="1">4.00 mm</option>
                           <option value="5.00" data-foo="2">5.00 mm</option>
                           <option value="6.00" data-foo="3">6.00 mm</option>
                           <option value="" data-foo="4"> mm</option>
                           <option value="" data-foo="5"> mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>

         <!-- 21 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Round_Checkerboard_Cut/Swarovski-Zirconia-Round_Checkerboard-White-Front.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Round_Checkerboard_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Round_Checkerboard_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Round Checkerboard Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Round_Checkerboard_Cut/RDCheckerboard.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Checkerboard_Cut/Swarovski-Zirconia-Round_Checkerboard-White-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Checkerboard_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Checkerboard_Cut/Swarovski-Zirconia-Round_Checkerboard-White-Front1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="3.00" data-foo="1">3.00 mm</option>
                           <option value="4.00" data-foo="2">4.00 mm</option>
                           <option value="5.00" data-foo="3">5.00 mm</option>
                           <option value="6.00" data-foo="4">6.00 mm</option>
                           <option value="6.50" data-foo="5">6.50 mm</option>
                           <option value="7.00" data-foo="6">7.00 mm</option>
                           <option value="8.00" data-foo="7">8.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

         <div class="component-headline">    
          <div class="headline_title">
            <h2><br>EXPERIMENTALS</h2>
          </div> 
          <div class="headline_text"></div>
         </div>

        <!-- 22 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Puresse_Cut/Zirconia-Puresse-White-xxxxx-FULLRES1.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Puresse_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Puresse_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Puresse Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Puresse_Cut/K2515_Puresse.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Puresse_Cut/Zirconia-Puresse-White-xxxxx-FULLRES1.png"
                                       data-video="../img/createdstones/detail_zirconia/Puresse_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Puresse_Cut/Zirconia-Puresse-White-xxxxx-FULLRES.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="3.00" data-foo="1">3.00 mm</option>
                           <option value="4.00" data-foo="2">4.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

         <!-- 23 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Pentagon_Cut/Zirconia-Pentagon-White-xxxxx-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Pentagon_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Pentagon_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Pentagon Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Pentagon_Cut/K2515_Pentagon.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Pentagon_Cut/Zirconia-Pentagon-White-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Pentagon_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Pentagon_Cut/Zirconia-Pentagon-White-xxxxx-FULLRES1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="1.5x1.5" data-foo="0">1.5x1.5 mm</option>
                           <option value="2x2" data-foo="1">2x2 mm</option>
                           <option value="2.5x2.5" data-foo="2">2.5x2.5 mm</option>
                           <option value="3x3" data-foo="3">3x3 mm</option>
                           <option value="4x4" data-foo="4">4x4 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 24 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Bloom_Cut/Zirconia-Bloom-White-xxxxx-RGB-4000px-00431.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Bloom_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Bloom_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Bloom Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Bloom_Cut/Bloom_Cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Bloom_Cut/Zirconia-Bloom-White-xxxxx-RGB-4000px-00431.png"
                                       data-video="../img/createdstones/detail_zirconia/Bloom_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Bloom_Cut/Zirconia-Bloom-White-xxxxx-RGB-4000px-0043.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.5x2.5" data-foo="0">2.5x2.5 mm</option>
                           <option value="3x3" data-foo="1">3x3 mm</option>
                           <option value="4x4" data-foo="2">4x4 mm</option>
                           <option value="5x5" data-foo="3">5x5 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

         <!-- 25 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Dahlia_Cut/Zirconia-Dahlia-White-xxxxx-FULLRES.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Dahlia_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Dahlia_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Dahlia Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Dahlia_Cut/DAHLIA_CUT.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Dahlia_Cut/Zirconia-Dahlia-White-xxxxx-FULLRES.png"
                                       data-video="../img/createdstones/detail_zirconia/Dahlia_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Dahlia_Cut/Zirconia-Dahlia-White-xxxxx-FULLRES1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3.00" data-foo="0">3.00 mm</option>
                           <option value="4.00" data-foo="1">4.00 mm</option>
                           <option value="5.00" data-foo="2">5.00 mm</option>
                           <option value="6.00" data-foo="3">6.00 mm</option>
                           <option value="7.00" data-foo="4">7.00 mm</option>
                           <option value="8.00" data-foo="5">8.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

         <!-- 26 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Round_Rosebush_Cut/Zirconia-Round_Rosebush-White-xxxxx-RGB-4000px-00431.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Round_Rosebush_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Round_Rosebush_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Round Rosebush Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Round_Rosebush_Cut/Rou-Rosebush-Cut-1.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Round_Rosebush_Cut/Zirconia-Round_Rosebush-White-xxxxx-RGB-4000px-00431.png"
                                       data-video="../img/createdstones/detail_zirconia/Round_Rosebush_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Round_Rosebush_Cut/Zirconia-Round_Rosebush-White-xxxxx-RGB-4000px-0043.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="3.00" data-foo="1">3.00 mm</option>
                           <option value="4.00" data-foo="2">4.00 mm</option>
                           <option value="5.00" data-foo="3">5.00 mm</option>
                           <option value="6.00" data-foo="4">6.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 26 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Side_View_Cut/Swarovski_Zirconia_Side-View_White_Front1.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Side_View_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Side_View_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Side View Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Side_View_Cut/sideviewcut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Side_View_Cut/Swarovski_Zirconia_Side-View_White_Front1.png"
                                       data-video="../img/createdstones/detail_zirconia/Side_View_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Side_View_Cut/Swarovski_Zirconia_Side-View_White_Front.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="5x4" data-foo="0">5x4 mm</option>
                           <option value="6.25x5" data-foo="1">6.25x5 mm</option>
                           <option value="7.5x6" data-foo="2">7.5x6 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 27 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Leaf_Cut/Swarovski_Zirconia_Leaf_White_Front.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Leaf_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Leaf_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Leaf Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Leaf_Cut/Leaf_Cut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Leaf_Cut/Swarovski_Zirconia_Leaf_White_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Leaf_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Leaf_Cut/Swarovski_Zirconia_Leaf_White_Front1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3.00" data-foo="0">3.00 mm</option>
                           <option value="4.00" data-foo="1">4.00 mm</option>
                           <option value="5.00" data-foo="2">5.00 mm</option>
                           <option value="6.00" data-foo="3">6.00 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 28 -->

        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Violet_Cut/Swarovski_Zirconia_Violet_White_Front.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Violet_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Violet_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Violet Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Violet_Cut/violetcut.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Violet_Cut/Swarovski_Zirconia_Violet_White_Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Violet_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Violet_Cut/Swarovski_Zirconia_Violet_White_Front1.png">                                                
                                  </a>
                                 </li>
                              
                              </ul>
                           </li>
                           <li>                            </li>
                           <!-- <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x3" data-foo="0">3x3 mm</option>
                           <option value="4x4" data-foo="1">4x4 mm</option>
                           <option value="5x5" data-foo="2">5x5 mm</option>
                           <option value="6x6" data-foo="3">6x6 mm</option>
                        </select>
                        <br>
                     </span>
                    
                     <div class="colorshare">
                        <span class="cutcolor"><a href="javascript:void(0)">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
        </div>

        <!-- 29 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/big_img/Swarovski-Zirconia-Liquid_Oval-White-Front.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski Zirconia - Liquid Oval Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski Zirconia - Liquid Oval Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Liquid Oval Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/01100640.SA.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/big_img/Swarovski-Zirconia-Liquid_Oval-White-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski Zirconia - Liquid Oval Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski-Zirconia-Liquid_Oval-White-Front.png">                                                
                                  </a>
                                 </li>
                                  <li>                                                
                                  <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/big_img/Swarovski-Zirconia-Liquid_Oval-Fancy_Light_Blue-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski Zirconia - Liquid Oval Cut - Fancy Light Blue (TCF) - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski-Zirconia-Liquid_Oval-Fancy_Light_Blue-Front.png">                                                
                                  </a>
                                 </li>
                                   <li>                                                
                                  <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/big_img/Swarovski-Zirconia-Liquid_Oval-Mint-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski Zirconia - Liquid Oval Cut - Mint (TCF) - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski-Zirconia-Liquid_Oval-Mint-Front.png">                                                
                                  </a>
                                 </li>
                                 <li>                                                
                                  <a href="javascript:void(0)" data-color="spring-green">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/big_img/Swarovski-Zirconia-Liquid_Oval-Spring_Green-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski Zirconia - Liquid Oval Cut - Spring Green - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski-Zirconia-Liquid_Oval-Spring_Green-Front.png">                                                
                                  </a>
                                 </li>
                                <li>                                                
                                  <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/big_img/Swarovski-Zirconia-Liquid_Oval-Fancy_Morganite-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski Zirconia - Liquid Oval Cut - Fancy Morganite - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Oval_Cut/Swarovski-Zirconia-Liquid_Oval-Fancy_Morganite-Front.png">                                                
                                  </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                         <!--   <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x2" data-foo="0">3x2 mm</option>
                           <option value="5x3" data-foo="1">5x3 mm</option>
                           <option value="6x4" data-foo="2">6x4 mm</option>
                           <option value="7x5" data-foo="3">7x5 mm</option>
                           <option value="8x6" data-foo="4">8x6 mm</option>
                        </select>
                        <br>
                     </span>
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>  

         <!-- 30 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/big_img/Swarovski-Zirconia-Liquid_Square-White-Front.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Liquid Square Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/04100500.SA_MW.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/big_img/Swarovski-Zirconia-Liquid_Square-White-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski Zirconia - Liquid Pear Cut - White - 3x2 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski-Zirconia-Liquid_Square-White-Front.png">                                                
                                  </a>
                                 </li>
                                  <li>                                                
                                  <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/big_img/Swarovski-Zirconia-Liquid_Square-Fancy_Light_Blue-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski Zirconia - Liquid Square Cut - Fancy Light Blue (TCF) - 3.00 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski-Zirconia-Liquid_Square-Fancy_Light_Blue-Front.png">                                                
                                  </a>
                                 </li>
                                   <li>                                                
                                  <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/big_img/Swarovski-Zirconia-Liquid_Square-Mint-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski Zirconia - Liquid Square Cut - Mint (TCF) - 3.00 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski-Zirconia-Liquid_Square-Mint-Front.png">                                                
                                  </a>
                                 </li>
                                 <li>                                                
                                  <a href="javascript:void(0)" data-color="spring-green">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/big_img/Swarovski-Zirconia-Liquid_Square-Spring_Green-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski Zirconia - Liquid Square Cut - Spring Green - 3.00 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski-Zirconia-Liquid_Square-Spring_Green-Front.png">                                                
                                  </a>
                                 </li>
                                <li>                                                
                                  <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/big_img/Swarovski-Zirconia-Liquid_Square-Fancy_Morganite-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski Zirconia - Liquid Square Cut - Fancy Morganite - 3.00 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Square_Cut/Swarovski-Zirconia-Liquid_Square-Fancy_Morganite-Front.png">                                                
                                  </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                         <!--   <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="2.00" data-foo="0">2.00 mm</option>
                           <option value="3.00" data-foo="1">3.00 mm</option>
                           <option value="4.00" data-foo="2">4.00 mm</option>
                           <option value="5.00" data-foo="3">5.00 mm</option>
                           <option value="6.00" data-foo="4">6.00 mm</option>
                        </select>
                        <br>
                     </span>
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>  
               </div>
              
            </div>
            <div class="borderblack screen"></div>
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>  
         
          <!-- 31 -->
        <div class="component-product featured col-md-12" data-cut="round-brilliant-cut" itemscope="" itemtype="http://schema.org/Product">
            <div class="row">
               <div class="borderblack mobile"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 cutpreview">
                  <div class="cutpreview_children">
                  <img src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/big_img/Swarovski-Zirconia-Liquid_Pear-White-Front.png" alt="">
                  <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski Zirconia - Liquid Square Cut - Fancy Morganite - 3.00 mm_2.mp4">
                     <source type="video/mp4" src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski Zirconia - Liquid Square Cut - Fancy Morganite - 3.00 mm_2.mp4">
                  </video>
               </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 cutoverview grey">
                <div class="cutoverview_child">
                  <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 w50">
                        <div style="position:relative; ">
                          SWAROVSKI ZIRCONIA                        
                           <h2>Liquid Pear Cut</h2>
                        </div>
                     <img class="img-fluid  lazyloaded" alt="Genuine Sapphire - round-brilliant-cut" id="cutdetails" src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/03100640.SA.png">                        
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 choice_gems">
                     <div class="row color-row justify-content-center">
                     <div class="colorselect">
                        <div class="select-color">{{trans('products.select_color')}}:</div>
                        <ul>
                           <!-- <li class="left">                                <a href="#"><img src="../img/gemstones/detail/arrow-left.png" title="Scroll left"></a>                            </li> -->
                           <li>
                              <ul class="coloritems" rel="435" value="1">
                                 <li class="active">                                                
                                  <a href="javascript:void(0)" data-color="white">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/big_img/Swarovski-Zirconia-Liquid_Pear-White-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski Zirconia - Liquid Square Cut - Fancy Morganite - 3.00 mm_2.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski-Zirconia-Liquid_Pear-White-Front.png">                                                
                                  </a>
                                 </li>
                                  <li>                                                
                                  <a href="javascript:void(0)" data-color="fancy-light-blue-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/big_img/Swarovski-Zirconia-Liquid_Pear-Fancy_Light_Blue-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski Zirconia - Liquid Pear Cut - Fancy Light Blue (TCF) - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski-Zirconia-Liquid_Pear-Fancy_Light_Blue-Front.png">                                                
                                  </a>
                                 </li>
                                   <li>                                                
                                  <a href="javascript:void(0)" data-color="mint-(tcf)">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/big_img/Swarovski-Zirconia-Liquid_Pear-Mint-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski Zirconia - Liquid Pear Cut - Mint (TCF) - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski-Zirconia-Liquid_Pear-Mint-Front.png">                                                
                                  </a>
                                 </li>
                                 <li>                                                
                                  <a href="javascript:void(0)" data-color="spring-green">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/big_img/Swarovski-Zirconia-Liquid_Pear-Spring_Green-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski Zirconia - Liquid Pear Cut - Spring Green - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski-Zirconia-Liquid_Pear-Spring_Green-Front.png">                                                
                                  </a>
                                 </li>
                                <li>                                                
                                  <a href="javascript:void(0)" data-color="fancy-morganite">                                                    
                                    <span class="glow"></span>                                                    
                                    <img class="lazyloaded" alt="Genuine Sapphire - round-brilliant-cut - Very Light Blue"  
                                       data-src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/big_img/Swarovski-Zirconia-Liquid_Pear-Fancy_Morganite-Front.png"
                                       data-video="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski Zirconia - Liquid Pear Cut - Fancy Morganite - 5x3 mm.mp4"
                                    src="../img/createdstones/detail_zirconia/Liquid_Pear_Cut/Swarovski-Zirconia-Liquid_Pear-Fancy_Morganite-Front.png">                                                
                                  </a>
                                 </li>
                              </ul>
                           </li>
                           <li>                            </li>
                         <!--   <li class="right">                                <a href="javascript:void(0)"><img src="../img/gemstones/detail/arrow-right.png"> </a>                            </li> -->
                        </ul>
                     </div>
                     </div>
                     <span class="cutsize">{{trans('products.select_size')}}<br></span>
                     <span class="cutsizes">
                        <select>
                           <option value="3x2" data-foo="0">3x2 mm</option>
                           <option value="5x3" data-foo="1">5x3 mm</option>
                           <option value="6x4" data-foo="2">6x4 mm</option>
                           <option value="7x5" data-foo="3">7x5 mm</option>
                           <option value="8x6" data-foo="4">8x6 mm</option>
                        </select>
                        <br>
                     </span>
                     <div class="colorshare">
                        <span class="cutcolor"><a href="https://www.swarovski-gemstones.com/products/Swarovski_Sapphire/en/round-brilliant-cut/very-light-blue/0.70">White</a></span>
                     </div>
                   
                     <span class="addinfos"></span>
                     </div>                
                  </div>
                </div>
               </div>
              
            </div>
            <!-- <div class="borderblack screen"></div> -->
            <img class="hidden" itemprop="image" src="//www.swarovski-gemstones.com/products/Genuine_Blue_Sapphire-Round_Brilliant-Very_Light_Blue-CMYK-2.png" alt="Genuine Sapphire - round-brilliant-cut">        <span class="hidden" itemprop="name">SWAROVSKI GENUINE SAPPHIRE</span>        <span class="hidden" itemprop="sku"></span>        <span class="hidden" itemprop="model">Round Brilliant Cut</span>                                            
         </div>    

      </div>
   </div>
   <!--<div class="row contentrow"></div>-->
   <!--UdmComment-->
</div>
            <!--UdmComment-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
   $(document).ready(function() {
      $('ul.coloritems').filter(function(){
         if($(this).children("li").length <= 5){
               $(this).css('marginLeft', '0px');
               // $(this).closest('.select-color').css('marginLeft', '0px');
         } 
      })  
   });
 $(document).on("click", ".colorselect .left", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left; - 1 * position >= 48 ? ($(this).parent().find(".coloritems").css("left", position + 48), $(this).parent().find(".colorselect .right").removeClass("arrowhide")) : $(this).parent().find(".colorselect .left").addClass("arrowhide")
        }), 
        $(document).on("click", ".colorselect .right", function(event) {
            event.preventDefault();
            var position = $(this).parent().find(".coloritems").position().left,
                maxleft = $(this).parent().find(".coloritems").attr("rel");
            maxleft > -1 * position && maxleft >= 0 ? ($(this).parent().find(".coloritems").css("left", position - 48), $(this).parent().find(".colorselect .left").removeClass("arrowhide")) : $(this).parent().find(".colorselect .right").addClass("arrowhide")
        });


       
         $(document).on("click", ".coloritems a", function(event) {
            var imgUrl  = $(this).find('img.lazyloaded').data('src');
            var videoUrl  = $(this).find('img.lazyloaded').data('video');
            var parentElement = $(this).closest('.coloritems');
            parentElement.find('li').removeClass('active');
            // $('.coloritems li').removeClass('active'); 
            $(this).parent('li').addClass('active');
            var colorUrl = $(this).data('color');
            console.log(colorUrl);
            $(this).closest('.choice_gems').find('.colorshare .cutcolor a').html(colorUrl.replace("-"," "));
            var Component_Product =  $(this).closest('.component-product');
            Component_Product.find('.cutpreview').find('img').attr('src','');
            Component_Product.find('.cutpreview').find('video').attr('src', '');
            Component_Product.find('.cutpreview').find('img').attr('src', imgUrl);
            Component_Product.find('.cutpreview').find('video').attr('src', videoUrl);
            Component_Product.find('.cutpreview').find('img').css('display', 'block');
            Component_Product.find('.cutpreview').find('video').css('display', 'none');
           Component_Product.find('.cutpreview').css('background', 'transparent url(../img/gemstones/detail/grey.jpg) 15px 0 repeat-y;ne');
        })

          // $(document).on('mousemove',".cutpreview",function(){
          //      $(this).find('img').css('display', 'none');
          //      $(this).find('video').css('display', 'block');
          //      $(this).css('background', 'none');
          // })

      </script>
@endsection