<style>
li.dropdown-lang{
  position: relative;
}
/*li.dropdown-lang > a:before {
  content : "";
  position: absolute;
  height: 30px;
  width: 1px;
  top: 50%;
  left: 0;
  margin-top: -10px;
  display: inline-block;
  background-color: #dfe0e3;
}*/

li.dropdown-lang > a img,li.dropdown-lang.open > a img{
  width: 30px;
  height: 30px;
}

li.dropdown-lang > a,li.dropdown-lang.open > a{
  padding: 10px 8px 8px 30px !important;
  position: relative;
  background:none;
  color: #303030;
  margin-top: 7px;
}
li.dropdown-lang > a span.caret-lang{
  display: inline-block;
  width: 10px;
  height: 10px;
  position: relative;
}
li.dropdown-lang .dropdown-menu.drop_language{
  border-radius: 8px;
  background-color: #ffffff;
  box-shadow: 0 8px 20px 0 rgba(0, 0, 0, 0.1);
  border: solid 1px #e3e3e3;
  min-width: 130px;
}
li.dropdown-lang .dropdown-menu.drop_language a{
  padding-top: 5px;
  padding-bottom: 5px;
  font-size: 12px;
}
li.dropdown-lang .dropdown-menu.drop_language img{
  width: 20px;
  height: 20px;
}
#languageSelect{
  padding-left:10px !important;
}

.dropdown-menu .children_product{
  text-transform: capitalize;
  padding-left: 1.9rem;
}

/*.rightsite ul.pull-right{
  padding-right: 40px;
}*/
#navbarNavDropdown .nav-item.languageSelect_mobi span{
  float: right;
  margin-right: 20px;
}
#navbarNavDropdown .nav-item.languageSelect_mobi a{
  color:rgba(0,0,0,.5)
}
#navbarNavDropdown .nav-item.languageSelect_mobi a.active{
  color:#133A6D;
  font-weight: bold;
}
  @media (max-width: 480px) {
      #siteOptions .siteOptions_Inner .rightsite{
         display: none;
      }
   }
</style>

<header id="header">

   <?php $lang = Cookie::get('netis_language', $browserLang) ?>
   <div id="siteOptions" class="containerFix">
      <div class="containerFix siteOptions_Inner">
        <div class="col-md-12">
         <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-5 leftsite" style="line-height: 25px;">
               <a class="containerFix text-center-xs" href="/">
                    <img id="mainLogo" class="img-responsive" src="/img/netisgemslogo.png" alt="">
                </a>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-7 rightsite" style="line-height: 25px;">
               <ul class="pull-right" style="list-style:none">
                     <li class="dropdown dropdown-lang open" dropdown="">
                      <a class="dropdown-toggle"  href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       @if($lang == 'vi')
                       <img alt="" class="cricle-lang" src="/img/lang-vi.png">
                       @else
                       <img alt="" class="cricle-lang" src="/img/lang-en.png">
                       @endif
                       <span class="fa fa-caret-down"></span>
                      </a>
                      <ul class="dropdown-menu drop_language" role="menu" id="languageSelect">
                       <li><a data-lang="vi" href="javascript:void(0)">
                       <img alt="" src="/img/lang-vi.png"> &nbsp;&nbsp;Tiếng Việt</a></li>
                       <li><a class="" data-lang="en" href="javascript:void(0)">
                       <img alt="" src="/img/lang-en.png"> &nbsp;&nbsp;English</a></li>
                      </ul>
                     </li>
               </ul>
            </div>
          </div>    
         </div>
      </div>
   </div>
<!--    <div id="mainHeader">
      <div id="mainHeader_Inner">
        <div class="col-md-12">
         <div class="row">
            <div class="col-md-4 col-sm-12 mainLogoContainer">
               <div class="row flexBox alignItemsCenter no-row-xs">
                  <div class="col-xs-3 visible-inline-xs">
                     <a class="mobileMenuBtn">
                        <div class="fa fa-bars"></div>
                     </a>
                  </div>
                  <div class="col-xs-6 col-sm-12">
                     <a class="containerFix text-center-xs" href="/">
                     <img id="mainLogo" class="img-responsive" src="img/netisgemslogo.png" alt="">
                     </a>
                  </div>
                  <div id="shoppingCartHeader1" class="col-xs-3 visible-inline-xs text-right shoppingCartInHeader">
                     <a href="" >
                     <span class="fa fa-shopping-cart rightMargin"></span>
                     <span data-bind="loadTemplate:{name:'/htmltemplates/addtocart/cart-count.html', data:$data}"><span class="cartCountContainer" style="" data-bind="visible:true">
                     <span class="cartCount emptyCartCount" data-bind="text: CartCount(), css: { emptyCartCount: CartCount() == 0 }">0</span>
                     </span></span>
                     </a>
                  </div>
               </div>
            </div>
            <div id="searchAndQuickLinks" class="col-md-8 col-sm-9 col-xs-12 pull-right">
               <div class="row">
                  <div class="float-right">
                      <div class="dropdown dropdown-lang open" dropdown="">
                      <a class="dropdown-toggle"  href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <img alt="" class="cricle-lang" src="img/lang-{{$lang}}.png">
                       <span class="fa fa-caret-down"></span>
                      </a>
                      <ul class="dropdown-menu drop_language" role="menu" id="languageSelect">
                       <li><a data-lang="vi" href="javascript:void(0)">
                       <img alt="" src="img/lang-vi.png"> &nbsp;&nbsp;Tiếng Việt</a></li>
                       <li><a class="" data-lang="en" href="javascript:void(0)">
                       <img alt="" src="img/lang-en.png"> &nbsp;&nbsp;English</a></li>
                      </ul>
                     </div>
                    
                  </div>
                  <div class="col-sm-3 col-md-2 col-sm-push-2 hidden-xs hideInShowCase cons-mode-hotkey" data-hotkey-value="Q O" style="padding-right: 0;"></div>
                  <div class="col-sm-7 col-sm-push-2 col-md-8 col-xs-12">
                     <form class="searchForm" method="get" action="javascript:void(0)">
                        <div class="col-md-12">
                           <div class="row" >
                              <div class="noMargin input-group col-xs-12 cons-mode-hotkey" data-hotkey-value="/">
                                 <input class="searchTextInput ui-autocomplete-input" type="text" name="query" maxlength="500" accesskey="S" placeholder=" {{trans('products.search')}}" autocomplete="off" >
                                 <div class="searchButtonContainer input-group-addon">
                                    <span class="StullerButton primary largeButton goSearch">
                                      <button type="submit" value="" ><span></span>
                                      </button></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
        </div> 
      </div>
      <div class="clearfix"></div>
   </div> -->
   <nav id="navigationHeader" >
      <div class="col-md-12">
         <div class="row">
            <ul id="navigationHeader_Inner">
               <li id="productsNavBar" class="showPopUp dropdown">
                  <a class="dropdown-toggle" href="javascript:void(0)" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  {{trans('products.products_home')}} <i class="fa fa-caret-down"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                     <a class="dropdown-item" href="/ourProducts"> {{trans('products.products_home')}}</a>
                     <a class="dropdown-item" href="/genuine-gemstones">{{trans('products.genuine_gemstones')}}</a>
                     <a class="dropdown-item children_product" href="/sapphire">Swarovski Sapphire</a>
                     <a class="dropdown-item children_product" href="/ruby">Swarovski Ruby</a>
                     <a class="dropdown-item children_product" href="/topaz">Swarovski Topaz</a>
                     <a class="dropdown-item children_product" href="/black_spinel">Swarovski Black Spinel</a>
                     <a class="dropdown-item children_product" href="/smoky_quartz">Swarovski Smoky Quartz</a>
                     <a class="dropdown-item children_product" href="/marcasite">Swarovski Marcasite</a>
                     <a class="dropdown-item" href="/created-stones">{{trans('products.created_stones')}}</a>
                     <a class="dropdown-item children_product" href="/zirconia">Swarovski Zirconia</a>
                     <a class="dropdown-item children_product" href="/nano">Swarovski Nano</a>
                     <a class="dropdown-item" href="https://www.swarovskigroup.com/S/aboutus/Swarovski_Company.de.html
                      ">{{trans('products.about_swarovski')}}</a>
                     <a class="dropdown-item children_product" href="/history"> {{trans('products.history')}}</a>
                     <a class="dropdown-item children_product" href="/csr">CSR</a>

                  </div>
               </li>
               <li id="productsNavBar" class="showPopUp dropdown">
                  <a class="dropdown-toggle" href="/our-company" >
                {{trans('products.our_company')}}
                  </a>
               </li>
            <!--    <li id="productsNavBar" class="showPopUp dropdown">
                  <a class="dropdown-toggle" href="javascript:void(0)" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  {{trans('products.about_swarovski')}} <i class="fa fa-caret-down"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink2">
                     <a class="dropdown-item" href="/history"> {{trans('products.history')}}</a>
                     <a class="dropdown-item" href="/csr">CSR</a>
                  </div>
               </li> -->
               <li id="productsNavBar" class="showPopUp dropdown">
                  <a class="dropdown-toggle" href="/news" role="button" id="dropdownMenuLink3"  >
                    {{trans('products.news')}} 
                  </a>
               </li>
               <li id="productsNavBar" class="showPopUp dropdown">
                  <a class="dropdown-toggle" href="/video" role="button" id="dropdownMenuLink4" >
                     Video 
                  </a>
               </li>
               <li id="productsNavBar" class="showPopUp dropdown">
                  <a class="dropdown-toggle" href="/contact-us" role="button" id="" >
                      {{trans('products.contact_us')}}
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </nav>
<nav class="navbar navbar-expand-lg navbar-light bg-light nav_menu_mobi">
  <a class="navbar-brand" href="/">Netis Gems</a>
  <button class="navbar-toggler btn_menu_mobi" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{trans('products.products_home')}} <i class="fa fa-caret-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/ourProducts"> {{trans('products.products_home')}}</a>
          <a class="dropdown-item" href="/genuine-gemstones">{{trans('products.genuine_gemstones')}}</a>
          <a class="dropdown-item children_product" href="/sapphire">Swarovski Sapphire</a>
          <a class="dropdown-item children_product" href="/ruby">Swarovski Ruby</a>
          <a class="dropdown-item children_product" href="/topaz">Swarovski Topaz</a>
          <a class="dropdown-item children_product" href="/black_spinel">Swarovski Black Spinel</a>
          <a class="dropdown-item children_product" href="/smoky_quartz">Swarovski Smoky Quartz</a>
          <a class="dropdown-item children_product" href="/marcasite">Swarovski Marcasite</a>
          <a class="dropdown-item" href="/created-stones">{{trans('products.created_stones')}}</a>
          <a class="dropdown-item children_product" href="/zirconia">Swarovski Zirconia</a>
          <a class="dropdown-item children_product" href="/nano">Swarovski Nano</a>
          <a class="dropdown-item" href="https://www.swarovskigroup.com/S/aboutus/Swarovski_Company.de.html">{{trans('products.about_swarovski')}}</a>
          <a class="dropdown-item children_product" href="/history"> {{trans('products.history')}}</a>
          <a class="dropdown-item children_product" href="/csr">CSR</a>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/our-company" >
                {{trans('products.our_company')}}
        </a>
      </li>
    <!--   <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           {{trans('products.about_swarovski')}} <i class="fa fa-caret-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">
           <a class="dropdown-item" href="/history"> {{trans('products.history')}}</a>
            <a class="dropdown-item" href="/csr">CSR</a>
        </div>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="/news">{{trans('products.news')}}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/video">Video</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/contact-us">{{trans('products.contact_us')}}</a>
      </li>
       <li class="nav-item languageSelect_mobi">
        <p class="nav-link" href="javascript:void(0)">{{trans('products.language')}}:
          <a data-lang="vi" href="javascript:void(0)"><span>VN</span></a>
          <a data-lang="en" href="javascript:void(0)"><span>EN</span></a>
        </p>
      </li>
    </ul>
  </div>
</nav>
</header>