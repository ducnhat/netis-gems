<?php //dd($browserLang); ?>
<!DOCTYPE html>
<html lang="{{ Cookie::get('netis_language',$browserLang) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/png" href="/img/small-logo.png"/>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" type="text/css"  href="{{ asset('css/main.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/slick/slick.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/slick/slick-theme.css') }}"/>    
  @yield('styles')
</head>
<?php $langNetis = Cookie::get('netis_language',$browserLang); ?>
<body>
  @include('layouts.include.header')
  <div id="contentMain">
    @yield('content')
  </div>
  @include('layouts.include.footer')
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('css/slick/js/jquery-1.11.0.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('css/slick/js/jquery-migrate-1.2.1.min.js') }}"></script>
  <script src="{{ asset('vendor/jquery/popper.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
  <script type="text/javascript" src="{{ asset('css/slick/slick.min.js') }}"></script>    
<script>
  $(document).ready(function(){
          var lang_mobi = $('html').attr('lang');
          // console.log(lang_mobi);
          // $('.languageSelect_mobi p a[data-lang='+lang_mobi+']').addClass('active');
          $('.languageSelect_mobi p').find('a[data-lang="'+lang_mobi+'"]').addClass('active');
          $('li.subnav ul.dropdown-menu  li a').each(function(index) {
            if(this.href.trim() == window.location)
            $(this).addClass("active");
          });
  
        //Click event to scroll to top
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop: '0px'}, 500);
        });
        // Khi người dùng cuộn chuột thì gọi hàm scrollFunction
        window.onscroll = function() {scrollFunction()};
        // khai báo hàm scrollFunction
        function scrollFunction() {
        // Kiểm tra vị trí hiện tại của con trỏ so với nội dung trang
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            //nếu lớn hơn 20px thì hiện button
            document.getElementById("myBtn").style.display = "block";
            } else {
            //nếu nhỏ hơn 20px thì ẩn button
            document.getElementById("myBtn").style.display = "none";
            }
        }

        var w_width = $(window).width();
        var w_height = $(window).height();
        // console.log(w_height);
        var footer_height = $('#footer').outerHeight();
        // console.log(footer_height);
        var header_height = $('#header').height();
        // console.log(header_height);
        $('#contentMain').css({'min-height':(w_height - (footer_height+header_height))+'px'});
        $(window).resize(function(){
            var w_height = $(window).height();
            var footer_height = $('#footer').height();
            var header_height = $('#header').height();
            $('#contentMain').css({'min-height':(w_height - (footer_height+header_height))+'px'});
       })

       // var current_url = window.location;
    $('#languageSelect li a').click(function() {
         var d = $(this).data('lang');
         console.log(d);
         $('html').attr('lang',d);
           var c1 = $('html').attr('lang'); 
            $.ajax({
            url: "{{route('Product.Changelanguage')}}",
            type: 'GET',
            data: {language: c1, _token: '{{csrf_token()}}'},
        })
        .done(function(data) {
            console.log(data);
            if(data.status == 1){
                location.reload();
            }
        })
    });

    $('.languageSelect_mobi p a').click(function() {
         var d = $(this).data('lang');
         console.log(d);
         $('html').attr('lang',d);
           var c1 = $('html').attr('lang'); 
            $.ajax({
            url: "{{route('Product.Changelanguage')}}",
            type: 'GET',
            data: {language: c1, _token: '{{csrf_token()}}'},
        })
        .done(function(data) {
            console.log(data);
            if(data.status == 1){
                location.reload();
            }
        })
      });


});
   </script>    
   @yield('scripts')  
</body>
</html>
