@extends('layouts.master')
@section('styles')
      <link rel="stylesheet" href="../css/desgin_competition.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
    <div class="content-main" id="content-main">
        <div class="container container-body">
            <div class="row">

            <!--     <div class="col-xs-12 col-md-2">

                    <div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
                        <div class="btn-group dropdown">
                            <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">

                                <li class="subnav" role="menuitem">
                                    <h5 class="title ">						<a href="/news/News.en.html" class="">						    News						</a>					    </h5>
                                    <ul class="dropdown-menu">

                                        <li role="menuitem" class="folder"> <a href="/news/Industry_News.en.html" class="">						    Industry News						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Productstories.en.html" class="">						    Product Stories						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Innovation_Lab_startpage.en.html" class="">						    Innovation Lab						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/collaborations.en.html" class="">						    Collaborations						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Inspirations.en.html" class="">						    Inspirations						</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <div class="col-xs-12 col-md-12">
                    <div class="row">
                        <div class="meta-nav backshare col-md-12">
                            <div class="col-xs-3 col-sm-6">
                                <a href="javascript:history.go(-1)" class="upper sub"> <span>&lt; {{trans('products.back')}}</span> </a>
                            </div>
<!--                             <div class="col-xs-9 col-sm-6 text-right">

                          
                                <div class="component-addthis-toolbox addthis_default_style addthis_32x32_style" role="menuitem">
                                    <a class="addthis_button_facebook ir  ir-social ir-facebook" href="http://www.facebook.com/share.php?u=" onclick="return facebook_share(520, 350)" target="_blank" title="Share This on Facebook"></a>
                                    <a class="addthis_button_twitter ir  ir-social ir-twitter" href="http://twitter.com/intent/tweet?status=%20+-+%20" onclick="return twitter_share(520, 350)" target="_blank" title="Share This on Twitter"></a>
                                    <div id="pinterest" class="pinterest-top" style="display: inline-block;"> <a data-pin-do="buttonBookmark" style="width:30px;" target="_blank" data-pin-custom="true" data-pin-log="button_pinit_bookmarklet" data-pin-href="https://www.pinterest.com/pin/create/button/"><span class="addthis_button_pinterest_share ir  ir-social ir-pinterest" title="Pin This on Pinterest">
                                    </span>            </a>
                                    </div>
                                    <a class="addthis_button_sinaweibo ir  ir-social ir-weibo" href="http://service.weibo.com/share/share.php?url=%23.VQHHV7iznfQ.sinaweibo&amp;title=" onclick="return weibo_share(520, 350)" target="_blank" title="Share This on Weibo"></a>
                                    <a class="addthis_button_linkedin ir  ir-social ir-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=&amp;title=" onclick="return linkedin_share(520, 425)" target="_blank" title="Share This on LinkedIn"></a>
                                    <a class="addthis_button_email ir ir-social ir-email" style="cursor: pointer;" title="Share via Email" onclick="javascript:window.location='mailto:?subject='+ encodeURIComponent( $('title').text() ) +'&amp;body=Here\'s a link to an interesting article I discovered on Swarovski-Gemstones\.com: ' + window.location.href;"></a>
                                </div>
                          

                            </div> -->
                        </div>
                    </div>
                    <!--/UdmComment-->
                    <div class="row contentrow">
                        @if($langNetis == 'vi')
                        <div class="component-headline">
                            <div class="headline_title">
                                <h2>HỘI THI THIẾT KẾ </h2></div>
                            <div class="headline_text">
                                <p><b>TỔ CHỨC HỘI THI THIẾT KẾ TRANG SỨC TẠI NGA</b></p>
                            </div>
                        </div>
                 <!--        <div class="component-promo featured col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">

                                        <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg"> </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <p class="slide-meta hidden"> </p>
                                       
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <p class="slide-meta hidden"> </p>
                                       
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>Ngành trang sức phát triển mạnh dựa trên những ý tưởng mới mẻ và sáng tạo, Cuộc thi thiết kế của Swarovski đã sẵn sàng để tiếp sức cho những ý tưởng về trang sức và đồng hồ tại Nga. Sử dụng loại sản phẩm Đá quý và Đá Nhân Tạo Swarovski, các thí sinh sẽ truyền cảm hứng cho thiết kế hiện đại và chính thống của Nga sẽ khuynh đảo thị trường.&nbsp;&nbsp;</p>
                                            <p>Qua cuộc thi, Ban giám khảo sẽ tìm kiếm một sự bùng nổ về màu sắc và phong cách theo chủ đề “Thiên đường nhiệt đới” trong các thiết kế của các thí sinh, cảm hứng từ rừng nhiệt đới, từ hoa và các loại trái cây kỳ lạ.&nbsp;</p>
                                            <p>Cuộc thi dành cho các nhà thiết kế, nhà sản xuất và tất cả những người mong muốn được sáng tạo bằng cách sử dụng vật liệu mới. Ứng cử viên phải đến từ Nga hoặc một nước nói tiếng Nga, và đủ 18 tuổi vào thời điểm nộp thiết kế . Thí sinh vào Vòng chung kết có khả năng tham dự hội chợ Junwex vào cuối tháng Chín tại Kostroma, Nga. Đây cũng là nơi Swarovski sẽ công bố người chiến thắng cuộc thi vào cuối hội chợ. Người chiến thắng với thiết kế tổng thể tốt nhất nhận được một chuyến đi đến trụ sở Swarovski ở Wattens, Áo. Các thiết kế chất lượng nhất sẽ có khả năng được công bố trong các ấn phẩm quảng cáo của Swarovski . Một ban giám khảo chuyên nghiệp, gồm các chuyên gia và nhà thiết kế quốc tế sẽ quyết định người chiến thắng.&nbsp;</p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                              <div class="component-headline">
                            <div class="headline_title">
                                <h2>DESIGN COMPETITION</h2></div>
                            <div class="headline_text">
                                <p><b>to Inspire the Russian Jewelry Market</b></p>
                            </div>
                        </div>
                   <!--      <div class="component-promo featured col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">

                                        <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg"> </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <p class="slide-meta hidden"> </p>
                                       
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <p class="slide-meta hidden"> </p>
                                       
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>The jewelry industry thrives on fresh and innovative new ideas, so that is why a Design Competition by Swarovski is poised to invigorate the Russian jewelry and watches landscape with brilliant concepts using the Swarovski Genuine Gemstones and Created Stones product assortment. The intention is to inspire modern, authentic Russian designs that will captivate the market.&nbsp;&nbsp;</p>
                                            <p>Judges will look for an explosion of colors and styles that reflect the theme “Tropical Paradise” in designs that are both visually exciting and distinctly fashion-forward in their presentation. Contestants are invited to draw inspiration from the world’s rainforests and jungles, and from exotic flowers and fruits.&nbsp;</p>
                                            <p>The competition is open to designers, manufacturers and all those who aspire to be creative using innovative materials and styles. Candidates must come from Russia or a Russian-speaking country, and be 18-years-old at the time of submitting their design. Finalists must also be able to attend the Junwex fair at the end of September in Kostroma, Russia, where Swarovski will announce the competition winner at a special dinner held at the end of the fair. The winner of the overall best design receives a trip to the Swarovski Headquarters in Wattens, Austria, and the best designs will have the possibility to be published in selected Swarovski marketing publications. A professional jury, made up of international experts and designers will determine the winning designs.&nbsp; Find out all you need to know about the competition <a target="_blank" href="http://www.crystalit.ru/konkurs-swarovski-gemstones-tropicheskij-raj-dlya-yuvelirov-i-dizajnerov2018" class="" data-gentics-gcn-url="http://www.crystalit.ru/konkurs-swarovski-gemstones-tropicheskij-raj-dlya-yuvelirov-i-dizajnerov2018">here</a>.&nbsp;</p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
@endsection