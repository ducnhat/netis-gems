@extends('layouts.master')
@section('styles')  
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/dancings_stones.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">

    <div class="content-main" id="content-main">

        <!-- Page Content
        ================================================== -->
        <div class="container container-body">
            <div class="row">

             <!--    <div class="col-xs-12 col-md-2">

                    <div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
                        <div class="btn-group dropdown">
                            <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">

                                <li class="subnav" role="menuitem">
                                    <h5 class="title ">						<a href="/news/News.en.html" class="">						    News						</a>					    </h5>
                                    <ul class="dropdown-menu">

                                        <li role="menuitem" class="folder"> <a href="/news/Industry_News.en.html" class="">						    Industry News						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Productstories.en.html" class="">						    Product Stories						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Innovation_Lab_startpage.en.html" class="">						    Innovation Lab						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/collaborations.en.html" class="">						    Collaborations						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Inspirations.en.html" class="">						    Inspirations						</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <div class="col-xs-12 col-md-12">
                    <div class="row">
                        <div class="meta-nav backshare col-md-12">
                            <div class="col-xs-3 col-sm-6">
                                <a href="javascript:history.go(-1)" class="upper sub"> <span>&lt; {{trans('products.back')}}</span> </a>
                            </div>
<!--                             <div class="col-xs-9 col-sm-6 text-right">

                          
                                <div class="component-addthis-toolbox addthis_default_style addthis_32x32_style" role="menuitem">
                                    <a class="addthis_button_facebook ir  ir-social ir-facebook" href="http://www.facebook.com/share.php?u=" onclick="return facebook_share(520, 350)" target="_blank" title="Share This on Facebook"></a>
                                    <a class="addthis_button_twitter ir  ir-social ir-twitter" href="http://twitter.com/intent/tweet?status=%20+-+%20" onclick="return twitter_share(520, 350)" target="_blank" title="Share This on Twitter"></a>
                                    <div id="pinterest" class="pinterest-top" style="display: inline-block;"> <a data-pin-do="buttonBookmark" style="width:30px;" target="_blank" data-pin-custom="true" data-pin-log="button_pinit_bookmarklet" data-pin-href="https://www.pinterest.com/pin/create/button/"><span class="addthis_button_pinterest_share ir  ir-social ir-pinterest" title="Pin This on Pinterest">
                                    </span>            </a>
                                    </div>
                                    <a class="addthis_button_sinaweibo ir  ir-social ir-weibo" href="http://service.weibo.com/share/share.php?url=%23.VQHHV7iznfQ.sinaweibo&amp;title=" onclick="return weibo_share(520, 350)" target="_blank" title="Share This on Weibo"></a>
                                    <a class="addthis_button_linkedin ir  ir-social ir-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=&amp;title=" onclick="return linkedin_share(520, 425)" target="_blank" title="Share This on LinkedIn"></a>
                                    <a class="addthis_button_email ir ir-social ir-email" style="cursor: pointer;" title="Share via Email" onclick="javascript:window.location='mailto:?subject='+ encodeURIComponent( $('title').text() ) +'&amp;body=Here\'s a link to an interesting article I discovered on Swarovski-Gemstones\.com: ' + window.location.href;"></a>
                                </div>
                          

                            </div> -->
                        </div>
                    </div>
                    <!--/UdmComment-->
                    <div class="row contentrow">
              <!--           <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">
                                        <img alt="Banner_Dancing_Stone2.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/Banner_Dancing_Stone2.jpg">                            
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">                   
                                        <p class="slide-meta hidden">                                                     
                                        </p>             
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                          @if($langNetis == 'vi')
                           <div class="component-headline">
                            <div class="headline_title">
                                <h2>ĐÁ NHẢY</h2></div>
                            <div class="headline_text">
                                <p><b>Phong cách mới mang nét trẻ trung tươi vui cho thiết kế</b></p>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">
                                        <!--    <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4" style="width: 100%">
                                             <source type="video/mp4" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4">
                                          </video> -->

                                       <!--  <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg">  --></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>Chuyển động là cuộc sống, nhảy múa là chuyển động, mọi người đều mong muốn món đồ trang sức của mình luôn chuyển động và lấp lánh. Dancing Stone ( Đá Nhảy) là một dòng sản phẩm mới từ Swarovski mang lại những điều kỳ diệu đó. Đá nhảy luôn nằm ở vị trí trung tâm của chiếc nhẫn, bông tai, mặt dây chuyền hoặc đồng hồ. Đá nhảy - Dancing stone, có thể dễ dàng tích hợp vào mọi thiết kế. Đá Nhảy có thề làm từ đá Swarovski Zirconia và Topaz. &nbsp;&nbsp;</p>
                                            <p>Đá Nhảylàm từ chất liệu bạc, hoặc vàng 18k theo yêu cầu. Ngoài màu trắng cón có thể làm với tất cả các màu đá Swarovski Zirconia và đá Topaz. Ngoài đá tròn thuần túy, chúng ta có thể sử dụng Đá 120-Facet, đá 88-Facet, và đá hình trái tim để làm Đá Nhảy . Dancing Stone truyền cảm hứng cho sự sáng tạo mới và khởi đầu cho hàng loạt các thiết kế mới và hiện đại </p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          @else
                           <div class="component-headline">
                            <div class="headline_title">
                                <h2>DANCING STONES</h2></div>
                            <div class="headline_text">
                                <p><b>Innovative preset element adds playfulness to designs</b></p>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">
                                         <!--   <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4" style="width: 100%">
                                             <source type="video/mp4" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4">
                                          </video> -->

                                       <!--  <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg">  --></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>Movement is life, dancing is movement, and sparkle is the effect we all desire from our jewelry. The Dancing Stone is a pre-set element now available from Swarovski that can capture the magic of all three. Always a standout centerpiece in a ring, earrings, pendant or watch, a Dancing Stone, which can be easily integrated into a design, features a unique mechanism that makes it ‘dance'. This delightful element with its irresistible twinkle is now available featuring Swarovski Zirconia and Swarovski Topaz. Dancing Stones introduce surprising stone placement for added on-trend attraction, and guarantee a distinct design twist to any jewelry or watch designs.&nbsp;&nbsp;</p>
                                            <p>Available in silver, or 18ct gold on request, the Dancing Stone comes in pure white, and all available Swarovski Zirconia and Topaz colors. It can be set with the classic round Pure Brilliance Cut, the brilliant 120-Facet Heritage Cut, the 88-Facet Cut, and the Heart Cut for a charming, romantic touch. And for edgy contemporary designs, choose the Dancing Stone in the innovative Side View Cut, which has a diamond-like appearance when viewed from the top of the stone. Dancing Stone is poised to inspire new creativity and unlock a whole range of design possibilities and is now available from Swarovski.</p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          @endif
                       
               <!--          <div class="component-headline">
                            <hr>
                            <div class="headline_title">
                                <h2>discover more</h2></div>
                            <div class="headline_text"></div>
                        </div>
                        <div class="component-focus-content col-xs-12 col-sm-12 col-md-12" id="component-focus-content-related-content-8938159">
                            <div class="item col-xs-12 col-sm-4 col-md-4   text-center" style="display:block;">
                                <a href="javascript:void(0)" target="">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/teaser_Gem_Visions.jpg"></div>
                                    <h5 class="title">Gem Visions 2019</h5></a>
                                <p class="focus-link hide">Swarovski works hard to provide customers both with insight into today’s most crucial breaking trends and the incentive they need to forge new creative territory in their designs. </p>
                                <p class="related-link"><a href="javascript:void(0)" target="">get inspired</a></p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:block;">
                                <a href="javascript:void(0)" target="_blank">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/Teaser_SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg"></div>
                                    <h5 class="title">The competition</h5></a>
                                <p class="focus-link hide"></p>
                                <p class="related-link"><a href="javascript:void(0)" target="_blank">participate here</a></p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:block;">
                                <a href="javascript:void(0)" target="">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/Unbenannt-2_1.jpg"></div>
                                    <h5 class="title">Innovations</h5></a>
                                <p class="focus-link hide">At Swarovski, we endeavor to find inspiring, innovative solutions that add variety, style, and individuality to our extensive product range. Thanks to our detailed research and creative expertise, we continually deliver exciting new materials, colors, shapes, and cuts, as well as inventive new designs and technical features for all of our product categories.</p>
                                <p class="related-link"><a href="javascript:void(0)" target="">see more</a></p>
                            </div>
                        </div> -->
                    </div>

                    <!--UdmComment-->
                </div>
            </div>
            <!-- Return To Top ================================================== -->
            <div class="component-return-to-top clearfix" id="component-return-to-top">
                <button type="button" class="btn btn-link btn-return-to-top pull-right hidden" style="right: -32.5px; bottom: 0px; display: inline-block;">
                    <span>↑</span>
                </button>
            </div>
        </div>

    </div>

</div>
@endsection