@extends('layouts.master')
@section('styles')
<title>Netis Gems</title>
@endsection
@section('content')
<style>
	.tile_img.text-center{
		position: relative;
	}

	.tile_img.text-center .btn_youtube{
		height: 60px ;
		width: 60px;
		position: absolute;
		top: 50%;
		left: 50%;
		transform:translate(-50%,-50%);
	}
	.blogReadMore span img{
		width: 39px;
		height:40px;
	}


</style>
<?php $langNetis = \App::getLocale(); ?>
<div class="container header_img" style="padding:15px 0px;margin:0 auto;">
	<div class="hero_image border">
		<img class="image-responsive" src="img/header_img.jpg" alt="Pearl Center">
		<!-- <img class="image-responsive" src="img/netisgemslogo.png" alt="Pearl Center"> -->
	</div>
</div>
 @if($langNetis == 'vi')
<div class="container-fluid content_img">
	<div class="row flexbox">
		<div class="col-xs-12 col-md-4 flex-child ">
			<div class="tile">
				<div class="tile_img text-center"><img class="image-responsive" src="img/gem_1.jpg" alt="Diamonds and Gemstones"></div>
				<div class="tile_body">
					<span class="tile_body_title">ĐÁ QUÝ VÀ ĐÁ NHÂN TẠO CHÍNH HÃNG SWAROVSKIt</span>
					<span class="tile_body_subtitle"> Thương hiệu đá Swarovski là một di sản có 122 năm lịch sử với các tiêu chuẩn nghiêm ngặt trong việc giới thiệu những phẩm phẩm cho thị trường. Netis Gems tự hào hợp tác với Swarovski để cung cấp đá quý  chất lượng cao nhiều màu sắc độ chính xác tốt nhất.</span>
				</div>
				<div class="tile_cta" >
					<span class="primary LargeButton StullerButton">
						<a href="/ourProducts" class="customButton">
							Xem Thêm
						</a>
					</span>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-md-4 flex-child ">
			<div class="tile">
				<div class="tile_img text-center"><img class="image-responsive" src="img/Teaser_Dancing_Stone2.jpg" alt="Diamonds and Gemstones"></div>
				<div class="tile_body">
					<span class="tile_body_title">ĐÁ NHẢY</span>
					<span class="tile_body_subtitle">Chuyển động là cuộc sống, nhảy múa là chuyển động, mọi người đều mong muốn món đồ trang sức của mình luôn chuyển động và lấp lánh. Dancing Stone ( Đá Nhảy) là một dòng sản phẩm mới từ Swarovski mang lại những điều kỳ diệu đó. </span>
				</div>
				<div class="tile_cta">
					<span class="primary LargeButton StullerButton">
						<a href="/dancings_stones"  class="customButton">
							Xem Thêm
						</a>
					</span>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-md-4 flex-child">
			<div class="tile">
				<div class="tile_img text-center" style="position: relative;">
					<a href="/video">
					<img class="image-responsive" src="../img/0.jpg">
					<img src="../img/youtube_button.png" class="btn_youtube" alt="">
					</a>
				</div>
				<div class="tile_body">
					<span class="tile_body_title">Videos</span>
					<span class="tile_body_subtitle">Cùng xem, khám phá và trải nghiệm những khác biệt từ Swarovski</span>
				</div>
				<div class="tile_cta">
					<span class="primary LargeButton StullerButton">
						<a href="/video"  class="customButton">
							Xem Thêm
						</a>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="container-fluid content_img">
	<div class="row flexbox">
		<div class="col-xs-12 col-md-4 flex-child ">
			<div class="tile">
				<div class="tile_img text-center"  style="position: relative;"><img class="image-responsive" src="img/gem_1.jpg" alt="Diamonds and Gemstones"></div>
				<div class="tile_body">
					<span class="tile_body_title">Genuine Gemstones and Created Stones</span>
					<span class="tile_body_subtitle">Swarovski Gemstones are genuine, cut, and polished with strict standards to present the iconic brilliance and fire that has made the brand a legacy for 122 years.</span>
				</div>
				<div class="tile_cta" >
					<span class="primary LargeButton StullerButton">
						<a href="/ourProducts" class="customButton">
							{{trans('products.read_more')}}
						</a>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4 flex-child ">
			<div class="tile">
				<div class="tile_img text-center"><img class="image-responsive" src="img/Teaser_Dancing_Stone2.jpg" alt="Diamonds and Gemstones"></div>
				<div class="tile_body">
					<span class="tile_body_title">DANCING STONES</span>
					<span class="tile_body_subtitle">Movement is life, dancing is movement, and sparkle is the effect we all desire from our jewelry. The Dancing Stone is a pre-set element now available from Swarovski that can capture the magic of all three. </span>
				</div>
				<div class="tile_cta">
					<span class="primary LargeButton StullerButton">
						<a href="/dancings_stones"  class="customButton">
							{{trans('products.read_more')}}
						</a>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4 flex-child">
			<div class="tile">
				<div class="tile_img text-center">	<a href="/video">
					<img class="image-responsive" src="../img/0.jpg">
					<img src="../img/youtube_button.png" class="btn_youtube" alt="">
					</a></div>
				<div class="tile_body">
					<span class="tile_body_title">Videos</span>
					<span class="tile_body_subtitle">Watch the videos, explore and experience the Swarovski difference</span>
				</div>
				<div class="tile_cta">
					<span class="primary LargeButton StullerButton">
						<a href="/video"  class="customButton">
							{{trans('products.read_more')}}
						</a>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<div class="container wrap-slick">
	<div style="width: 100%; height: 30px; padding-top: 12px; border-bottom: 1px solid #c4c4c4; text-align: center">
		<span class="Archer" style="font-weight:bold;color: #133A6D; font-size: 18pt; background-color: #fff; padding: 0px 15px 0px 15px; ">
			@if($langNetis == 'vi')
			Thông Tin Mới
			@else
			Related Articles
			@endif
		</span>
	</div>

	<div class="owl-carousel flexbox owl-theme" id="blogCarousel" >  

		<div class="owl-wrapper-outer ">
			<div class="owl-wrapper" style="left: 0px; display: block;">
				<div class="owl-item" >
					<div class="blogArticleBox blogCarouselArticle flex-child" >
						<img src="img/news/news1.jpg" alt="" class="img-fluid img-scroll">
						<hr class="blogTitleDivider">
						<p class="blogBlurb">
							@if($langNetis == 'vi')
							Cuộc thi thiết kế để truyền cảm hứng cho thị trường trang sức Nga
							@else
							Design Competition to Inspire the Russian Jewelry Market
							@endif
						</p>
						<p class="blogReadMore topMarginLarge">
							<a href="/desgin_competition">
							<span><img src="img/icon_readmore.jpg" class="img-responsive" alt="Stuller's Blog" style="margin-right:5px; display:inline;"></span>{{trans('products.read_more')}}
							</a>
						</p>
						<a href="/desgin_competition" class="link-page"></a>
						<!--<p class="blogDate">February 5, 2018</p>-->
					</div>
				</div>
			</div>
		</div>

		<!-- aaaa -->
		<div class="owl-wrapper-outer ">
			<div class="owl-wrapper" style="left: 0px; display: block;">
				<div class="owl-item" >
					<div class="blogArticleBox blogCarouselArticle flex-child" >
						<img src="img/news/Teaser_Dancing_Stone2.jpg" alt="" class="img-fluid img-scroll">
						<hr class="blogTitleDivider">
						<p class="blogBlurb">
							@if($langNetis == 'vi')
							Phong cách mới mang nét trẻ trung tươi vui cho thiết kế
							@else
							Innovative preset element adds playfulness to designs
							@endif
						</p>
						<p class="blogReadMore topMarginLarge">
							<a href="/dancings_stones">
							<span><img src="img/icon_readmore.jpg" class="img-responsive" alt="Stuller's Blog" style="margin-right:5px; display:inline;"></span>{{trans('products.read_more')}}
							</a>
						</p>
						<a href="/dancings_stones" class="link-page"></a>
						<!--<p class="blogDate">February 5, 2018</p>-->
					</div>
				</div>
			</div>
		</div>
		<!-- aaaa -->
		<div class="owl-wrapper-outer ">
			<div class="owl-wrapper" style="left: 0px; display: block;">
				<div class="owl-item" >
					<div class="blogArticleBox blogCarouselArticle flex-child" >
						<img src="img/news/Teaser_GemVisions2019_CutsColors_GreenLight_S27.jpg" alt="" class="img-fluid img-scroll">
						<hr class="blogTitleDivider">
						<p class="blogBlurb">
							@if($langNetis == 'vi')
							Xu hướng hình ảnh đá quý cho mùa xuân / hè 2019
							@else
							A Gem Visions Trend for Spring / Summer 2019
							@endif

						</p>
						<p class="blogReadMore topMarginLarge">
							<a href="/yes_to_green_light">
							<span><img src="img/icon_readmore.jpg" class="img-responsive" alt="Stuller's Blog" style="margin-right:5px; display:inline;"></span>{{trans('products.read_more')}}
							</a>
						</p>
						<a href="/yes_to_green_light" class="link-page"></a>
						<!--<p class="blogDate">February 5, 2018</p>-->
					</div>
				</div>
			</div>
		</div>
		<!-- aaaa -->
		<div class="owl-wrapper-outer ">
			<div class="owl-wrapper" style="left: 0px; display: block;">
				<div class="owl-item" >
					<div class="blogArticleBox blogCarouselArticle flex-child" >
						<div class="wrap-img">
						<img src="https://img.youtube.com/vi/YQNw2nClyhY/0.jpg" alt="" class="img-fluid img-scroll">
						<img class="btn_youtube" src="../img/youtube_button.png" alt="" >
						</div>
						<hr class="blogTitleDivider">
						<p class="blogBlurb">
							Zirconia from Swarovski – As Brilliant as a Diamond
						</p>
						<p class="blogReadMore topMarginLarge">
							<a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/YQNw2nClyhY?enablejsapi=1">
							<span><img src="img/icon_readmore.jpg" class="img-responsive" alt="Stuller's Blog" style="margin-right:5px; display:inline;"></span>{{trans('products.read_more')}}
							</a>
						</p>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/YQNw2nClyhY?enablejsapi=1" class="btn_vid link-page"></a>
						<!--<p class="blogDate">February 5, 2018</p>-->
					</div>
				</div>
			</div>
		</div>
		<!-- aaaa -->
		<div class="owl-wrapper-outer ">
			<div class="owl-wrapper" style="left: 0px; display: block;">
				<div class="owl-item" >
					<div class="blogArticleBox blogCarouselArticle flex-child" >
						<div class="wrap-img">
						<img src="https://img.youtube.com/vi/6pYAAf1oS_Y/0.jpg" alt="" class="img-fluid img-scroll">
						<img class="btn_youtube" src="../img/youtube_button.png" alt="" >
						</div>
						<hr class="blogTitleDivider">
						<p class="blogBlurb">
							The Bewitching Pure Brilliance of Swarovski Zirconia
						</p>
						<p class="blogReadMore topMarginLarge">
							<a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/6pYAAf1oS_Y?enablejsapi=1">
							<span><img src="img/icon_readmore.jpg" class="img-responsive" alt="Stuller's Blog" style="margin-right:5px; display:inline;"></span>{{trans('products.read_more')}}
							</a>
						</p>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/6pYAAf1oS_Y?enablejsapi=1" class="btn_vid link-page"></a>
						<!--<p class="blogDate">February 5, 2018</p>-->
					</div>
				</div>
			</div>
		</div>
		<!-- aaaa -->
		<div class="owl-wrapper-outer ">
			<div class="owl-wrapper" style="left: 0px; display: block;">
				<div class="owl-item" >
					<div class="blogArticleBox blogCarouselArticle flex-child" >
						<div class="wrap-img">
						<img src="https://img.youtube.com/vi/Nf3XoGagQTs/0.jpg" alt="" class="img-fluid img-scroll">
						<img class="btn_youtube" src="../img/youtube_button.png" alt="" >
						</div>
						<hr class="blogTitleDivider">
						<p class="blogBlurb">
							Swarovski Genuine Gemstones and Created Stones
						</p>
						<p class="blogReadMore topMarginLarge">
							<a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/Nf3XoGagQTs?enablejsapi=1">
							<span><img src="img/icon_readmore.jpg" class="img-responsive" alt="Stuller's Blog" style="margin-right:5px; display:inline;"></span>{{trans('products.read_more')}}
							</a>
						</p>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/Nf3XoGagQTs?enablejsapi=1" class="btn_vid link-page"></a>
						<!--<p class="blogDate">February 5, 2018</p>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end slide -->

	<hr style="border:none; border-top:1px solid #c4c4c4; color:#fff; background-color:#fff; height:1px; width:100%;">   

	<div class="container pre_content">
		<div class="col-xs-12 miningDisclaimer_container">
			<div class="row">
				<div class="col-xs-12 col-md-4 Archer miningDisclaimer_title">
					Colored Gemstones Responsible Mining Standard
				</div>
				<div class="col-xs-12 col-md-8 Archer miningDisclaimer">
					As part of multi-stakeholder collaboration, the Swarovski Gemstones<sup>™</sup> Business has been actively engaged in efforts to establish the first colored gemstones responsible mining standard in the industry. We are pleased to see that, in 2016, the RJC announced that it will expand its certification scope to include colored gemstones.
				</div>
			</div>

		</div>
	</div> 
	<!--   end pre -->
<div class="modal fade" id="exampleModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close btn_video" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <iframe id="player" width="100%" height="315" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary close-modal btn_video" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>


</div>
@endsection
@section('scripts')
  <script>
        $(document).ready(function(){
        	 // $(".blogBlurb").each(function(){
        	 // 	if ($(this).text().length > 50) {
        	 // 		$(this).text($(this).text().substr(0, 50));
        	 // 		$(this).append('...');
        	 // 	}
        	 // });
            $('#blogCarousel').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
				dots: true,
				// variableWidth: true,
				// adaptiveHeight:true,
				// autoplay:true,
        		// mobileFirst:true,
        		
				responsive: [
				{
				breakpoint: 1025,
				settings: {
				slidesToShow:2,
				slidesToScroll: 2,
				// centerMode: true,
				// variableWidth: true,
				// variableWidth: true,
				// adaptiveHeight:true,
				}
				},
				{
				breakpoint: 480,
					settings: {
					slidesToShow: 1,
					slidesToScroll: 1
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
				]
            }).on('setPosition', function (event, slick) {
    slick.$slides.css('height', slick.$slideTrack.height() + 'px');
});
            $('.modal-body #player').attr('src', '');
        })

        $('.btn_vid').click(function(event) {
        	var Urlvideo = $(this).data('video');
        	$('.modal-body #player').attr({
        		src: Urlvideo,
        	});
        	var a = $(this).parent().parent().find('.blogBlurb').text();
        	$('#exampleModalLabel').text(a);
        });

        $('.btn_video').click(function() {
        	$('#player').each(function(){ 
        		var frame = document.getElementById("player");
        		frame.contentWindow.postMessage(
        			'{"event":"command","func":"pauseVideo","args":""}',
        			'*'); 
        	});
        	$('.modal-body #player').attr('src', '');
        });
   </script>
@endsection