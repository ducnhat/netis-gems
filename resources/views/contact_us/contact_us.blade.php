@extends('layouts.master')
@section('styles')      
	  <title>{{trans('products.contact_us')}}</title>
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/contact_us.css">
@endsection
@section('content')
<div class='background'>
	<div class="col-md-12" >	
		<div class="row" style="padding-top: 35px;">
			<div class="col-md-12">
				<div class="row" >
					<h2 style="margin:0 auto;padding-bottom: 20px;text-transform: uppercase;">{{trans('products.contact_us')}}</h2>
				</div>
			</div>		
			<div class="col-md-6">
				<div class='outer'>
				</div>
				<div class='main'>
					<div class='pd'>
						<p class='st'>{{trans('products.name')}}</p>
						<a class="fullname" href="">Wendy Huynh (Uyên)</a>
						<p class='st'>{{trans('products.phone')}}</p>
						<a href='tel:84908245522'>+84 908 245522</a>
						<p class='st'>Email</p>
						<a href='mailto:wendy.huynh@netisgems.com'>wendy.huynh@netisgems.com</a>
						<p class='st'>{{trans('products.business_development_manager')}}</p>	
						<p class='st'>{{trans('products.address')}}</p>
						<a href='#'>6th fl, Khai Hoan Building. 83B Hoang Sa St, Dakao Ward, Dist 1, HCMC</a></div>		
					</div>
				</div>
				<div class="col-md-6">
					<div class='outer'>
					</div>
					<div class='main'>
						<div class='pd'>
							<p class='st'>{{trans('products.name')}}</p>
							<a class="fullname" href="">Andy Phan ( Nghi) </a>
							<p class='st'>{{trans('products.phone')}}</p>
							<a href='tel:84907392923'>+84 907 392923</a>
							<p class='st'>Email</p>
							<a href='mailto::andy.phan@netisgems.com'>andy.phan@netisgems.com</a>
							<p class='st'>{{trans('products.account_manager')}}</p>
							<p class='st'>{{trans('products.address')}}</p>
							<a href='#'>6th fl, Khai Hoan Building. 83B Hoang Sa St, Dakao Ward, Dist 1, HCMC</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection