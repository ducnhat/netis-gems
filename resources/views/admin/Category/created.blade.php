@extends('admin.layouts.master')
@section('styles')
@endsection
@section('content')
<div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tables</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Thêm Tin Đăng</div>
        <div class="card-body">
              @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
    <form action="{{route('admin.category.store')}}" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token()}}">
        @if($errors->has('city'))
      <div class="alert alert-danger" role="alert">
         {{$errors->first('city')}}
      </div>
      @endif
      <div class="form-group">
      	<label for="exampleInputEmail1">Name En</label>
      	<input type="text" name="name_en" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" value="">
      </div>
      <div class="form-group">
      	<label for="exampleInputEmail1">Name Vi</label>
      	<input type="text" name="name_vi" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" value="">
      </div>
        <div class="form-group">
      	<fieldset class="form-group">
      		<legend>Status</legend>
      		<div class="form-check">
      			<label class="form-check-label">
      				<input type="radio" class="form-check-input" name="status" id="optionsRadios1" value="1">
      				Hidden
      			</label>
      		</div>
      		<div class="form-check">
      			<label class="form-check-label">
      				<input type="radio" class="form-check-input" name="status" id="optionsRadios2" checked value="2">
      				Show
      			</label>
      		</div>
      	</fieldset>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection