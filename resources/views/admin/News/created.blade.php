@extends('admin.layouts.master')
@section('styles')
  <script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
@endsection
@section('content')
   <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tables</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Thêm Tin Đăng</div>
        <div class="card-body">
              @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
    <form action="{{route('admin.news.store')}}" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token()}}">
        @if($errors->has('city'))
      <div class="alert alert-danger" role="alert">
         {{$errors->first('city')}}
      </div>
      @endif
      <div class="form-group">
      	<label for="exampleInputEmail1">Title EN</label>
      	<input type="text" name="title_en" class="form-control" id="" aria-describedby="emailHelp" placeholder="Title En" value="">
        <label for="exampleInputEmail1">Title VI</label>
        <input type="text" name="title_vi" class="form-control" id="" aria-describedby="emailHelp" placeholder="Title Vi" value="">
      </div>
      <div class="form-group">
      	<label for="exampleInputEmail1">Category</label>
        <?php $categories = DB::table('categories')->get();?>
     
      	<select class="form-control" name="select_category">
             @foreach( $categories as $value)

      		<option value="{{$value->id}}" >
      		{{$value->name_en}}
      		</option>
          @endforeach
      <!-- 		<option value="2" >
      			Kim Cương
      		</option> -->
      	</select>
      </div>
      <div class="form-group">
      	<label for="exampleInputEmail1">Recommend En</label>
      	<input type="text" name="recommend_en" class="form-control" id="" aria-describedby="emailHelp" placeholder="Recommend En" value="">
        <label for="exampleInputEmail1">Recommend Vi</label>
        <input type="text" name="recommend_vi" class="form-control" id="" aria-describedby="emailHelp" placeholder="Recommend Vi" value="">
      </div>
        <div class="col-md-12">
              <div class="row"> 
                    <div class="col col-md-4">
                      <label for="file-input" class=" form-control-label">File input</label>
                      <input type="file" id="file-input" name="image_upload filesTest" class="form-control-file upload-image" onchange="UploadImage(this);">
                    </div>
                    <div class="col col-md-6">
                    <label>Hình Ảnh Đại diện</label>                     
                     <div class="media-left"> <a href="agent.html">
                      <img class="media-object rounded-circle" name="image" id="image_show" src="" width="64" height="64" alt=""> 
                      <input type="hidden" name="image" id="images_show_data" value=""></a>
                    </div>
                    </div>
                  
              </div>
        </div>
      <div class="form-group">
      	<label for="inputAddress2">Content En</label>
      	<textarea name="content_en" id="textarea-input" rows="3" placeholder="content en" class="form-control ckeditor" value=""></textarea>
<!--       	<script>
      		CKEDITOR.replace( 'content_en' );
      	</script> -->
      </div>
      <div class="form-group">
        <label for="inputAddress3">Content Vi</label>
        <textarea name="content_vi" id="textarea-input" rows="3" placeholder="content vi" class="form-control ckeditor" value=""></textarea>
     <!--    <script>
          CKEDITOR.replace( 'content_vi' );
        </script> -->
      </div>
      <div class="form-group">
      	<label for="exampleInputEmail1">Video Link</label>
      	<input type="text" class="form-control video_youtube" id="video_youtube" aria-describedby="emailHelp" placeholder="Video URL" value="">
      	<input type="hidden" name="video_link" class="myVideo1" value="">
      	<div class="myVideo"></div>
      </div>
      <div class="form-group">
      	<fieldset class="form-group">
      		<legend>Status</legend>
      		<div class="form-check">
      			<label class="form-check-label">
      				<input type="radio" class="form-check-input" name="status" id="optionsRadios1" value="1">
      				Hidden
      			</label>
      		</div>
      		<div class="form-check">
      			<label class="form-check-label">
      				<input type="radio" class="form-check-input" name="status" id="optionsRadios2" checked value="2">
      				Show
      			</label>
      		</div>
      	</fieldset>
      </div>
    <button type="submit" class="btn btn-primary" onclick="changeURL()">Submit</button>
    </form>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
@endsection
@section('scripts')
<script>
	function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}

var myId;
 function  changeURL(){
    var myUrl = $('#video_youtube').val();
    console.log(myUrl);
    myId = getId(myUrl);
    console.log(myId);
    var linkURL = "//www.youtube.com/embed/" + myId +"?enablejsapi=1";
    $('.myVideo1').val(linkURL);
 }
// $('#video_youtube').change(function () {
//     var myUrl = $('#video_youtube').val();
//     console.log(myUrl);
//     myId = getId(myUrl);
//     console.log(myId);
//     var linkURL = "//www.youtube.com/embed/" + myId +"?enablejsapi=1";
//     $('.myVideo1').val(linkURL)
//      $('.myVideo').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen></iframe>');
//     });

function UploadImage(e){
  
    var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $.ajax({
        url: "{{route('admin.news.uploadimage',['type'=>'Images'])}}", // Url to which the request is send
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function(res)   // A function to be called if request succeeds
        {
          dataSuccess = JSON.parse(res);
          // console.log(res);
          console.log(dataSuccess.status);
          if(dataSuccess.status == false){
            alert(dataSuccess.errors.file);
          }else{
          $('#image_show').attr('src','');
          $('#images_show_data').val('');
          console.log(dataSuccess.filename);
          $('#image_show').attr('src', '/'+dataSuccess.filename);
          $('#images_show_data').val('/'+dataSuccess.filename);
          }
         
        },error: function(error){
            alert(error);
            }
        });
        };
</script>
@endsection