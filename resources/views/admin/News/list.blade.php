@extends('admin.layouts.master')

@section('content')
 <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html"></a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> 
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <h2>List News/Video</h2>
                        <a class="btn btn-primary" href="{{route('admin.news.created')}}" role="button">Thêm Mới</a>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Recommend</th>
                                        <th>Content</th>
                                        <th>Video Link</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                        <th>Tools</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($news as $value)
                                    <tr>
                                        <td>{{$value->title_en}}</td>
                                        <td>{{$value->recommend_en}}</td>
                                        <td>{{$value->content_en}}</td>
                                        <td>{{$value->video_link}}</td>
                                        <td>@if($value->status == '2' )
                                        Show
                                        @else
                                        Hidden
                                        @endif
                                        </td>
                                        <td><a class="btn btn-danger" href="{{route('admin.news.destroy',['id'=>$value->id])}}" role="button">Delete</a></td>
                                        <td><a class="btn btn-info" href="{{route('admin.news.edit',['id'=>$value->id])}}" role="button">Updated</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>

@endsection

@section('scripts')
  <script src="/admin/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/admin/js/bootstrap.min.js"></script>
@endsection