@extends('admin.layouts.master')
@section('styles')
  <script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
@endsection
@section('content')
   <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tables</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Thêm Tin Đăng</div>
        <div class="card-body">
    <form action="{{route('admin.news.update',['id'=>$news->id])}}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token()}}">
      <div class="form-group">
        <label for="exampleInputEmail1">Title EN</label>
        <input type="text" name="title_en" class="form-control" id="" aria-describedby="emailHelp" placeholder="Title En" value="{{$news->title_en}}">
        <label for="exampleInputEmail1">Title VI</label>
        <input type="text" name="title_vi" class="form-control" id="" aria-describedby="emailHelp" placeholder="Title Vi" value="{{$news->title_vi}}">
      </div>
      <div class="form-group">
      	<label for="exampleInputEmail1">Category</label>
          <?php $categories = DB::table('categories')->get();?>
      	<select class="form-control" name="select_category">
           @foreach( $categories as $value)
      		<option value="{{$value->id}}" {{isset($news) ? ($value->id == $news->category_id ? 'selected' : '') : '' }} >
      		{{$value->name_en}}
      		</option>
          @endforeach
      	</select>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Recommend En</label>
        <input type="text" name="recommend_en" class="form-control" id="" aria-describedby="emailHelp" placeholder="Recommend En" value="{{$news->recommend_en}}">
        <label for="exampleInputEmail1">Recommend Vi</label>
        <input type="text" name="recommend_vi" class="form-control" id="" aria-describedby="emailHelp" placeholder="Recommend Vi" value="{{$news->recommend_vi}}">
      </div>
      <div class="form-group">
        <label for="inputAddress2">Content En</label>
        <textarea name="content_en" id="textarea-input" rows="3" placeholder="content en" class="form-control ckeditor" value=""> {{$news->content_en}}</textarea>
<!--        <script>
          CKEDITOR.replace( 'content_en' );
        </script> -->
      </div>
      <div class="form-group">
        <label for="inputAddress3">Content Vi</label>
        <textarea name="content_vi" id="textarea-input" rows="3" placeholder="content vi" class="form-control ckeditor" value=""> {{$news->content_vi}}</textarea>
     <!--    <script>
          CKEDITOR.replace( 'content_vi' );
        </script> -->
      </div>
      <div class="form-group">
      	<label for="exampleInputEmail1">Video</label>
      	<input type="text" class="form-control video_youtube" id="video_youtube" aria-describedby="emailHelp" placeholder="Video URL" value="{{$news->video_link}}">
      	<input type="hidden" name="video_link" class="myVideo1" value="{{$news->video_link}}">
      	<div class="myVideo"></div>
      </div>
      <div class="form-group">
      	<fieldset class="form-group">
      		<legend>Trạng Thái</legend>
      		<div class="form-check">
      			<label class="form-check-label">
      				<input type="radio" class="form-check-input" name="status" id="optionsRadios1" value="1"  @if($news->status == 1 )
            checked
          @endif>
      				Hidden
      			</label>
      		</div>
      		<div class="form-check">
      			<label class="form-check-label">
      				<input type="radio" class="form-check-input" name="status" id="optionsRadios2"  value="2"   @if($news->status == 2 )
            checked
          @elseif(!isset($news))
             checked   
          @endif>
      				Show
      			</label>
      		</div>
      	</fieldset>
      </div>
    <button type="submit" class="btn btn-primary" onclick="changeURL()">Submit</button>
    </form>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {

    
  });
	function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}

 function  changeURL(){
    var myUrl = $('#video_youtube').val();
    console.log(myUrl);
    myId = getId(myUrl);
    console.log(myId);
    var linkURL = "//www.youtube.com/embed/" + myId +"?enablejsapi=1";
    $('.myVideo1').val(linkURL);
 }

   
    // $('#video_youtube').change(function () {
    // var myUrl = $('#video_youtube').val();
    // console.log(myUrl);
    // myId = getId(myUrl);
    // console.log(myId);
    // var linkURL = "//www.youtube.com/embed/" + myId +"?enablejsapi=1";
    // $('.myVideo1').val(linkURL)
    //  $('.myVideo').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen></iframe>');
    // });



</script>
@endsection