@extends('layouts.master')
@section('styles')
      <title>CSR</title>
      <link rel="stylesheet" href="css/company_business.css">
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
 	<style>
         h4.title{
            font-size: 1.3rem;
         }
   		@media (min-width: 768px){
			.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11 {
			float: left;
			}
			
   		}

   	</style>
      <?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
<div class="content-main" id="content-main">
   <!-- Page Content -->
   <div class="container container-body">
      <div class="row">
         <div class="col-xs-12 col-md-2 menu-left">
            <div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
               <div class="btn-group dropdown">
                  <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">
                     <li class="subnav" role="menuitem">
                        <h5 class="title ">
                           <a href="https://www.swarovskigroup.com/S/aboutus/Swarovski_Company.de.html
" class="">{{trans('products.about_swarovski')}}</a>
                        </h5>
                        <ul class="dropdown-menu">
                           <li role="menuitem" class="folder">
                              <a href="/history" class="">
                               {{trans('products.history')}}
                              </a>                        
                           </li>
                           <li role="menuitem" class="folder">                
                              <a href="/csr" class="active">                    CSR                
                              </a>                        
                           </li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <!--/UdmComment-->
         <div class="col-xs-12 col-md-10 no-padding-left no-padding-right">
            <div class="component-promo featured hero col-xs-12 col-md-12 no-margin">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="image-container">
                        <div class="image-overlay is-loaded" style="display: none;"></div>
                        <img alt="Swarovski Gemstones" class=" img-responsive lazyloaded" src="../img/our_company/business/csr_bg.jpg" >                     
                     </div>
                  </div>
                    @if($langNetis == 'vi')
                         <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto">
                     <div class="component-promo-text">
                        <p class="slide-meta hidden"></p>
                        <p>Đá quý và Đá CZ của Swarovski nổi tiếng về chất lượng đẳng cấp trên toàn thế giới. Đối với chúng tôi, chất lượng không chỉ ở từng mặt cắt, độ trong suốt hay màu sắc đá  mà còn là tính trách nhiệm và sự tin cậy. Ở mọi giai đoạn trong quá trình chế tác đá , chúng tôi cố gắng đảm bảo các tiêu chuẩn cao nhất . Là một doanh nghiệp do gia đình tự quản với lịch sử 120 năm , trách nhiệm luôn luôn là một phần của quan trọng nhất. Quan trọng hơn,chúng tôi cam kết xây dựng thành tích mang tính bền vững xã hội và bảo vệ môi trường trong suốt hoạt động. Bởi vì cuối cùng, khi nói đến đá quý - đá nhận tạo, chất lượng thực sự không đơn thuần chỉ là những gì bạn thấy.&nbsp;</p>
                     </div>
                  </div>
                    @else
                  <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto">
                     <div class="component-promo-text">
                        <p class="slide-meta hidden"></p>
                        <p>Swarovski Genuine Gemstones and Created Stones are known the world over for their unrivalled quality. But for us, quality is not just about cut, clarity or color. It is also about responsibility, integrity and trust. At every stage in the journey of our stones, we strive to ensure the highest possible standards. As a family-run business with a proud 120-year history, responsibility is something that has always been part of our DNA. Today, it is more important than ever and we are committed to building on our strong track record of social and environmental sustainability throughout our own operations and by working with suppliers and our peers in the industry. Because ultimately, when it comes to genuine gemstones or created stones, real quality goes beyond just what you see.&nbsp;</p>
                     </div>
                  </div>
                  @endif
               </div>
            </div>
<!--           <div class="component-headline">
			<hr>
			<div class="headline_title">
				<h2></h2>
			</div>
			<div class="headline_text">
				<h3>CORPORATE SOCIAL RESPONSIBILITY<br></h3>
			</div>
      	  </div>
      	  <div class="component-promo featured col-xs-12 col-sm-4">    	  	
      	  		<div class="col-xs-12">
      	  			<a href="javascript:void(0)" target="">
      	  				<div class="image-container">
      	  					<img alt="RJC" class=" img-responsive lazyloaded" src="../img/our_company/business/csr_1.jpg">                            
      	  				</div>
      	  			</a>
      	  		</div>
      	  		<div class="col-xs-12 text-center">
      	  			<div class="component-promo-text">                  
      	  				<p class="slide-meta ">                                                                                
                     </p>                    
      	  				<h4 class="title">Certified by the Responsible Jewellery Council<br><br></h4>
      	  				<div>
      	  					<p>Our recent certification by the Responsible Jewellery Council provides independent, third-party confirmation of our commitment to responsible business practices.</p>
      	  					<p>
      	  						<please></please>
      	  					</p>
      	  				</div>
      	  				<p class="related-link">                            
                        <a href="javascript:void(0)" target="">
                          read more                            
                       </a>                        
                    </p>
      	  			</div>
      	  		</div> 
      	  </div>
      	  <div class="component-promo featured col-xs-12 col-sm-4">      	  	
      	  		<div class="col-xs-12">
      	  			<a href="javascript:void(0)" target="">
      	  				<div class="image-container">
      	  					<img alt="zirconia" class=" img-responsive lazyloaded" src="../img/our_company/business/csr_2.jpg">                            
      	  				</div>
      	  			</a>
      	  		</div>
      	  		<div class="col-xs-12 text-center">
      	  			<div class="component-promo-text">                    
      	  				<p class="slide-meta ">                                                                                
                     </p>                      
      	  				<h4 class="title">Swarovski Zirconia - THE MOST RESPONSIBLE ZIRCONIA ON THE MARKET&nbsp;</h4>
      	  				<div>
      	  					<p>Strong ownership and control over the supply and production of our Swarovski Zirconia make it the number one product for environmentally and socially conscious customers and consumers.</p>
      	  					<p>
      	  						<please></please>
      	  					</p>
      	  				</div>
      	  				<p class="related-link">                            
                        <a href="javascript:void(0)" target="">  
                         explore more                            
                        </a>                        
                     </p>
      	  			</div>
      	  		</div>      	
      	  </div>
      	  <div class="component-promo featured col-xs-12 col-sm-4">      	  	
      	  		<div class="col-xs-12">
      	  			<a href="javascript:void(0)" target="">
      	  				<div class="image-container">
      	  					<img alt="gemstones" class=" img-responsive lazyloaded" src="../img/our_company/business/csr_3.jpg">                            
      	  				</div>
      	  			</a>
      	  		</div>
      	  		<div class="col-xs-12 text-center">
      	  			<div class="component-promo-text">                        
      	  				<p class="slide-meta ">                                                                                
                     </p>                        
      	  				<h4 class="title">Swarovski Genuine Gemstones – a commitment to responsibility<br></h4>
      	  				<div>
      	  					<p>Fragmented supply chains in the genuine gemstones market mean full traceability is challenging to achieve. But that is our goal and we have plans and ambitions in place to achieve it.</p>
      	  					<p>
      	  						<please></please>
      	  					</p>
      	  				</div>
      	  				<p class="related-link">
                        <a href="javascript:void(0)" target="">                                find out more                            
                        </a>                        
                     </p>
      	  			</div>
      	  		</div>      	
      	  </div> -->
       <div class="component-headline">
            <hr>
            <div class="headline_title">
               <h2>{{trans('products.discover_more')}}</h2>
            </div>
            <div class="headline_text"></div>
         </div>
         <div class="component-focus-content col-xs-12 col-sm-12 col-md-12" id="component-focus-content-related-content-5442010">
            <div class="row">
            <div class="item col-xs-12 col-sm-4 col-md-4   text-center" style="display:inline-block;">
               <a href="/ourProducts" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/Teaser_sRGB_SGE_Zirconia_Innovations_2018_Final.jpg" ></div>
                  <h5 class="title"> {{trans('products.products_home')}}</h5>
               </a>
               <p class="focus-link hide">Swarovski offers exquisite Genuine Gemstones and Created Stones made from the purest raw materials and crafted to the highest standards for quality, color, size and shape.</p>
               <p class="related-link"><a href="/ourProducts" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/our-company" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/teaser_brands.jpg"></div>
                  <h5 class="title">{{trans('products.our_company')}}</h5>
               </a>
               <p class="focus-link hide">The Swarovski Group operates a global branding program which enables Swarovski branding partners to use one of the company’s ingredient brands Zirconia from Swarovski® and Gemstones from Swarovski® as an independent quality endorsement on the partner’s products.</p>
               <p class="related-link"><a href="/our-company" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/contact-us" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/Findus.jpg" ></div>
                  <h5 class="title">{{trans('products.contact_us')}}</h5>
               </a>
               <p class="focus-link hide">Sales Offices, where to buy</p>
               <p class="related-link"><a href="/contact-us" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            </div>
         </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection