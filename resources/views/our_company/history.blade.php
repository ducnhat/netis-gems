@extends('layouts.master')
@section('styles')
      <title>{{trans('products.history')}}</title>
      <link rel="stylesheet" href="css/company_business.css">
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
<div class="content-main" id="content-main">
   <!-- Page Content -->
   <div class="container container-body">
      <div class="row">
         <div class="col-xs-12 col-md-2 menu-left">
            <div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
               <div class="btn-group dropdown">
                  <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">
                     <li class="subnav" role="menuitem">
                        <h5 class="title ">
                           <a href="https://www.swarovskigroup.com/S/aboutus/Swarovski_Company.de.html
                           " class="">{{trans('products.about_swarovski')}}</a>
                        </h5>
                        <ul class="dropdown-menu">
                           <li role="menuitem" class="folder">                <a href="/history" class="active">{{trans('products.history')}}</a></li>
                           <li role="menuitem" class="folder">                <a href="/csr" class="">CSR</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <!--/UdmComment-->
         <div class="col-xs-12 col-md-10 no-padding-left no-padding-right">
            <div class="component-promo featured hero col-xs-12 col-md-12 no-margin">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="image-container">
                        <div class="image-overlay is-loaded" style="display: none;"></div>
                        <img alt="Swarovski Gemstones" class=" img-responsive lazyloaded" src="../img/our_company/business/history_bg.jpg" >                     
                     </div>
                  </div>
                   @if($langNetis == 'vi')
                     <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto">
                     <div class="component-promo-text">
                        <p class="slide-meta hidden"></p>
                        <p>Swarovski là một thương hiệu danh tiếng toàn cầu trong công nghệ cắt chính xác và đã trải qua 120 năm lịch sử. Thuộc tính đặc biệt của Swarovski bắt nguồn từ di sản văn hóa của châu Âu, sự tinh tế đến từ kết hợp giữa nghệ thuật, khoa học và bí quyết kinh doanh.&nbsp;<br><br>Năm 1965, viên đá quý Swarovski chính hãng đầu tiên được cắt, đánh dấu sự ra đời ngành kinh doanh đá quý Swarovski. Ngày nay, 50 năm sau, Swarovski tiếp tục hướng đến sự rạng rỡ của các dòng đá quý và đá nhân tạo theo các chiều hướng phát triển mới . Thương hiệu và sản phẩm của tập đoàn phổ biến trên toàn thế giới và có mặt  trong các lĩnh vực trang sức, kính mắt, đồng hồ và các thiết bị điện tử….</p>
                     </div>
                  </div>
                   @else
                  <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto">
                     <div class="component-promo-text">
                        <p class="slide-meta hidden"></p>
                        <p>Swarovski can reflect upon an international reputation in precision cutting technology and the virtuous handling of light that spans a 120 years. Our company‘s distinctive character is rooted in the cultural heritage of Europe, as is our flair for forging links between the arts, science, and business.&nbsp;<br><br>In 1965, the very first Swarovski Genuine Gemstone was cut, marking the birth of the Swarovski Gemstones Business. Today, 50 years on, we continue to capture the radiance of genuine gemstones and created stones, whilst infusing them with a new dimension of brilliance. Our brand and products are celebrated the world over and can be found in the fields of fine jewellery, eye wear, watches, and electronic devices</p>
                     </div>
                  </div>
                  @endif
               </div>
            </div>
            <hr>
            <div class="component-promo featured col-xs-12 text-center">
               <h2 class="title alt"></h2>
               <div class="col-md-8 col-md-offset-2 text-center"></div>
            </div>
        <!--     <div class="component-promo featured col-xs-12 col-sm-12">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="image-container">
                        <img alt="Pioneering Spirit" class=" img-responsive lazyloaded" src="../img/our_company/business/SG_BANNER_VICENZA_MILESTONES_2260x5260_lay-1_3.jpg">                           
                     </div>
                  </div>
               </div>
            </div> -->
         <div class="component-headline">
            <hr>
            <div class="headline_title">
               <h2>{{trans('products.discover_more')}}</h2>
            </div>
            <div class="headline_text"></div>
         </div>
         <div class="component-focus-content col-xs-12 col-sm-12 col-md-12" id="component-focus-content-related-content-5442010">
            <div class="row">
            <div class="item col-xs-12 col-sm-4 col-md-4   text-center" style="display:inline-block;">
               <a href="/ourProducts" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/Teaser_sRGB_SGE_Zirconia_Innovations_2018_Final.jpg" ></div>
                  <h5 class="title"> {{trans('products.products_home')}}</h5>
               </a>
               <p class="focus-link hide">Swarovski offers exquisite Genuine Gemstones and Created Stones made from the purest raw materials and crafted to the highest standards for quality, color, size and shape.</p>
               <p class="related-link"><a href="/ourProducts" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/our-company" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/teaser_brands.jpg"></div>
                  <h5 class="title">{{trans('products.our_company')}}</h5>
               </a>
               <p class="focus-link hide">The Swarovski Group operates a global branding program which enables Swarovski branding partners to use one of the company’s ingredient brands Zirconia from Swarovski® and Gemstones from Swarovski® as an independent quality endorsement on the partner’s products.</p>
               <p class="related-link"><a href="/our-company" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/contact-us" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/Findus.jpg" ></div>
                  <h5 class="title">{{trans('products.contact_us')}}</h5>
               </a>
               <p class="focus-link hide">Sales Offices, where to buy</p>
               <p class="related-link"><a href="/contact-us" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            </div>
         </div>
            <!--UdmComment-->

            <!--/UdmComment-->
         </div>
         <!--UdmComment-->
      </div>
   </div>
</div>
</div>
@endsection