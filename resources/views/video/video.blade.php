@extends('layouts.master')
@section('styles')
<title>Video</title>      
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../css/video.css">
<script type="text/javascript" src="http://www.youtube.com/player_api"></script>
@endsection
@section('content')
<div class="container container-body">
      <p class="text-center" style="margin-bottom: 20px;">
           {{trans('products.please_find_video')}}
        </p>
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="results">
            <div class="component-focus-content focus-content-news col-xs-12 col-sm-12 col-md-12">
               <div class="row">
                  <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                     <a href="javascript:void(0)">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                     <a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="//www.youtube.com/embed/_-6lU50Nack?enablejsapi=1" >
                     <img src="https://img.youtube.com/vi/_-6lU50Nack/0.jpg" alt="">
                     <img class="btn_youtube" src="../img/youtube_button.png" alt="" >                     
                     </a>
                     </div>
                     </a>
                     <h5 class="title">
                        <a href="javascript:void(0)">Swissbacks Official Video</a>
                     </h5>
                     <p class="related-link">
                     </p>
                  </div>
                  <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                     <a href="javascript:void(0)">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                     <a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/6pYAAf1oS_Y?enablejsapi=1" >
                     <img src="https://img.youtube.com/vi/6pYAAf1oS_Y/0.jpg" alt="">
                     <img class="btn_youtube" src="../img/youtube_button.png" alt="" >
                     </a> 
                     </div>
                     </a>
                     <h5 class="title">
                        <a href="javascript:void(0)">The Bewitching Pure Brilliance of Swarovski Zirconia</a>
                     </h5>
                     <p class="related-link">
                     </p>
                  </div>
                  <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                     <a href="javascript:void(0)">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                     <a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/Nf3XoGagQTs?enablejsapi=1" >
                     <img src="https://img.youtube.com/vi/Nf3XoGagQTs/0.jpg" alt="">
                     <img class="btn_youtube" src="../img/youtube_button.png" alt="" >
                     </a> 
                                          
                     </div>
                     </a>
                     <h5 class="title">
                        <a href="javascript:void(0)">Swarovski Genuine Gemstones and Created Stones</a>
                     </h5>
                     <p class="related-link">
                     </p>
                  </div>
               <!-- </div> -->
               <!-- page 2 -->
              <!--  <div class="row"> -->
                  <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                     <a href="/desgin_competition">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                     <a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/JoLg4jCRJ2c?enablejsapi=1" >
                     <img src="https://img.youtube.com/vi/JoLg4jCRJ2c/0.jpg" alt="">
                      <img class="btn_youtube" src="../img/youtube_button.png" alt="" >
                     </a>
                                         
                     </div>
                     </a>
                     <h5 class="title">
                        <a href="javascript:void(0)">Swarovski History</a>
                     </h5>
                     <p class="related-link">
                     </p>
                  </div>
                  <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                     <a href="javascript:void(0)">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                     <a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/YQNw2nClyhY?enablejsapi=1" >
                     <img src="https://img.youtube.com/vi/YQNw2nClyhY/0.jpg" alt="">
                     <img class="btn_youtube" src="../img/youtube_button.png" alt="" >
                     </a> 
                                          
                     </div>
                     </a>
                     <h5 class="title">
                        <a href="javascript:void(0)">Zirconia from Swarovski – As Brilliant as a Diamond</a>
                     </h5>
                     <p class="related-link">
                     </p>
                  </div>
                  <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                     <a href="javascript:void(0)">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                     <a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="https://www.youtube.com/embed/kdTNpjRK-os?enablejsapi=1" >
                     <img src="https://img.youtube.com/vi/kdTNpjRK-os/0.jpg" alt="">
                     <img class="btn_youtube" src="../img/youtube_button.png" alt="" >                     
                     </a> 
                     </div>
                     </a>
                     <h5 class="title">
                        <a href="javascript:void(0)">Swarovski Gemsvision 2019</a>
                     </h5>
                     <p class="related-link">
                     </p>
                  </div>
                  @if(isset($video))
                  @foreach($video as $val)   
                  <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                     <a href="javascript:void(0)">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                     <a href="javascript:void(0)" class="btn_vid" data-toggle="modal" data-target="#exampleModal" data-video="{{$val->video_link}}" >
                     <img class="img-Video" src="" alt="">
                     <img class="btn_youtube" src="../img/youtube_button.png" alt="" >                     
                     </a> 
                     </div>
                     </a>
                     <h5 class="title">
                        <a href="javascript:void(0)">{{$val->title_en}}</a>
                     </h5>
                     <p class="related-link">
                     </p>
                  </div>
                  @endforeach
                  @endif
               </div>
               <!-- end page 2 -->
            </div>
         </div>
       <!--   <div class="component-results-footer row">
            <div class="col-md-12 show-more-results">
               <a href="javascript:void(0)" class="btn btn-default show-more-results" type="button">Show more News
               </a>
            </div>
         </div> -->
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static"  data-keyboard="false">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close btn_video" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <iframe id="player" width="100%" height="315" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary close-modal btn_video" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
   function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}
   
   $(document).ready(function() {

      $('.btn_vid').filter(function(){
         var myId;
         var myUrl = $(this).data('video');
         // console.log(myUrl);
         myId = getId(myUrl);
         // console.log(myId);
         var linkURL = "https://img.youtube.com/vi/" + myId +"/0.jpg";
         $(this).children('.img-Video').attr('src', linkURL);
      });

      $('.modal-body #player').attr('src', '');
      });

      $('.btn_vid').click(function(event) {
         var Urlvideo = $(this).data('video');
         $('.modal-body #player').attr({
            src: Urlvideo,
         });

         var a = $(this).parent().parent().find('h5.title a').text();
         $('#exampleModalLabel').text(a);

      });

      $('.btn_video').click(function() {
        $('#player').each(function(){ 
          var frame = document.getElementById("player");
          frame.contentWindow.postMessage(
            '{"event":"command","func":"pauseVideo","args":""}',
            '*'); 
        });
        $('.modal-body #player').attr('src', '');
      });
</script>
@endsection