<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en')->unique();
            $table->string('title_vi')->unique();
            $table->string('img')->nullable();
            $table->string('recommend_en',400)->nullable();
            $table->string('recommend_vi',400)->nullable();
            $table->longText('content_en')->nullable();
            $table->longText('content_vi')->nullable();
            $table->string('video_link')->nullable();
            $table->tinyInteger('status');
            $table->string('slug');
            // $table->tinyInteger('page');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
