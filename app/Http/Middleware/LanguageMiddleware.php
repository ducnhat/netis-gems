<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(1);
        $browserLang = $request->server('HTTP_ACCEPT_LANGUAGE');
        $langPos = strpos($browserLang, 'vi');

        if($langPos === false){
            $browserLang = 'en';
        }else{
            $browserLang = 'vi';
        }

        $lang = Cookie::get('netis_language', false);
        // dd($lang);
        if(!$lang){
            Cookie::queue('netis_language', $browserLang, 86400 * 30);
            $lang = $browserLang;
            //dd($lang);
        }

        $request->session()->put('netis_language', $lang);

        \App::setLocale($lang);

        return $next($request);
    }
}
