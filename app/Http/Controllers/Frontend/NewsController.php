<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Category;
class NewsController extends Controller
{
    public function index(){
    	$category = Category::where('name_en','news')->first();
    	if(!empty($category)){
    		$news = News::where('category_id',$category->id)->get();
    		return view('news.news',compact('news'));
    	}else{
    		return view('news.news');
    	}
    	
    }

    public function index2(){
    	$category = Category::where('name_en','video')->first();
    	if(!empty($category)){
    	$video = News::where('category_id',$category->id)->get();
    	// dd($video);
    	return view('video.video',compact('video'));
    	}else{
    		return view('video.video');
    	}
    }

    public function detailNews($slug){
    	$news = News::where('slug',$slug)->first();
    	// dd($news);
    	return view('news.detail.detail',compact('news'));
    }
}
