<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
class ProductController extends Controller
{
    public function ChangeLang(Request $request){
    	//dd($request->all());
    	// dd($_GET['language']);
    	  Cookie::queue('netis_language', $_GET['language'], 86400 * 30);
    	  $browserLang = $request->server('HTTP_ACCEPT_LANGUAGE');
    	  $browserLang = $_GET['language'];
    	  // dd($browserLang);
    	   \App::setLocale($browserLang);
    	   return (['status'=>1]);

    }
}
