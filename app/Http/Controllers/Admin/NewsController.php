<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Category;
use Validator;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $news = News::all();
       return view('admin.news.list',compact('news'));
    }


      public function index2($id = null) 
    {   
        return view('admin.news.created');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // dd($request->all());
           $request->validate(
            [   
                'title_en' => 'required|unique:news',
                'title_vi' => 'required|unique:news',
                // 'recommend' => 'required',
                // 'content' => 'required',
                  ],
            [   'title_en.required' => 'Tiêu đề EN không được để trống',
                'title_en.unique' => 'Tiêu đề EN đã được sử dụng',
                'title_vi.unique' => 'Tiêu đề VI đã được sử dụng',
                'title_vi.required' => 'Tiêu đề VI không được để trống',
                // 'recommend.required' => 'Tóm tắt không được để trống',
                //   'content.required' => 'Nội Dung không được để trống',
              ]);
        // $a=array($request->select_category);
        $a = [
            'user_id' => 1,
            "category_id"=>$request->select_category,
            "img"=>$request->image,
            "slug"=>str_slug($request->title_en, "-"),
        ];
        $data = $request->toArray();
        $data2 = array_merge($data,$a);
        // dd($data2);
        $news = News::create($data2);
        return redirect()->route('admin.news.list');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $news = News::find($id);
         return view('admin.news.edited',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $news = News::find($id);
        $news->title_en = $request->title_en;
        $news->title_vi = $request->title_vi;
        $news->recommend_en = $request->recommend_en;
        $news->recommend_vi = $request->recommend_vi;
        $news->content_en = $request->content_en;
        $news->content_vi = $request->content_vi;
        $news->video_link = $request->video_link;
        $news->category_id = $request->select_category;
        $news->user_id = 1;
        $news->status = $request->status;
        $news->slug = str_slug($request->title_en, "-");
        $news->save();
        return redirect()->route('admin.news.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect()->route('admin.news.list');
    }

    public function uploadImage($type = null,Request $request){
        $extension = $request->file('file')->getClientOriginalExtension();
        if ($type) {
            $dir = 'uploads/' . $type . '/';
        } else {
            $dir = 'uploads/';
        }
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
        $nameAndPatch = $dir . $filename;
        return json_encode(['status' => true, 'filename' => $nameAndPatch]);
    }
}
