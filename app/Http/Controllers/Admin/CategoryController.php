<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
class CategoryController extends Controller
{
    public function index(){
    	$category = Category::all();
    	return view('admin.category.list',compact('category'));
    }

       public function index2($id = null) 
    {   
        // if(isset($id)){
        // $name = City::find($id);
        // }
        return view('admin.category.created');
    }

    public function store(Request $request){
    	// dd($request->all());
    	$category = new Category();
    	$category->name_vi = $request->name_vi;
    	$category->name_en = $request->name_en;
    	$category->status = $request->status;
    	$category->save();
    	// dd($category);
    	return redirect()->route('admin.category.list');
    }

    public function destroy($id){
    	$category = Category::find($id);
    	$category->delete();
    	return redirect()->route('admin.category.list');
    }

       public function edit($id)
    {
         $category = Category::find($id);
         return view('admin.category.edited',compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name_vi = $request->name_vi;
    	$category->name_en = $request->name_en;
    	$category->status = $request->status;
    	$category->save();
    	return redirect()->route('admin.category.list');
    }

}
