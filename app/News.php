<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
     protected $fillable = [
        'title_en', 'title_vi', 'img','recommend_en','recommend_vi','content_en','content_vi',
        'video_link', 'status', 'category_id', 'user_id','slug',
    ];
}
