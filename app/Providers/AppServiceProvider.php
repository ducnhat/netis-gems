<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);
        // dd(123);
        app('view')->composer('layouts.master', function($view){
            $request = new Request();
            $browserLang = array_key_exists('HTTP_ACCEPT_LANGUAGE',$_SERVER)  ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : 'vi';
            $langPos = strpos($browserLang, 'vi');

            if($langPos === false){
                $browserLang = 'en';
            }else{
                $browserLang = 'vi';
            }
            // dd($browserLang);
            $view->with(compact('browserLang'));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
